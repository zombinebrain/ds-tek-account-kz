/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
// @ts-ignore
import nodemailer from "nodemailer";
import fs from "fs";
import path from "path";
import handlebars from "handlebars";

const email = process.env.EMAIL;
const pass = process.env.EMAIL_PASS;

export const transporter = nodemailer.createTransport({
  host: "smtp.yandex.ru",
  port: 465,
  secure: true,
  auth: {
    user: email,
    pass,
  },
});

export const mailOptions = {
  from: email,
};

export const sendMail = async (
  templateFileName: string,
  config: {
    from: string | undefined;
    to: string | undefined;
    subject: string | undefined;
    text?: string;
    attachments?: {
      filename: string;
      path?: string;
      cid?: string;
      content?: any;
    }[];
  },
  replacements?: Record<string, string>
) => {
  const filePath = path.join(
    process.cwd(),
    "public",
    "templates",
    templateFileName + ".html"
  );

  const source = fs.readFileSync(filePath, "utf-8").toString();
  const template = handlebars.compile(source);

  const htmlToSend = template(replacements);

  const info = await transporter.sendMail({
    from: config.from,
    to: config.to,
    subject: config.subject,
    text: config.text ?? "", // dont really need this but it is recommended to have a text property as well
    html: htmlToSend,
    attachments: config.attachments,
  });

  console.log("Message sent: %s", info.response);
};
