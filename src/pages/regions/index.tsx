import { Region } from "@prisma/client";
import React, { useState } from "react";
import RegionDialog from "~/components/dialogs/RegionDialog";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import RegionsTable from "~/components/tables/RegionsTable";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import BaseButton from "~/components/ui/BaseButton";
import AddIcon from "@mui/icons-material/Add";

const RegionsPage: NextPageWithLayout = () => {
  const [open, setOpen] = React.useState(false);
  const [selectedRegion, setSelectedRegion] = useState<Region | null>(null);

  const handleClickOpen = () => {
    setSelectedRegion(null);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedRegion(null);
    setOpen(false);
  };

  const selectRegion = (region: Region) => {
    setSelectedRegion(region);
    setOpen(true);
  };

  return (
    <>
      <PageHeader title="Настройка стоимости доставки по регионам РФ" />
      <div className="p-5">
        <div className="flex justify-end">
          <BaseButton onClick={handleClickOpen}>
            <AddIcon className="mr-2" />
            Добавить регион
          </BaseButton>
        </div>
        <div className="mt-5">
          <RegionsTable onRegionSelect={selectRegion} />
        </div>
      </div>
      <RegionDialog
        open={open}
        onClose={handleClose}
        selectedRegion={selectedRegion}
      />
    </>
  );
};

RegionsPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

RegionsPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default RegionsPage;
