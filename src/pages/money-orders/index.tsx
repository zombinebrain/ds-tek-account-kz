import {NextPageWithLayout} from "~/typing/page";
import MainLayout from "~/components/layout/layout";
import {UserRoles} from "~/models/user";
import PageHeader from "~/components/layout/PageHeader";
import BaseButton from "~/components/ui/BaseButton";
import {api} from "~/utils/api";
import MoneyTransactionCard from "~/components/cards/MoneyTransactionCard";
import {useState} from "react";
import {Moment} from "moment/moment";
import {createAndDownloadXLSX} from "~/utils/createAndDownloadXLSX";
import DatePickerDownloadDialog from "~/components/dialogs/DatePickerDownloadDialog";
import {UserDrawer} from "~/components/drawers/UserDrawer";

const MoneyOrdersPage: NextPageWithLayout = () => {
  const [isOpenDownloadDialog, setIsOpenDownloadDialog] = useState(false);
  const [timePeriod, setTimePeriod] = useState<{ to: null | Moment, from: null | Moment }>({ to: null, from: null });
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [userEmail, setUserEmail] = useState("");

  const { data: transactions } = api.transactions.getMoneyGiftTransactions.useQuery();

  const {
    isInitialLoading: isReportOrdersLoading,
    refetch: fetchReportOrders
  } = api.transactions.getMoneyGiftTransactionsByDate.useQuery({
    to: timePeriod.to?.format() ?? null,
    from: timePeriod.from?.format() ?? null
  }, {
    refetchOnWindowFocus: false,
    enabled: false
  });

  const DownloadReport = async () => {
    if (!timePeriod.to || !timePeriod.from) {
      return;
    }
    const res = await fetchReportOrders();
    if (res.data) {
      createAndDownloadXLSX<{ name: string, points: number, contact: string, createdAt: Date }>(res.data, [
        { key: 'name', name: 'Наименование' },
        { key: 'points', name: 'Сумма' },
        { key: 'contact', name: 'Контактное лицо' },
        { key: 'createdAt', name: 'Дата оформления заказа' }
      ], `Отчёт о денежных подарках ${timePeriod.from.format('DD.MM.YYYY')}-${timePeriod.to.format('DD.MM.YYYY')}`);
    }
  };

  const closeDrawer = () => {
    setDrawerOpen(false);
    setUserEmail("");
  };

  const selectUser = (email: string) => {
    setUserEmail(email);
    setDrawerOpen(true);
  };

  return (
    <>
      <PageHeader title="Заказы" />
      <div className="px-5 pb-5">
        <div className="flex justify-end mb-5">
          <BaseButton
            onClick={() => setIsOpenDownloadDialog(true)}
          >
            Скачать отчёт
          </BaseButton>
        </div>
        <div className="flex flex-col gap-y-3">
          {
            transactions?.map(item => (
              <MoneyTransactionCard
                key={item.id}
                transaction={item}
                selectUser={selectUser}
              />
            ))
          }
        </div>
      </div>
      <DatePickerDownloadDialog
        isOpen={isOpenDownloadDialog}
        onClose={() => setIsOpenDownloadDialog(false)}
        title="Отчёт о заказах"
        timePeriod={timePeriod}
        setTimePeriod={setTimePeriod}
        isDownloading={isReportOrdersLoading}
        onDownload={DownloadReport}
      />
      <UserDrawer
        userEmail={userEmail}
        open={drawerOpen}
        onClose={closeDrawer}
      />
    </>
  );
};

MoneyOrdersPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

MoneyOrdersPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default MoneyOrdersPage;
