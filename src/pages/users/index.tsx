import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import React, { useState } from "react";
import InviteManagerDialog from "~/components/dialogs/InviteManagerDialog";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import UserManagementTable from "~/components/tables/UserManagementTable";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import { CustomTabPanel, a11yProps } from "~/components/CustomTabPanel";
import { api } from "~/utils/api";
import { UserRole } from "@prisma/client";
import CircularProgress from "@mui/material/CircularProgress";
import BaseButton from "~/components/ui/BaseButton";
import AddIcon from "@mui/icons-material/Add";

const tabs = [
  {
    label: "Клиенты",
    key: UserRole.RETAIL,
  },
  {
    label: "Менеджеры дистрибьютера",
    key: UserRole.SALES,
  },
  {
    label: "Работники",
    key: UserRole.MANAGER,
  },
];

const UsersPage: NextPageWithLayout = () => {
  const [open, setOpen] = useState(false);
  const [tabValue, setTabValue] = useState(0);

  const {
    isFetched: usersFetched,
    data: users = [],
    refetch: refetchUsers,
  } = api.users.getALL.useQuery({
    role: tabs[tabValue]!.key,
  });

  const { mutate: deleteUser } = api.users.deleteUser.useMutation({
    async onSuccess() {
      await refetchUsers();
    },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  return (
    <>
      <PageHeader title="Пользователи" />
      <div className="p-5">
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={tabValue}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            {tabs.map((tab, index) => (
              <Tab
                key={tab.key}
                label={tab.label}
                {...a11yProps(index)}
                className="normal-case font-bold text-subtitle2"
              />
            ))}
          </Tabs>
        </Box>

        {tabs.map((tab, index) => (
          <CustomTabPanel key={`${tab.key}-tab`} value={tabValue} index={index}>
            {tab.key === UserRole.MANAGER && (
              <div className="flex justify-end">
                <BaseButton
                  onClick={handleClickOpen}
                >
                  <AddIcon className="mr-2" />
                  Пригласить менеджера
                </BaseButton>
              </div>
            )}
            <div className="pt-5">
              {usersFetched ? (
                <UserManagementTable
                  tab={tabs[tabValue]!.key}
                  users={users}
                  deleteUser={deleteUser}
                />
              ) : (
                <Box
                  sx={{
                    display: "flex",
                    width: "100%",
                    height: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <CircularProgress
                    sx={{
                      color: "#E51C2F",
                    }}
                  />
                </Box>
              )}
            </div>
          </CustomTabPanel>
        ))}
      </div>
      <InviteManagerDialog open={open} onClose={handleClose} />
    </>
  );
};

UsersPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

UsersPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default UsersPage;
