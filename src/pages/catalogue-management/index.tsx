import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import React, { useState } from "react";
import GiftCard from "~/components/cards/GiftCard";
import ProductDialog from "~/components/dialogs/ProductDialog";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import ConfirmationDialog from "~/components/dialogs/ConfirmationDialog";
import BaseButton from "~/components/ui/BaseButton";
import AddIcon from "@mui/icons-material/Add";
import {CatalogueGift, GiftImage} from "~/models/gift";
import Typography from "@mui/material/Typography";
import Switch from "@mui/material/Switch";
import BaseLoader from "~/components/ui/BaseLoader";

const CatalogueManagementPage: NextPageWithLayout = () => {
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [isDeleteOpen, setIsDeleteOpen] = useState(false);
  const [selectedGift, setSelectedGift] = useState<CatalogueGift | null>(null);
  const {
    data: gifts,
    isLoading: isLoadingGifts,
    refetch: refetchGifts,
  } = api.gifts.getALL.useQuery();
  const { mutate: deleteGift } = api.gifts.delete.useMutation({
    onSuccess: async () => {
      await refetchGifts();
    },
  });
  const { mutate: updateGift, isLoading: isUpdating } = api.gifts.update.useMutation({
    onSuccess: async () => {
      setSelectedGift(null);
      await refetchGifts();
    },
  });

  const handleClickOpen = () => {
    setSelectedGift(null);
    setIsEditOpen(true);
  };

  const handleEditClose = () => {
    setSelectedGift(null);
    setIsEditOpen(false);
  };

  const openEditDialog = (gift: CatalogueGift) => {
    setSelectedGift(gift);
    setIsEditOpen(true);
  };

  const openDeleteDialog = (gift: CatalogueGift) => {
    setSelectedGift(gift);
    setIsDeleteOpen(true);
  };

  const handleDeleteClose = () => {
    setSelectedGift(null);
    setIsDeleteOpen(false);
  };

  const handleGiftDelete = () => {
    if (selectedGift) {
      deleteGift(selectedGift.id);
      handleDeleteClose();
    }
  };

  const handleSuccessSubmit = async () => {
    await refetchGifts();
  };

  return (
    <>
      <PageHeader title="Каталог подарков" />
      <div className="p-5">
        <div className="flex justify-end">
          <BaseButton onClick={handleClickOpen}>
            <AddIcon className="mr-2" />
            Добавить товар
          </BaseButton>
        </div>
        <div className="mt-5 flex flex-wrap gap-5">
          {isLoadingGifts
            ? <BaseLoader />
            : gifts?.length ? (
            gifts.map((gift) => (
              <GiftCard
                className="min-w-[300px] max-w-[300px]"
                showArticle
                deletable
                onEdit={() => openEditDialog(gift)}
                onDelete={() => openDeleteDialog(gift)}
                key={gift.id}
                gift={gift}
                additionalButton={
                  <div className="flex items-center">
                    <span className="text-subtitle2">Активный</span>
                    <Switch
                      disabled={isUpdating && selectedGift?.id === gift.id}
                      checked={gift.visible}
                      onChange={(e) => {
                        setSelectedGift(gift);
                        updateGift({
                          ...gift,
                          article: gift.article,
                          images: [
                            ...(
                              gift.images.filter(
                                (image) => !(image instanceof File)
                              ) as GiftImage[]
                            ).map((image) => image.url)
                          ],
                          visible: e.target.checked
                        });
                      }}
                      id={"visible-" + gift.id}
                      name="visible"
                      color="error"
                    />
                  </div>
                }
              />
            ))
          ) : (
            <Typography variant="subtitle1">
              Подарки не были добавлены
            </Typography>
          )}
        </div>
      </div>
      <ConfirmationDialog
        isOpen={isDeleteOpen}
        onClose={handleDeleteClose}
        onConfirm={handleGiftDelete}
        title="Вы уверены, что хотите удалить этот подарок?"
      />
      <ProductDialog
        open={isEditOpen}
        onClose={handleEditClose}
        onSuccess={handleSuccessSubmit}
        selectedGift={selectedGift}
      />
    </>
  );
};

CatalogueManagementPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

CatalogueManagementPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default CatalogueManagementPage;
