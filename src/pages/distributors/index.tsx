import React, { useState } from "react";
import DistributorsDialog from "~/components/dialogs/DistributorsDialog";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import DistributorsTable from "~/components/tables/DistributorsTable";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import { Distributor } from "@prisma/client";
import BaseButton from "~/components/ui/BaseButton";
import AddIcon from "@mui/icons-material/Add";

const DistributorsPage: NextPageWithLayout = () => {
  const [open, setOpen] = React.useState(false);
  const [selectedDistributor, setSelectedDistributor] = useState<Distributor | null>(null);

  const {
    status,
    data: distributors,
    refetch: refetchDistributors,
  } = api.distributors.getAll.useQuery();

  const handleClickEdit = (distributor: Distributor) => {
    setSelectedDistributor(distributor);
    setOpen(true);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setSelectedDistributor(null);
  };

  return (
    <>
      <PageHeader title="Дистрибьюторы" />
      <div className="p-5">
        <div className="flex justify-end">
          <BaseButton onClick={handleClickOpen}>
            <AddIcon className="mr-2" />
            Добавить дистрибьютора
          </BaseButton>
        </div>
        <div className="mt-5">
          <DistributorsTable
            distributors={distributors ?? []}
            status={status}
            onEdit={handleClickEdit}
          />
        </div>
      </div>
      <DistributorsDialog
        open={open}
        onClose={handleClose}
        refetchDistributors={refetchDistributors}
        selectedDistributor={selectedDistributor}
      />
    </>
  );
};

DistributorsPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

DistributorsPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default DistributorsPage;
