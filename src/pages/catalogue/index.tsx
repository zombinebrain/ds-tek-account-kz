import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import ClientLayout from "~/components/layout/client/layout";
import { UserRoles } from "~/models/user";
import AddCompanyBanner from "~/components/banners/AddCompanyBanner";
import CatalogueTemplate from "~/components/gift/CatalogueTemplate";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import Drawer from "@mui/material/Drawer";
import ClearIcon from "@mui/icons-material/Clear";
import React, {useState} from "react";
import PhoneMoneyGiftForm from "~/components/gift/PhoneMoneyGiftForm";
import CardMoneyGiftForm from "~/components/gift/CardMoneyGiftForm";
import MoneyGiftCard from "~/components/gift/MoneyGiftCard";

type MoneyGiftDrawerType = 'PHONE' | 'CARD';

const CataloguePage: NextPageWithLayout = () => {
  const [moneyGiftDrawer, setMoneyGiftDrawer] = useState<MoneyGiftDrawerType | null>(null);

  const onMoneyGiftClick = (type: MoneyGiftDrawerType) => {
    setMoneyGiftDrawer(type);
  };

  const { data: gifts, isLoading: isLoadingGifts, refetch: refetchGifts } =
    api.gifts.getAvailable.useQuery();
  const { data: companiesCount = 0, isFetched: companiesCountFetched } =
    api.companies.getMyCount.useQuery();

  return (
    <>
      {companiesCountFetched && !companiesCount && <AddCompanyBanner />}
      <div className="mb-5">
        <Typography variant="h2" className="mb-5">
          Виртуальные Денежные подарки
        </Typography>
        <div className="flex gap-5 py-6">
          <MoneyGiftCard
            imageSrc="/money_phone.webp"
            title="Пополнить баланс мобильного телефона"
            onAddClick={() => onMoneyGiftClick('PHONE')}
          />
          <MoneyGiftCard
            imageSrc="/money_card.webp"
            title="Перевести бонусные баллы на банковскую карту"
            onAddClick={() => onMoneyGiftClick('CARD')}
          />
        </div>
      </div>
      <CatalogueTemplate
        title="Подарки из каталога"
        isLoadingCatalogue={isLoadingGifts}
        catalogue={gifts}
        refetchCatalogue={refetchGifts}
      />
      <Drawer
        anchor="right"
        open={!!moneyGiftDrawer}
        onClose={() => setMoneyGiftDrawer(null)}
        PaperProps={{
          className: "p-5 w-[400px]",
          sx: {
            height: "calc(100% - 40px)",
          },
        }}
      >
        <IconButton
          onClick={() => setMoneyGiftDrawer(null)}
          aria-label="delete"
          className="w-fit"
          sx={{
            padding: 0,
            marginBottom: "5px",
          }}
        >
          <ClearIcon />
        </IconButton>
        <>
          {
            moneyGiftDrawer === "PHONE"
              ? <PhoneMoneyGiftForm closeDrawer={() => setMoneyGiftDrawer(null)} />
              : <CardMoneyGiftForm closeDrawer={() => setMoneyGiftDrawer(null)} />
          }
        </>
      </Drawer>
    </>
  );
};

CataloguePage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

CataloguePage.auth = {
  roles: [UserRoles.RETAIL, UserRoles.SALES],
};

export default CataloguePage;
