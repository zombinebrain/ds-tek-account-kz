import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import DashboardVidget from "~/components/vidgets/DashboardVidget";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
// import CallOutlinedIcon from "@mui/icons-material/CallOutlined";
// import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import Typography from "@mui/material/Typography";
import EditSupportDialog from "~/components/dialogs/EditSupportDialog";
import { useState } from "react";
// import { useSelector } from "react-redux";
// import { RootState } from "~/app/store";
import { api } from "~/utils/api";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import StoreMallDirectoryOutlinedIcon from "@mui/icons-material/StoreMallDirectoryOutlined";
import TireRepairOutlinedIcon from "@mui/icons-material/TireRepairOutlined";
import HandshakeIcon from '@mui/icons-material/Handshake';
import { CompanyTypeText } from "~/models/company";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import {
  OrderStatusIcons,
  OrderStatusText,
  OrderStatuses,
} from "~/models/order";
import { OrderStatus } from "@prisma/client";
import { useRouter } from "next/router";

const DashboardPage: NextPageWithLayout = () => {
  const [open, setOpen] = useState(false);
  const router = useRouter();

  // const supportContacts = useSelector(
  //   (state: RootState) => state.supportContacts.contacts
  // );

  const { data: companiesData } = api.companies.getCount.useQuery();
  const { data: usersData } = api.users.getCount.useQuery();
  const { data: bonusCampaignsData } = api.bonusCampaigns.getLatest.useQuery();
  const { data: ordersData } = api.orders.getCount.useQuery();

  // const phoneContacts = useMemo(() => {
  //   return supportContacts.filter((contact) => contact.type === "PHONE");
  // }, [supportContacts]);
  // const emailContacts = useMemo(() => {
  //   return supportContacts.filter((contact) => contact.type === "EMAIL");
  // }, [supportContacts]);

  const handleOrderStatusClick = async (status: OrderStatus) => {
    await router.push(`/orders?status=${status}`);
  };

  return (
    <>
      <PageHeader title="Мой рабочий стол" />
      <div className="flex flex-wrap gap-5 px-10">
        <DashboardVidget title="заказы" to="/orders" count={ordersData?.all}>
          <div className="flex justify-between px-5 pb-5">
            {ordersData &&
              Object.keys(OrderStatuses).map((status, index) => {
                const IconComponent =
                  OrderStatusIcons[status as keyof typeof OrderStatuses];
                return (
                  <div
                    onClick={() =>
                      handleOrderStatusClick(status as OrderStatus)
                    }
                    key={index}
                    className="flex w-[85px] cursor-pointer flex-col items-center rounded-[10px] hover:bg-grey1"
                  >
                    <div className="flex h-[44px] w-[44px] items-center justify-center rounded-full bg-[#F2F3F5]">
                      <IconComponent />
                    </div>
                    <Typography variant="subtitle2">
                      {OrderStatusText[status as keyof typeof OrderStatuses]}
                    </Typography>
                    {
                      ordersData[
                        status.toLowerCase() as keyof typeof ordersData
                      ]
                    }
                  </div>
                );
              })}
          </div>
        </DashboardVidget>
        <DashboardVidget title="бонусные программы" to="/reports">
          <div className="px-5 pb-5">
            {bonusCampaignsData?.map((campaign) => (
              <div
                key={campaign.id}
                className="flex items-center justify-between"
              >
                <div className="flex items-center">
                  <DescriptionOutlinedIcon className="mr-2" />
                  <Typography variant="subtitle2">
                    {`${campaign.name ? campaign.name + " " : ""}${
                      campaign.startDate
                    } - ${campaign.endDate}`}
                  </Typography>
                </div>
                <Typography variant="subtitle2">
                  {campaign.status === "CLOSED" && "Период закрыт"}
                </Typography>
              </div>
            ))}
          </div>
        </DashboardVidget>
        <DashboardVidget title="пользователи" to="/users">
          <div className="px-5 pb-5">
            <div className="flex items-center">
              <PersonOutlineOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                Клиенты • {usersData?.retail}
              </Typography>
            </div>
            <div className="mt-1 flex items-center">
              <PersonOutlineOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                Менеджеры дистрибьютера • {usersData?.sales}
              </Typography>
            </div>
          </div>
        </DashboardVidget>
        <DashboardVidget title="розничные торговые объекты" to="/companies">
          <div className="px-5 pb-5">
            <div className="flex items-center">
              <TireRepairOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                {CompanyTypeText.STO} • {companiesData?.serviceStations}
              </Typography>
            </div>
            <div className="mt-1 flex items-center">
              <StoreMallDirectoryOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                Магазины • {companiesData?.shops}
              </Typography>
            </div>
            <div className="mt-1 flex items-center">
              <HandshakeIcon className="mr-2" />
              <Typography variant="subtitle1">
                Субдилеры • {companiesData?.subdealers}
              </Typography>
            </div>
          </div>
        </DashboardVidget>
        {/* <DashboardVidget
          title="служба поддержки клиентов"
          onEdit={() => setOpen(true)}
        >
          <div className="px-5 pb-5">
            <div className="flex items-center">
              <CallOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                {phoneContacts.length
                  ? phoneContacts.map((contact) => contact.value).join(", ")
                  : "Телефон не указан"}
              </Typography>
            </div>
            <div className="mt-1 flex items-center">
              <EmailOutlinedIcon className="mr-2" />
              <Typography variant="subtitle1">
                {emailContacts.length
                  ? emailContacts.map((contact) => contact.value).join(", ")
                  : "Email не указан"}
              </Typography>
            </div>
          </div>
        </DashboardVidget> */}
      </div>
      <EditSupportDialog open={open} onClose={() => setOpen(false)} />
    </>
  );
};

DashboardPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

DashboardPage.auth = {
  roles: [UserRoles.ADMIN, UserRoles.MANAGER],
};

export default DashboardPage;
