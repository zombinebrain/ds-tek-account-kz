import LandingLayout from "~/components/layout/landing/layout";
import {NextPageWithLayout} from "~/typing/page";
import {api} from "~/utils/api";
import BaseLoader from "~/components/ui/BaseLoader";
import PartnerCard from "~/components/landing/PartnerCard";

const PartnersPage: NextPageWithLayout = () => {
  const { data: partners = [], isLoading } = api.distributors.getAllForLanding.useQuery();

  return (
    <div className="flex flex-col items-center">
      <div className="font-bold text-[44px] mb-15">Партнёры</div>
      {
        isLoading
          ? <BaseLoader />
          : (
            <div className="grid grid-cols-[repeat(3,minmax(0,400px))] gap-10">
              {
                partners.map(partner => (
                  <PartnerCard partner={partner} key={partner.id} />
                ))
              }
            </div>
          )
      }
    </div>
  );
};

PartnersPage.getLayout = function getLayout(page) {
  return <LandingLayout>{page}</LandingLayout>;
};

export default PartnersPage;
