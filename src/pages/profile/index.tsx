import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import React, { useEffect, useState } from "react";
import AddCompanyDialog from "~/components/dialogs/add-company-dialog/AddCompanyDialog";
import ClientLayout from "~/components/layout/client/layout";
import AddIcon from "@mui/icons-material/Add";
import {ExtendedClientUser, UserRoles} from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import CompanyCard from "~/components/cards/CompanyCard";
import { signOut } from "next-auth/react";
import { debounce } from "@mui/material/utils";
import { CompanyWithCompanyDistributors } from "~/models/company";
import { setUser } from "~/app/store/modules/user";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "~/app/store";
import BaseButton from "~/components/ui/BaseButton";
import ConfirmationDialog from "~/components/dialogs/ConfirmationDialog";

type SelectedCompanyType = {companyId: string, distributorId?: string};

const ProfilePage: NextPageWithLayout = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.users.user);
  const [name, setName] = useState(user!.name);
  const [phone, setPhone] = useState(user!.phone);
  const [open, setOpen] = useState(false);
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [deleteDialogText, setDeleteDialogText] = useState("");
  const [selectedCompany, setSelectedCompany] = useState<SelectedCompanyType | null>(null);

  const { data: companies, refetch: refetchCompanies } =
    api.companies.getMyCompaniesWithDistributors.useQuery();

  const { mutate: updateUser } = api.users.update.useMutation({
    onSuccess: (updatedUser: ExtendedClientUser) => {
      dispatch(setUser(updatedUser));
    },
  });
  const { mutate: deleteCompany } = api.companies.delete.useMutation({
    onSuccess: async () => {
      await refetchCompanies();
      if (user && selectedCompany) {
        dispatch(setUser({
          ...user,
          companies: user.companies.filter(company => company.id !== selectedCompany.companyId)
        }));
      }
      closeDeleteDialog();
    },
  });
  const { mutate: deleteCompanyDistributor } =
    api.companies.deleteCompanyDistributor.useMutation({
      onSuccess: async () => {
        await refetchCompanies();
        closeDeleteDialog();
      },
    });

  const update = React.useMemo(
    () =>
      debounce((name: string, phone: string) => {
        updateUser({
          name,
          phone,
        });
      }, 1000),
    [updateUser]
  );

  const closeDeleteDialog = () => {
    setDeleteDialogText("");
    setDeleteDialogOpen(false);
    setSelectedCompany(null);
  };

  const handleDeleteCompany = (companyId: string) => {
    setSelectedCompany({ companyId });
    setDeleteDialogText("вы уверены, что хотите удалить свою компанию?");
    setDeleteDialogOpen(true);
  };

  const handleDeleteDistributor = (
    distributorId: string,
    companyId: string
  ) => {
    setSelectedCompany({ companyId, distributorId });
    setDeleteDialogText("вы уверены, что хотите удалить своего дистрибьютера?");
    setDeleteDialogOpen(true);
  };

  const handleDeleteConfirm = () => {
    if (selectedCompany) {
      const { distributorId, companyId } = selectedCompany;
      distributorId
        ? deleteCompanyDistributor({ distributorId, companyId })
        : deleteCompany({ id: companyId });
    }
  };

  useEffect(() => {
    if (name !== user?.name || phone !== user.phone) {
      update(name, phone);
    }
  }, [name, phone, update, user]);

  useEffect(() => {
    const mustOpenDialog = localStorage.getItem("openAddCompanyDialog");
    if (mustOpenDialog) {
      setOpen(true);
      localStorage.removeItem("openAddCompanyDialog");
    }
  }, []);

  return (
    <div className="flex flex-col items-center p-5">
      <Typography variant="h2">
        Мой профиль
      </Typography>
      <div className="w-[400px] p-5">
        <Typography variant="h4" className="my-6">
          личные данные
        </Typography>
        <div className="flex flex-col">
          <TextField
            value={name}
            onChange={(e) => setName(e.target?.value)}
            variant="outlined"
            placeholder="Ваше имя"
            className="bg-white"
            inputProps={{ className: 'text-subtitle1 py-3' }}
          />
          <TextField
            value={phone}
            onChange={(e) => setPhone(e.target?.value)}
            variant="outlined"
            placeholder="Ваш телефон"
            className="bg-white mt-3"
            inputProps={{ className: 'text-subtitle1 py-3' }}
          />
        </div>
        <Typography variant="h4" className="my-6">
          розничные торговые объекты
        </Typography>
        <div>
          <BaseButton
            onClick={() => setOpen(true)}
            className="w-full"
          >
            <>
              <AddIcon className="mr-2" />
              добавить РТО
            </>
          </BaseButton>
          <div className="mt-5">
            <div className="max-h-[500px] overflow-y-auto space-y-4 pb-0.5 -mb-0.5">
              {companies?.map((company: CompanyWithCompanyDistributors) => (
                <CompanyCard
                  key={company.id}
                  company={company}
                  onDeleteCompany={() => handleDeleteCompany(company.id)}
                  onDeleteDistributor={(distributorId: string) =>
                    handleDeleteDistributor(distributorId, company.id)
                  }
                />
              ))}
            </div>
{/*            <BaseButton
              small
              variant="LIGHT"
              onClick={async () =>
                await signOut({ callbackUrl: process.env.BASE_URL })
              }
              className="mt-5 w-full"
            >
              выйти
            </BaseButton>*/}
          </div>
        </div>
      </div>
      <ConfirmationDialog
        isOpen={deleteDialogOpen}
        onClose={closeDeleteDialog}
        onConfirm={handleDeleteConfirm}
        title={deleteDialogText}
      />
      <AddCompanyDialog
        open={open}
        onClose={() => setOpen(false)}
        refetchCompanies={refetchCompanies}
      />
    </div>
  );
};

ProfilePage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

ProfilePage.auth = {
  roles: [UserRoles.RETAIL],
};

export default ProfilePage;
