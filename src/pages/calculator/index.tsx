import ClientLayout from "~/components/layout/client/layout";
import {UserRoles} from "~/models/user";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import React, {useState} from "react";
import {NextPageWithLayout} from "~/typing/page";
import {api} from "~/utils/api";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import {Grid} from "@mui/material";
import { createFilterOptions } from '@mui/material/Autocomplete';
import InputAdornment from "@mui/material/InputAdornment";
import IconSearch from "~/components/icons/IconSearch";
import IconButton from "@mui/material/IconButton";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import PriceBadge from "~/components/badges/PriceBadge";
import IconDelete from "~/components/icons/IconDelete";

type Article = {
  code: string;
  points: number;
  count: number;
};

const CalculatorPage: NextPageWithLayout = () => {
  const [selectedArticles, setSelectedArticles] = useState<Article[]>([]);
  const { data = [] } = api.bonusCampaigns.getLatestCampaignWithArticles.useQuery();

  const options: readonly Article[] = data[0] ? data[0].articles.map(item => ({ ...item, count: 1 })) : [];

  const filterOptions = createFilterOptions<Article>({
    ignoreCase: true,
    matchFrom: "start",
    limit: 50
  });

  const handleCount = (index: number, direction: 'increment' | 'decrement') => {
    const updated = selectedArticles.map((a, i) => {
      if (i === index) {
        return ({
          ...a,
          count: direction === 'decrement' ? --a.count : ++a.count
        });
      } else {
        return a;
      }
    });
    setSelectedArticles(updated);
  };

  const pointsCount = selectedArticles.reduce((accumulator, currentValue) => accumulator + currentValue.points * currentValue.count, 0);

  return (
    <>
      <Typography variant="h2" className="mb-5 text-center">
        Калькулятор баллов
      </Typography>
      <div className="flex flex-col items-center w-[400px] mx-auto">
        <div className="relative bg-white p-5 pr-[150px] my-6">
          <Typography variant="subtitle2" className="flex flex-col mb-3">
            <span className="font-bold mb-2">Планируйте подарки заранее</span>
            <span>Воспользуйтесь виртуальным калькулятором и рассчитайте будущее начисление баллов за покупки FENOX.</span>
          </Typography>
          <div>
            <Image
              className="absolute -top-11 -right-11"
              width={220}
              height={220}
              src="/calculator.png"
              alt="calculator image"
            />
          </div>
        </div>
        <Autocomplete
          forcePopupIcon={false}
          getOptionLabel={(option) => option.code}
          filterOptions={filterOptions}
          disableClearable
          options={options}
          autoComplete
          multiple
          fullWidth
          filterSelectedOptions
          isOptionEqualToValue={(option, value) => option.code === value.code}
          noOptionsText="Результатов не найдено"
          value={selectedArticles}
          onChange={(event, newValue: Article[]) => {
            setSelectedArticles(newValue);
          }}
          sx={{
            "& .MuiOutlinedInput-root": {
              borderRadius: "0",
              padding: "14 12"
            },
            "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
              border: "none",
              borderBottom: "1px solid #D7D7D7"
            }
          }}
          renderInput={(params) => (
            <TextField
              className="bg-white text-subtitle1"
              placeholder="ввести артикул"
              {...params}
              InputProps={{
                ...params.InputProps,
                className: "text-subtitle1 border-0",
                startAdornment: (
                  <InputAdornment position="start">
                    <IconSearch/>
                  </InputAdornment>
                ),
              }}
            />
          )}
          renderOption={(props, option) => {
            return (
              <li {...props} key={option.code}>
                <Grid container alignItems="center">
                  <Grid
                    item
                    sx={{
                      width: "calc(100% - 44px)",
                      wordWrap: "break-word",
                    }}
                  >
                    <Typography variant="subtitle2">
                      {option.code}
                    </Typography>
                  </Grid>
                </Grid>
              </li>
            );
          }}
          renderTags={() => null}
        />
        <section className="w-[400px] text-center">
          <Typography variant="h3" className="my-8">
            Баллы
          </Typography>
          <Typography variant="h4" className="pb-5 flex justify-between border-solid border-0 border-b border-grey2">
            <span>Итого</span><span>{pointsCount} баллов</span>
          </Typography>
          <div className="mt-5 flex flex-col space-y-3 max-h-[570px] overflow-y-auto">
            {
              !selectedArticles.length ? (
                <Typography variant="subtitle2" className="pt-15">Введите артикул в строку поиска</Typography>
              ) : (
                selectedArticles.map((article, index) => (
                  <div key={article.code} className="flex items-center justify-between space-x-6">
                    <span className="flex-1 text-left">{article.code}</span>
                    <PriceBadge points={article.points} />
                    <div className="flex items-center ">
                      <IconButton
                        disabled={article.count === 1}
                        onClick={() => handleCount(index, 'decrement')}
                        aria-label="add to cart"
                        className="h-5 w-5 bg-red text-white hover:bg-grey3 disabled:bg-grey2"
                      >
                        <RemoveIcon />
                      </IconButton>
                      <div className="text-xl mx-1 inline-block font-bold">{article.count}</div>
                      <IconButton
                        onClick={() => handleCount(index, 'increment')}
                        aria-label="add to cart"
                        className="h-5 w-5 bg-red text-white hover:bg-grey3"
                      >
                        <AddIcon />
                      </IconButton>
                    </div>
                    <IconButton
                      onClick={() => {
                        const updated = selectedArticles.filter((a) => a.code !== article.code);
                        setSelectedArticles(updated)
                      }}
                    >
                      <IconDelete />
                    </IconButton>
                  </div>
                ))
              )
            }
          </div>
        </section>
      </div>
    </>
  );
};

CalculatorPage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

CalculatorPage.auth = {
  roles: [UserRoles.RETAIL]
};

export default CalculatorPage;
