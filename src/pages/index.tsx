import { NextPageWithLayout } from "~/typing/page";
import Image from "next/image";
import SignInDialog from "~/components/dialogs/SignInDialog";
import { useEffect, useState } from "react";
import SignUpDialog from "~/components/dialogs/SignUpDialog";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { UserRoles } from "~/models/user";
import LandingLayout from "~/components/layout/landing/layout";
import Link from "next/link";
import {api} from "~/utils/api";
import LandingButton from "~/components/landing/LandingButton";
import BaseLoader from "~/components/ui/BaseLoader";
import GalleryGiftCard, {GalleryGiftType} from "~/components/landing/GalleryGiftCard";
import Typography from "@mui/material/Typography";
import IconArrow from "~/components/icons/IconArrow";
import GalleryGiftDialog from "~/components/landing/GalleryGiftDialog";
import LandingMoneyGifts from "~/components/landing/LandingMoneyGifts";

const steps = [
  {
    header: 'Зарегистрируйтесь',
    description: 'Приветственные бонусы, в размере 100 баллов',
    id: 'sign'
  },
  {
    header: 'Получайте баллы',
    description: 'Покупайте товары FENOX и получайте баллы',
    id: 'points'
  },
  {
    header: 'Забирайте подарки',
    description: 'Выбирайте подарки из каталога',
    id: 'gifts'
  }
];

const HomePage: NextPageWithLayout = () => {
  const [isSignInModalOpen, setIsSignInModalOpen] = useState(false);
  const [isSignUpDialogOpen, setIsSignUpDialogOpen] = useState(false);
  const [isSelectedGiftDialog, setIsSelectedGiftDialog] = useState(false);
  const [selectedGift, setSelectedGift] = useState<GalleryGiftType | null>(null);
  const { data: session } = useSession();
  const router = useRouter();

  const { data: gifts = [], isLoading: isGiftsLoading } = api.gifts.getGiftGallery.useQuery(12);
  //const { data: partners = [], isLoading: isPartnersLoading } = api.distributors.getAllForLanding.useQuery(9);

  useEffect(() => {
    if (
      router &&
      session &&
      (session.user.role === UserRoles.RETAIL ||
        session.user.role === UserRoles.SALES)
    ) {
      void router.push("/catalogue");
    } else if (router && session) {
      void router.push("/dashboard");
    }
  }, [router, session]);

  const handleCloseSelectedGiftDialog = () => {
    setIsSelectedGiftDialog(false);
    setSelectedGift(null);
  };

  return (
    <div className="flex flex-col items-center">
      <section className="flex relative px-20 mb-16 w-full">
        <div className="w-[680px]">
          <Typography variant="h1" className="mb-5">
            Программа Лояльности Fenox Шоколад
          </Typography>
          <span className="text-subtitle1 text-grey3">
            Вы владелец СТО или магазина автозапчастей.<br/> Регистрируйтесь, покупайте FENOX, забирайте подарки!
          </span>
          <div className="mt-12 space-x-5 text-grey3 bg-grey1 rounded w-fit p-4">
            Бонусная программа в разработке
            {/*<LandingButton
              type="SECONDARY"
              onClick={() => setIsSignInModalOpen(true)}
            >
              Войти
            </LandingButton>
            <LandingButton onClick={() => setIsSignUpDialogOpen(true)}>
              Зарегистрироваться
            </LandingButton>*/}
          </div>
        </div>
        <div className="w-1/2">
          <Image
            className="absolute -top-[148px] right-0 z-10 rotate-3"
            width={741}
            height={660}
            src="/chocolate-bar.png"
            alt="Шоколадная плитка Fenox"
          />
        </div>
      </section>
      <section className="flex w-full h-[670px] z-50 relative mb-[34px]">
        <div className="absolute inset-0 -skew-y-[9deg] bg-grey1 z-[-1]" />
        <div className="clip-path">
          <Image
            className="pt-[64px] box-content"
            width={720}
            height={720}
            src="/chocolate_engine.png"
            alt="Шоколадный двигатель"
          />
        </div>
        <div className="flex flex-col items-center justify-center space-y-10 w-1/2 mb-5">
          {
            steps.map(step => (
              <div className="w-[400px] relative" key={step.id}>
                <IconArrow
                  className="absolute top-[18px] -left-8"
                  width={24}
                  height={24}
                />
                <Typography variant="h2" className="text-red mb-2.5">
                  {step.header}
                </Typography>
                <span className="text-subtitle1">{step.description}</span>
              </div>
            ))
          }
        </div>
      </section>
      <section className="my-25">
        <Typography variant="h2" className="mb-15 text-center">Виртуальные денежные подарки</Typography>
        <LandingMoneyGifts onClick={() => setIsSignUpDialogOpen(true)} />
      </section>
      <section className="mt-25 mb-10">
        <div className="flex flex-col items-center">
          <Typography variant="h2" className="mb-5 text-center">Каталог подарков</Typography>
          {/*<Link href="/catalogue-gallery" className="text-red no-underline hover:underline flex items-center w-fit">
            Посмотреть все
            <svg className="ml-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M8 1L13 9.5L8 18" stroke="#E51C2F"/>
            </svg>
          </Link>*/}
        </div>
        {
          isGiftsLoading
            ? <BaseLoader />
            : (
              <div className="last:pb-15 w-[1440px] overflow-hidden [mask-image:_linear-gradient(to_right,transparent_0,_black_64px,_black_calc(100%-64px),transparent_100%)]">
                <div className="flex space-x-10 animate-infinite-scroll hover:animate-paused pt-15">
                  {
                    [...gifts.slice(0, 6), ...gifts.slice(0, 6)].map((gift, index) => (
                      <GalleryGiftCard
                        key={gift.id + index}
                        className="cursor-pointer"
                        gift={gift}
/*                        onCardClick={() => {
                          setSelectedGift(gift);
                          setIsSelectedGiftDialog(true);
                        }}*/
                      />
                    ))
                  }
                </div>
                {
                  gifts.length >= 12 && (
                    <div className="flex space-x-10 animate-infinite-scroll-reverse hover:animate-paused mt-15">
                      {
                        [...gifts.slice(6, 12), ...gifts.slice(6, 12)].map((gift, index) => (
                          <GalleryGiftCard
                            key={gift.id + index}
                            className="cursor-pointer"
                            gift={gift}
/*                            onCardClick={() => {
                              setSelectedGift(gift);
                              setIsSelectedGiftDialog(true);
                            }}*/
                          />
                        ))
                      }
                    </div>
                  )
                }
              </div>
            )
        }
      </section>
      {/*<section className="my-25">
        <div className="flex flex-col items-center mb-15">
          <Typography variant="h2" className="mb-5">Партнёры программы</Typography>
          <Link href="/partners" className="text-red no-underline hover:underline flex items-center w-fit">
            Посмотреть всех
            <svg className="ml-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M8 1L13 9.5L8 18" stroke="#E51C2F"/>
            </svg>
          </Link>
        </div>
        {
          isPartnersLoading
            ? <BaseLoader />
            : (
              <div className="grid grid-cols-[repeat(3,minmax(0,400px))] gap-10">
                {
                  partners.map(partner => (
                    <PartnerCard partner={partner} key={partner.id} />
                  ))
                }
              </div>
            )
        }
      </section>*/}
      <section className="relative w-full h-[820px]">
        <Image fill={true} src="/chocolate_hero.png" alt="Баннер баллов за регистрацию" />
        <div className="flex flex-col absolute top-1/2 left-20 -translate-y-1/2 w-1/2">
          <Typography variant="h1" className="text-white">Дарим бонус<br /> за регистрацию: <br />100
            баллов</Typography>
          <div className="mt-8 space-x-5 text-grey3 bg-grey1 rounded w-fit p-4">
            Бонусная программа в разработке
{/*            <LandingButton
              type="SECONDARY"
              onClick={() => setIsSignInModalOpen(true)}
            >
              Войти
            </LandingButton>
            <LandingButton onClick={() => setIsSignUpDialogOpen(true)}>
              Зарегистрироваться
            </LandingButton>*/}
          </div>
        </div>
      </section>
      <SignInDialog
        open={isSignInModalOpen}
        onClose={() => setIsSignInModalOpen(false)}
        onSignUp={() => setIsSignUpDialogOpen(true)}
      />
      <SignUpDialog
        open={isSignUpDialogOpen}
        onClose={() => setIsSignUpDialogOpen(false)}
      />
      {
        selectedGift && (
          <GalleryGiftDialog
            isOpen={isSelectedGiftDialog}
            onClose={handleCloseSelectedGiftDialog}
            gift={selectedGift}
          />
        )
      }
    </div>
  );
};

HomePage.getLayout = function getLayout(page) {
  return <LandingLayout>{page}</LandingLayout>;
};

export default HomePage;
