import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import OrderCard from "~/components/cards/OrderCard";
import { NextPageWithLayout } from "~/typing/page";
import ClearIcon from "@mui/icons-material/Clear";
import { api } from "~/utils/api";
import { useEffect, useState } from "react";
import Cart from "~/components/cart/Cart";
import { OrderWithImageGift } from "~/models/order";
import OrderStatusChip from "~/components/OrderStatusChip";
import ClientLayout from "~/components/layout/client/layout";
import { UserRoles } from "~/models/user";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "~/app/store";
import { toggleCartOpen } from "~/app/store/modules/cart";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { setBalance, setOrders } from "~/app/store/modules/user";
import { setLatestTransactions } from "~/app/store/modules/transactions";
import BaseButton from "~/components/ui/BaseButton";

const MyOrdersPage: NextPageWithLayout = () => {
  const isCartOpen = useSelector((state: RootState) => state.cart.open);
  const dispatch = useDispatch();
  const {
    data: myOrders,
    refetch: refetchMyOrders,
    isFetched,
  } = api.orders.getMy.useQuery();
  // const [isCartOpen, setCartOpen] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState<OrderWithImageGift | null>(
    null
  );
  const { data: balance, refetch: refetchBalance } =
    api.transactions.getMyBalance.useQuery();
  const { data: latestTransactions, refetch: refetchLatestTransactions } =
    api.transactions.getLatest.useQuery();
  const { data: activeOrdersCount, refetch: refetchActiveOrdersCount } =
    api.orders.getMyOrdersCount.useQuery();

  const { mutate: cancelOrder, isLoading: isOrderCanceling } =
    api.orders.cancelOrder.useMutation({
      async onSuccess() {
        await refetchBalance();
        await refetchLatestTransactions();
        await refetchMyOrders();
        await refetchActiveOrdersCount();
        setSelectedOrder(null);
        dispatch(toggleCartOpen(false));
      },
    });

  const { mutate: confirmOrderDelivery } =
    api.orders.confirmOrderDelivery.useMutation({
      async onSuccess() {
        await refetchMyOrders();
        setSelectedOrder(null);
        dispatch(toggleCartOpen(false));
      },
    });

  const selectOrder = (order: OrderWithImageGift) => {
    setSelectedOrder(order);
    dispatch(toggleCartOpen(true));
  };

  useEffect(() => {
    if (balance !== undefined) {
      dispatch(setBalance(balance));
    }
  }, [balance, dispatch]);

  useEffect(() => {
    if (latestTransactions !== undefined) {
      dispatch(setLatestTransactions(latestTransactions));
    }
  }, [latestTransactions, dispatch]);

  useEffect(() => {
    if (activeOrdersCount !== undefined) {
      dispatch(setOrders(activeOrdersCount));
    }
  }, [activeOrdersCount, dispatch]);

  return (
    <>
      <div className="flex flex-col items-center p-5">
        <Typography variant="h2">Мои заказы</Typography>
        <div className="mt-5 flex flex-col gap-5">
          {isFetched ? (
            myOrders?.length ? (
              myOrders.map((order) => (
                <OrderCard
                  key={order.id}
                  order={order}
                  onOrderSelect={selectOrder}
                />
              ))
            ) : (
              <div className="flex flex-col items-center space-y-4">
                <Typography variant="subtitle1">
                  Вы еще не оформили ни одного заказа
                </Typography>
                <BaseButton href="/catalogue">Перейти в каталог</BaseButton>
              </div>
            )
          ) : (
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress
                sx={{
                  color: "#E51C2F",
                }}
              />
            </Box>
          )}
        </div>
      </div>
      {selectedOrder && (
        <Drawer
          anchor="right"
          open={isCartOpen}
          onClose={() => dispatch(toggleCartOpen(false))}
          PaperProps={{
            className: "p-5 w-[400px]",
            sx: {
              backgroundColor: "#F2F3F5",
              height: "calc(100% - 40px)",
            },
          }}
        >
          <div className="flex justify-between">
            <IconButton
              onClick={() => dispatch(toggleCartOpen(false))}
              aria-label="delete"
              className="mb-5 w-fit"
              sx={{
                padding: 0,
                marginBottom: "5px",
              }}
            >
              <ClearIcon />
            </IconButton>
            <OrderStatusChip status={selectedOrder.status} />
          </div>
          <Cart
            className="h-full"
            order={selectedOrder}
            onCancelOrder={cancelOrder}
            isOrderCanceling={isOrderCanceling}
            onConfirmOrderDelivered={confirmOrderDelivery}
          />
        </Drawer>
      )}
    </>
  );
};

MyOrdersPage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

MyOrdersPage.auth = {
  roles: [UserRoles.RETAIL],
};

export default MyOrdersPage;
