import {NextPageWithLayout} from "~/typing/page";
import ClientLayout from "~/components/layout/client/layout";
import {UserRoles} from "~/models/user";
import {api} from "~/utils/api";
import CatalogueTemplate from "~/components/gift/CatalogueTemplate";

const FavoritesPage: NextPageWithLayout = () => {
  const { data: favorites, isLoading: isLoadingGifts, refetch: refetchFavorites } = api.gifts.getMyFavorites.useQuery();

  return (
    <CatalogueTemplate
      title="Избранное"
      isLoadingCatalogue={isLoadingGifts}
      catalogue={favorites}
      refetchCatalogue={refetchFavorites}
    />
  );
};

FavoritesPage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

FavoritesPage.auth = {
  roles: [UserRoles.RETAIL, UserRoles.SALES],
};

export default FavoritesPage;
