import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import SignUpForm from "~/components/auth/SignUpForm";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import DocumentLayout from "~/components/layout/document/layout";

const InvitePage: NextPageWithLayout = () => {
  const router = useRouter();
  const inviteCode = router.query.code as string;
  const { status, data } = api.users.getByCode.useQuery(
    inviteCode ?? undefined
  );

  return (
    <>
      {status === "loading" ? (
        <Box
          sx={{
            display: "flex",
            width: "100%",
            height: "100vh",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress
            sx={{
              color: "#E51C2F",
            }}
          />
        </Box>
      ) : data ? (
        <div className="flex h-full items-center justify-center">
          <div className="w-[444px]">
            <Typography variant="h4">
              Регистрация
            </Typography>
            <SignUpForm
              initialFormValues={{
                email: data.email,
                role: data.role,
                password: "",
              }}
              code={inviteCode}
            />
          </div>
        </div>
      ) : (
        <div>Такого приглашения не существует</div>
      )}
    </>
  );
};

InvitePage.getLayout = function getLayout(page) {
  return <DocumentLayout>{page}</DocumentLayout>;
};

InvitePage.auth = true;

export default InvitePage;
