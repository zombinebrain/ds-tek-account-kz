import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import Image from "next/image";
import SignInDialog from "~/components/dialogs/SignInDialog";
import Typography from "@mui/material/Typography";
import BaseButton from "~/components/ui/BaseButton";
import DocumentLayout from "~/components/layout/document/layout";
import BaseLoader from "~/components/ui/BaseLoader";

const VerifyPage: NextPageWithLayout = () => {
  const [open, setOpen] = useState(false);
  const router = useRouter();
  const verificationCode = router.query.verificationCode as string;

  const [message, setMessage] = useState("");

  const { mutate } = api.users.verify.useMutation({
    onSuccess() {
      setMessage("success");
    },
    onError(error) {
      setMessage(error.message);
    },
  });

  useEffect(() => {
    verificationCode && mutate(verificationCode);
  }, [verificationCode, mutate]);

  return (
    <div className="flex h-full items-center justify-center">
      {message ? (
        <div className="flex flex-col items-center">
          <Image
            src="/account-verified.svg"
            width={161}
            height={157}
            alt="account-verified icon"
            className="mb-5"
          />
          <Typography variant="subtitle1">
            Поздравляем! Адрес электронной почты был подтвержден.
          </Typography>
          <Typography variant="subtitle1">
            Вы зарегистрированы в Программе Лояльности &quot;FENOX Шоколад&quot;.
          </Typography>
          <BaseButton
            onClick={() => setOpen(true)}
            className="mt-6"
          >
            войти в личный кабинет
          </BaseButton>
        </div>
      ) : <BaseLoader />}
      <SignInDialog open={open} onClose={() => setOpen(false)} />
    </div>
  );
};

VerifyPage.getLayout = function getLayout(page) {
  return <DocumentLayout>{page}</DocumentLayout>;
};

VerifyPage.auth = true;

export default VerifyPage;
