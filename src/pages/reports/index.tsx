import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Typography from "@mui/material/Typography";
import { BonusCampaign, BonusCampaignStatus } from "@prisma/client";
import { useState } from "react";
import BonusCampaignReports from "~/components/BonusCampaignReports";
import { CustomTabPanel, a11yProps } from "~/components/CustomTabPanel";
import BonusCampaignCard from "~/components/cards/BonusCampaignCard";
import BonusCampaignDialog from "~/components/dialogs/BonusCampaignDialog";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import AddIcon from "@mui/icons-material/Add";
import { api } from "~/utils/api";
import { ReportLogs } from "~/components/reports/ReportLogs";
import { BonusConfiguration } from "~/components/reports/BonusConfiguration";
import { BonusCampaignErrors } from "~/models/bonusCampaigns";
import { ErrorType } from "~/models/errors";
import BaseButton from "~/components/ui/BaseButton";
import BaseLoader from "~/components/ui/BaseLoader";

const tabs = [
  {
    label: "Конфигуратор бальной системы",
    key: "config",
  },
  {
    label: "Загрузка отчетов",
    key: "reports",
  },
  {
    label: "Журнал несовпадений",
    key: "logs",
  },
];

const ReportsPage: NextPageWithLayout = () => {
  const [value, setValue] = useState(0);
  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");
  const [selectedCampaign, setSelectedCampaign] =
    useState<BonusCampaign | null>(null);

  const {
    data: campaigns,
    isFetched: isCampaignsFetched,
    refetch: refetchCampaigns,
  } = api.bonusCampaigns.getAll.useQuery();

  const { mutate: createBonusCampaign } = api.bonusCampaigns.create.useMutation(
    {
      onSuccess: async () => {
        setOpen(false);
        await refetchCampaigns();
      },
      onError(error) {
        setError(BonusCampaignErrors[error.message as ErrorType]!);
      },
    }
  );

  const { mutate: updateBonusCampaign, isLoading: isUpdatingBonusCampaign } =
    api.bonusCampaigns.closePeriod.useMutation({
      onSuccess: () => {
        refetchCampaigns()
          .then(() => {
            selectedCampaign &&
              setSelectedCampaign({
                ...selectedCampaign,
                status: BonusCampaignStatus.CLOSED,
              });
          })
          .catch((err) => console.log(err));
      },
    });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <PageHeader title="Бонусные программы" />
      <div className="h-full p-5">
        <BaseButton
          onClick={handleClickOpen}
          className="flex items-center w-[350px]"
        >
          <AddIcon className="mr-2" />
          Добавить программу
        </BaseButton>
        <div className="mt-3 flex h-full">
          {isCampaignsFetched ? (
            <>
              <div className="min-w-[350px]">
                {!!campaigns!.length ? (
                  <div className="flex flex-col gap-3">
                    {campaigns?.map((campaign) => (
                      <BonusCampaignCard
                        key={campaign.id}
                        bonusCampaign={campaign}
                        onBonusCampaignSelect={() => {
                          setSelectedCampaign(campaign);
                        }}
                        className={
                          selectedCampaign?.id === campaign.id
                            ? "border border-solid"
                            : ""
                        }
                      />
                    ))}
                  </div>
                ) : (
                  <Typography variant="subtitle1">
                    Не создано бонусных программ
                  </Typography>
                )}
              </div>
              <div className="ml-5 w-full">
                {selectedCampaign ? (
                  <>
                    <Box>
                      <Tabs value={value} onChange={handleChange}>
                        {tabs.map((tab, index) => (
                          <Tab
                            key={tab.key}
                            label={tab.label}
                            {...a11yProps(index)}
                            className="text-subtitle2 font-bold normal-case"
                          />
                        ))}
                      </Tabs>
                    </Box>
                    {tabs.map((tab, index) => (
                      <CustomTabPanel
                        key={`${tab.key}-tab`}
                        value={value}
                        index={index}
                        className="mt-5"
                      >
                        {value === 0 && (
                          <BonusConfiguration
                            bonus_campaign={selectedCampaign}
                          />
                        )}
                        {value === 1 && (
                          <BonusCampaignReports
                            refetchCampaigns={refetchCampaigns}
                            updateBonusCampaign={updateBonusCampaign}
                            isUpdatingBonusCampaign={isUpdatingBonusCampaign}
                            bonusCampaign={selectedCampaign}
                          />
                        )}
                        {value === 2 && (
                          <ReportLogs bonus_campaign={selectedCampaign} />
                        )}
                      </CustomTabPanel>
                    ))}
                  </>
                ) : (
                  <Typography variant="subtitle1" className="text-center">
                    Выберите бонусную кампанию
                  </Typography>
                )}
              </div>
            </>
          ) : <BaseLoader />}
        </div>
      </div>
      <BonusCampaignDialog
        open={open}
        onClose={handleClose}
        error={error}
        setError={setError}
        createBonusCampaign={createBonusCampaign}
      />
    </>
  );
};

ReportsPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

ReportsPage.auth = {
  roles: [UserRoles.ADMIN],
};

export default ReportsPage;
