import {
  DeleteObjectCommand,
  PutObjectCommand,
  GetObjectCommand,
  S3Client,
} from "@aws-sdk/client-s3";
import formidable from "formidable";
import * as yup from "yup";
import fs from "fs";
import PersistentFile from "formidable/PersistentFile";
import { NextApiRequest, NextApiResponse } from "next";

const REGION = "ru-central1";
const ENDPOINT = "https://storage.yandexcloud.net";

// const s3Client = new S3Client({
//   forcePathStyle: false,
//   region: REGION,
//   endpoint: ENDPOINT,
//   // credentials: {
//   //   accessKeyId: process.env.STORAGE_KEY!,
//   //   secretAccessKey: process.env.STORAGE_SECRET!,
//   // },
// });

type FormFields = {
  bucket?: string;
  key?: string[];
};

type FormFiles = {
  file?: PersistentFile &
    {
      originalFilename: string;
      filepath: string;
    }[];
};

const keyId = process.env.STORAGE_KEY!;
const secret = process.env.STORAGE_SECRET!;

const s3Client = new S3Client({
  endpoint: ENDPOINT,
  region: REGION,
  credentials: {
    accessKeyId: keyId,
    secretAccessKey: secret,
  },
});

const formSchema = yup.object().shape({
  key: yup.array(yup.mixed()).optional(),
  bucket: yup.string().required(),
  file: yup.array(yup.mixed()).required(),
});

async function saveFormData(fields: FormFields, files: FormFiles) {
  const filesToUpload = files.file;
  if (filesToUpload) {
    const urls: string[] = [];
    for (const file of Array.from(filesToUpload)) {
      const buffer = fs.readFileSync(file.filepath);
      const blob = new Blob([buffer]);
      const arrayBuffer = await blob.arrayBuffer();
      const blobBuffer = Buffer.from(arrayBuffer);

      const Key = `${fields.bucket}/${Date.now()}_${file.originalFilename}`;
      const putCommand = new PutObjectCommand({
        ACL: "public-read",
        Bucket: "fenoxbonus",
        Key,
        Body: blobBuffer,
      });
      try {
        await s3Client.send(putCommand);
        urls.push(Key);
      } catch (e) {
        console.log(e);
      }
    }
    return urls;
  }
}

async function deleteFormData(fields: FormFields) {
  const filesToDelete = fields.key;
  if (filesToDelete) {
    for (const fileName of Array.from(filesToDelete)) {
      try {
        const params = {
          Bucket: "fenoxbonus",
          Key: fileName
        };
        // Delete the object from S3
        await s3Client.send(new DeleteObjectCommand(params));
      } catch (e) {
        console.log(e);
      }
    }
  }
}

async function getFormData(fileName: string) {
  try {
    const params = {
      Bucket: "fenoxbonus",
      Key: fileName
    };
    // Get the object from S3
    const res = await s3Client.send(new GetObjectCommand(params));
    return res.Body?.transformToString('base64');
  } catch (e) {
    console.log(e);
  }
}

async function validateFromData(fields: FormFields, files: FormFiles) {
  try {
    await formSchema.validate({ ...fields, ...files });
    return true;
  } catch (e) {
    return false;
  }
}

async function handlePostFormReq(req: NextApiRequest, res: NextApiResponse) {
  const form = formidable({ multiples: true });

  const formData = new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) {
        reject("error");
      }
      resolve({ fields, files });
    });
  });

  try {
    const { fields, files } = (await formData) as {
      fields: FormFields;
      files: FormFiles;
    };
    const filesToUpload = {
      ...files,
      file: !Array.isArray(files.file) ? [files.file] : files.file,
    };
    const isValid = await validateFromData(
      {
        bucket: fields.bucket,
        key: !Array.isArray(fields.key) ? [fields.key] : fields.key,
      } as FormFields,
      filesToUpload as FormFiles
    );
    if (!isValid) throw Error("invalid form schema");

    try {
      const urls = await saveFormData(fields, filesToUpload as FormFiles);
      res.status(200).send({ status: "submitted", urls });
      return;
    } catch (e) {
      res.status(500).send({ status: "something went wrong" });
      return;
    }
  } catch (e) {
    res.status(400).send({ status: "invalid submission" });
    return;
  }
}

async function handleDeleteFormReq(req: NextApiRequest, res: NextApiResponse) {
  const form = formidable({ multiples: true });

  const formData = new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) {
        reject("error");
      }
      resolve({ fields, files });
    });
  });

  try {
    const { fields, files } = (await formData) as {
      fields: FormFields;
      files: FormFiles;
    };

    const filesToUpload = {
      ...files,
      file: !Array.isArray(files.file) ? [files.file] : files.file,
    };

    const isValid = await validateFromData(
      {
        bucket: fields.bucket,
        key: !Array.isArray(fields.key) ? [fields.key] : fields.key,
      } as FormFields,
      filesToUpload as FormFiles
    );
    if (!isValid) throw Error("invalid form schema");

    try {
      await deleteFormData(fields);
      res.status(200).send({ status: "submitted" });
      return;
    } catch (e) {
      res.status(500).send({ status: "something went wrong" });
      return;
    }
  } catch (e) {
    res.status(400).send({ status: "invalid submission" });
    return;
  }
}

async function handleGetFormReq(req: NextApiRequest, res: NextApiResponse) {
  const fileName = req.query.name as string;
  try {
    if (!fileName) {
      throw Error('Invalid file name');
    }
    try {
      const data = await getFormData(fileName);
      res.status(200).send({ status: "submitted", data });
      return;
    } catch (err) {
      res.status(500).send({ status: "something went wrong" });
      return;
    }
  } catch (err) {
    res.status(400).send({ status: err instanceof Error ? err.message : "invalid submission" });
    return;
  }
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method == "POST") {
    await handlePostFormReq(req, res);
  } else if (req.method == "DELETE") {
    await handleDeleteFormReq(req, res);
  } else if (req.method == "GET") {
    await handleGetFormReq(req, res);
  } else {
    res.status(404).send("method not found");
  }
}

export const config = {
  api: {
    bodyParser: false,
  },
};
