import LandingLayout from "~/components/layout/landing/layout";
import {NextPageWithLayout} from "~/typing/page";
import {api} from "~/utils/api";
import BaseLoader from "~/components/ui/BaseLoader";
import SignUpDialog from "~/components/dialogs/SignUpDialog";
import {useState} from "react";
import GalleryGiftCard, {GalleryGiftType} from "~/components/landing/GalleryGiftCard";
import GalleryGiftDialog from "~/components/landing/GalleryGiftDialog";
import Typography from "@mui/material/Typography";
import LandingMoneyGifts from "~/components/landing/LandingMoneyGifts";

const CatalogueGalleryPage: NextPageWithLayout = () => {
  const [isSignUpDialogOpen, setIsSignUpDialogOpen] = useState(false);
  const [isSelectedGiftDialog, setIsSelectedGiftDialog] = useState(false);
  const [selectedGift, setSelectedGift] = useState<GalleryGiftType | null>(null);

  const { data: gifts = [], isLoading } = api.gifts.getGiftGallery.useQuery();

  const handleCloseSelectedGiftDialog = () => {
    setIsSelectedGiftDialog(false);
    setSelectedGift(null);
  };

  return (
    <div className="flex flex-col items-center">
      <section className="mb-25">
        <Typography variant="h2" className="mb-15 text-center">Виртуальные денежные подарки</Typography>
        <LandingMoneyGifts onClick={() => setIsSignUpDialogOpen(true)} />
      </section>
      <section>
        <Typography variant="h2" className="mb-15 text-center">Каталог подарков</Typography>
        {
          isLoading
            ? <BaseLoader />
            : (
              <div className="grid grid-cols-[repeat(4,minmax(0,290px))] gap-10 pb-25">
                {
                  [...gifts].map(gift => (
                    <div
                      className="relative cursor-pointer"
                      key={gift.id}
                      onClick={() => {
                        setSelectedGift(gift);
                        setIsSelectedGiftDialog(true);
                      }}
                    >
                      <GalleryGiftCard
                        key={gift.id}
                        gift={gift}
                        onAddClick={() => setIsSignUpDialogOpen(true)}
                      />
                    </div>
                  ))
                }
              </div>
            )
        }
      </section>
      <SignUpDialog
        open={isSignUpDialogOpen}
        onClose={() => setIsSignUpDialogOpen(false)}
      />
      {
        selectedGift && (
          <GalleryGiftDialog
            isOpen={isSelectedGiftDialog}
            onClose={handleCloseSelectedGiftDialog}
            gift={selectedGift}
          />
        )
      }
    </div>
  );
};


CatalogueGalleryPage.getLayout = function getLayout(page) {
  return <LandingLayout>{page}</LandingLayout>;
};
export default CatalogueGalleryPage;
