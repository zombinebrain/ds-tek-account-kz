import { type Session } from "next-auth";
import { SessionProvider, useSession } from "next-auth/react";
import { AppProps } from "next/app";
import { api } from "~/utils/api";
import "~/styles/globals.css";
import { NextPageProps, NextPageWithLayout } from "~/typing/page";
import { UserRole } from "~/models/user";
import { store } from "~/app/store";
import { Provider } from "react-redux";
import Error403 from "~/components/errors/403";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "~/utils/theme";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import Head from "next/head";
import { StyledEngineProvider } from '@mui/material/styles';
import { BebasNeue, Pragmatica } from "~/utils/fonts";

type AppPropsWithLayout = AppProps<
  NextPageProps & {
    session: Session;
  }
> & {
  Component: NextPageWithLayout;
};

const MyApp = ({
  Component,
  pageProps: { session, ...pageProps },
}: AppPropsWithLayout) => {
  const getLayout = Component.getLayout ?? ((page) => page);

  return (
    <>
      <style global jsx>
        {`
          html, body {
            font-family: ${Pragmatica.style.fontFamily};
          }
        `}
      </style>
      {/* TODO Test if it fixes mui styles overriding tailwind on prod according to this guide https://mui.com/material-ui/guides/interoperability/#tailwind-css */}
      <StyledEngineProvider injectFirst>
        <SessionProvider session={session}>
          <Head>
            <title>Fenox. Кабинет пользователя</title>
          </Head>
          <main id="root" className={`${BebasNeue.variable} ${Pragmatica.variable}`}>
            <Provider store={store}>
              <ThemeProvider theme={theme}>
                <LocalizationProvider dateAdapter={AdapterMoment}>
                    {Component.auth ? (
                      <Auth
                        requiredRoles={
                          typeof Component.auth === "object"
                            ? Component.auth.roles
                            : undefined
                        }
                      >
                        {getLayout(<Component {...pageProps} />)}
                      </Auth>
                    ) : (
                      getLayout(<Component {...pageProps} />)
                    )}
                </LocalizationProvider>
              </ThemeProvider>
            </Provider>
          </main>
        </SessionProvider>
      </StyledEngineProvider>
    </>
  );
};

function Auth({
  children,
  requiredRoles,
}: {
  children: React.ReactNode;
  requiredRoles?: UserRole[];
}) {
  // if `{ required: true }` is supplied, `status` can only be "loading" or "authenticated"
  const { status, data: session } = useSession({ required: true });

  if (status === "loading") {
    return (
      <Box
        sx={{
          display: "flex",
          width: "100%",
          height: "100vh",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <CircularProgress
          sx={{
            color: "#E51C2F",
          }}
        />
      </Box>
    );
  }
  if (
    requiredRoles &&
    requiredRoles.findIndex((role) => session.user.role === role) === -1
  ) {
    return <Error403 />; // Handle unauthorized access
  }

  return children;
}

export default api.withTRPC(MyApp);
