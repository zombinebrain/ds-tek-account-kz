import React, {useState} from "react";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import AllCompaniesTable from "~/components/tables/AllCompaniesTable";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import {a11yProps, CustomTabPanel} from "~/components/CustomTabPanel";
import {BonusConfiguration} from "~/components/reports/BonusConfiguration";
import BonusCampaignReports from "~/components/BonusCampaignReports";
import {ReportLogs} from "~/components/reports/ReportLogs";
import {CompanyType} from "@prisma/client";
import {api} from "~/utils/api";

const tabs = [
  {
    label: "СТО",
    key: CompanyType.STO
  },
  {
    label: "Магазин",
    key: CompanyType.SHOP
  },
  {
    label: "Субдилер",
    key: CompanyType.SUBDEALER
  }
];

const CompaniesPage: NextPageWithLayout = () => {
  const [tabValue, setTabValue] = useState(0);
  const {
    isFetched: isCompaniesFetched,
    data: companies,
    refetch: refetchCompanies
  } = api.companies.getCompaniesByType.useQuery(tabs[tabValue]!.key);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  return (
    <>
      <PageHeader title="РТО" />
      <div className="p-5">
        <Box>
          <Tabs value={tabValue} onChange={handleChange}>
            {tabs.map((tab, index) => (
              <Tab
                key={tab.key}
                label={tab.label}
                {...a11yProps(index)}
                className="text-subtitle2 font-bold normal-case"
              />
            ))}
          </Tabs>
        </Box>
        {tabs.map((tab, index) => (
          <CustomTabPanel
            key={`${tab.key}-tab`}
            value={tabValue}
            index={index}
            className="mt-5"
          >
            <AllCompaniesTable
              isFetched={isCompaniesFetched}
              companies={companies}
            />
          </CustomTabPanel>
        ))}
      </div>
    </>
  );
};

CompaniesPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

CompaniesPage.auth = {
  roles: [UserRoles.ADMIN, UserRoles.MANAGER],
};

export default CompaniesPage;
