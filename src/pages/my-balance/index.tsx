import Typography from "@mui/material/Typography";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import ClientLayout from "~/components/layout/client/layout";
import { UserRoles } from "~/models/user";
import TransactionCard from "~/components/cards/TransactionCard";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { useState } from "react";
import { TransactionWithBonusCampaignOrderDistributorCompany } from "~/models/transactions";
import { TransactionDrawer } from "~/components/drawers/TransactionDrawer";

const MyBalancePage: NextPageWithLayout = () => {
  const { data: myTransactions, isFetched } = api.transactions.getMy.useQuery();

  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedTransaction, selectTransaction] =
    useState<TransactionWithBonusCampaignOrderDistributorCompany | null>(null);

  const setTransaction = (
    transaction: TransactionWithBonusCampaignOrderDistributorCompany
  ) => {
    selectTransaction(transaction);
    setDrawerOpen(true);
  };

  return (
    <>
      <div className="flex flex-col items-center p-5">
        <Typography variant="h2">
          История
        </Typography>
        <div className="mt-5 flex flex-col gap-5">
          {isFetched ? (
            myTransactions?.length ? (
              myTransactions?.map((transaction) => (
                <TransactionCard
                  key={transaction.id}
                  transaction={transaction}
                  onTransactionSelect={() => setTransaction(transaction)}
                />
              ))
            ) : (
              <Typography variant="subtitle1">
                Поступления или списания отсутствуют
              </Typography>
            )
          ) : (
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress
                sx={{
                  color: "#E51C2F",
                }}
              />
            </Box>
          )}
        </div>
      </div>
      {selectedTransaction && (
        <TransactionDrawer
          transactionID={selectedTransaction.id}
          open={drawerOpen}
          onClose={() => setDrawerOpen(false)}
        />
      )}
    </>
  );
};

MyBalancePage.getLayout = (page) => {
  return <ClientLayout>{page}</ClientLayout>;
};

MyBalancePage.auth = {
  roles: [UserRoles.RETAIL],
};

export default MyBalancePage;
