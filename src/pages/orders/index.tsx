import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import moment, {Moment} from "moment";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { CustomTabPanel, a11yProps } from "~/components/CustomTabPanel";
import AdminOrderCard from "~/components/cards/AdminOrderCard";
import DeliveryConfirmationDialog from "~/components/dialogs/DeliveryConfirmationDialog";
import { UserDrawer } from "~/components/drawers/UserDrawer";
import PageHeader from "~/components/layout/PageHeader";
import MainLayout from "~/components/layout/layout";
import {OrderStatuses} from "~/models/order";
import { UserRoles } from "~/models/user";
import { NextPageWithLayout } from "~/typing/page";
import { api } from "~/utils/api";
import BaseButton from "~/components/ui/BaseButton";
import {createAndDownloadXLSX} from "~/utils/createAndDownloadXLSX";
import DatePickerDownloadDialog from "~/components/dialogs/DatePickerDownloadDialog";

const tabs = [
  {
    label: "Оформлен",
    key: OrderStatuses.PENDING,
  },
  {
    label: "На сборке",
    key: OrderStatuses.INPROGRESS,
  },
  {
    label: "В пути",
    key: OrderStatuses.INDELIVERY,
  },
  {
    label: "Доставлен",
    key: OrderStatuses.DELIVERED,
  },
  {
    label: "Отменен",
    key: OrderStatuses.CANCELED,
  },
];

const OrdersPage: NextPageWithLayout = () => {
  const router = useRouter();
  const [tabValue, setTabValue] = useState(0);
  const [orderId, setOrderId] = useState<string | null>(null);
  const [open, setOpen] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [userEmail, setUserEmail] = useState("");
  const [isOpenDownloadDialog, setIsOpenDownloadDialog] = useState(false);
  const [timePeriod, setTimePeriod] = useState<{ to: null | Moment, from: null | Moment }>({ to: null, from: null });

  const { mutate: sendToDelivery } = api.orders.sendToDelivery.useMutation({
    async onSuccess() {
      setOpen(false);
      setOrderId(null);
      await refetchOrders();
    },
  });

  const closeDrawer = () => {
    setDrawerOpen(false);
    setUserEmail("");
  };

  const selectUser = (email: string) => {
    setUserEmail(email);
    setDrawerOpen(true);
  };

  const {
    // isFetched: ordersFetched,
    data: orders,
    refetch: refetchOrders,
  } = api.orders.getALL.useQuery({
    status: tabs[tabValue]!.key,
  });

  const {
    isInitialLoading: isReportOrdersLoading,
    refetch: fetchReportOrders
  } = api.orders.getAllOrdersInTimePeriod.useQuery({
    to: timePeriod.to?.format() ?? null,
    from: timePeriod.from?.format() ?? null
  }, {
    refetchOnWindowFocus: false,
    enabled: false
  });

  const DownloadReport = async () => {
    if (!timePeriod.to || !timePeriod.from) {
      return;
    }
    const res = await fetchReportOrders();
    if (res.data) {
      createAndDownloadXLSX<{ article: string | null, name: string, contact: string, count: number, createdAt: Date, status: string }>(res.data, [
        { key: 'article', name: 'Артикул' },
        { key: 'name', name: 'Наименование' },
        { key: 'count', name: 'Количество' },
        { key: 'contact', name: 'Контактное лицо' },
        { key: 'createdAt', name: 'Дата оформления заказа' },
        { key: 'status', name: 'Статус заказа' }
      ], `Отчёт о заказах ${timePeriod.from.format('DD.MM.YYYY')}-${timePeriod.to.format('DD.MM.YYYY')}`);
    }
  }

  const { mutate: cancelOrder, isLoading: cancelingOrder } =
    api.orders.cancelOrder.useMutation({
      async onSuccess() {
        setOpen(false);
        setOrderId(null);
        await refetchOrders();
      },
    });

  const { mutate: updateOrderStatus, isLoading: updatingStatus } =
    api.orders.updateStatus.useMutation({
      async onSuccess() {
        setOpen(false);
        setOrderId(null);
        await refetchOrders();
      },
    });

  const {mutate: createOrderInvoice, isLoading: isCreatingOrderInvoice} = api.orders.createOrderInvoice.useMutation({
    onSuccess: async () => {
      await refetchOrders();
    },
  });

  const {mutate: updateOrderInvoice, isLoading: isUpdatingOrderInvoice} = api.orders.updateOrderInvoice.useMutation({
    onSuccess: async () => {
      await refetchOrders();
    },
  });

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  const handleDeliveryConfirm = (
    code: string | undefined,
    site: string | undefined,
    deliveryDate: Date | null
  ) => {
    sendToDelivery({
      id: orderId!,
      code,
      site,
      deliveryDate: moment(deliveryDate).toDate(),
    });
  };

  useEffect(() => {
    if (router.query.status) {
      const index = tabs.findIndex((tab) => tab.key === router.query.status);
      if (index !== -1) {
        setTabValue(index);
      }
    }
  }, [router]);

  return (
    <>
      <PageHeader title="Заказы" />
      <div className="p-5">
        <div className="flex justify-between">
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              value={tabValue}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              {tabs.map((tab, index) => (
                <Tab
                  key={tab.key}
                  label={tab.label}
                  {...a11yProps(index)}
                  sx={{ textTransform: "none" }}
                  className="text-subtitle2 font-bold"
                />
              ))}
            </Tabs>
          </Box>
          <BaseButton
            onClick={() => setIsOpenDownloadDialog(true)}
          >
            Скачать отчёт
          </BaseButton>
        </div>
        {tabs.map((tab, index) => (
          <CustomTabPanel key={`${tab.key}-tab`} value={tabValue} index={index}>
            <div className="flex flex-col gap-4 pt-4">
              {orders?.map((order) => (
                <AdminOrderCard
                  key={order.id}
                  order={order}
                  selectUser={selectUser}
                  updatingStatus={updatingStatus}
                  updateOrderStatus={updateOrderStatus}
                  cancelingOrder={cancelingOrder}
                  cancelOrder={cancelOrder}
                  addInvoice={createOrderInvoice}
                  updateInvoice={updateOrderInvoice}
                  onDeliveryConfirmation={() => setOpen(true)}
                  setOrderId={setOrderId}
                />
              ))}
            </div>
          </CustomTabPanel>
        ))}
      </div>
      <DatePickerDownloadDialog
        isOpen={isOpenDownloadDialog}
        onClose={() => setIsOpenDownloadDialog(false)}
        title="Отчёт о заказах"
        timePeriod={timePeriod}
        setTimePeriod={setTimePeriod}
        isDownloading={isReportOrdersLoading}
        onDownload={DownloadReport}
      />
      <DeliveryConfirmationDialog
        open={open}
        onClose={() => setOpen(false)}
        onDeliveryConfirm={handleDeliveryConfirm}
      />
      <UserDrawer
        userEmail={userEmail}
        open={drawerOpen}
        onClose={closeDrawer}
      />
    </>
  );
};

OrdersPage.getLayout = (page) => {
  return <MainLayout>{page}</MainLayout>;
};

OrdersPage.auth = {
  roles: [UserRoles.ADMIN, UserRoles.MANAGER],
};

export default OrdersPage;
