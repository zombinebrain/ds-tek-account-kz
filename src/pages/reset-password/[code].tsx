import { NextPageWithLayout } from "~/typing/page";
import { useRef, useState } from "react";
import { useRouter } from "next/router";
import { api } from "~/utils/api";
import DocumentLayout from "~/components/layout/document/layout";
import BaseLoader from "~/components/ui/BaseLoader";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import { Errors } from "~/models/errors";

export const ResetPasswordPageErrors = {
  [Errors.NOT_FOUND]: "Код восстановления пароля не найден",
};

type InitialValuesType = {
  password?: string;
}

const initialValues: InitialValuesType = {
  password: undefined
};

const ResetPasswordPage: NextPageWithLayout = () => {
  const { query, push } = useRouter();
  const code = typeof query.code === 'string' ? query.code : '';
  const passwordRef = useRef<{ value: string }>(null);

  const [error, setError] = useState<string | null>(null);
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  const { data: user, isLoading, error: verifyError } = api.users.verifyResetCode.useQuery({ code }, {
    enabled: !!code,
  });

  const { mutate: updatePassword, isLoading: isPasswordUpdating } = api.users.resetUserPassword.useMutation({
    onSuccess: async () => {
      await push('/');
    }
  });

  return (
    <div className="grid place-items-center h-full">
      {
        verifyError && (
          <Typography variant="subtitle1" className="mt-2">{ResetPasswordPageErrors[verifyError.message] ?? 'Что-то пошло не так!'}</Typography>
        )
      }
      { isLoading && !verifyError && <BaseLoader /> }
      {
        user && (
            <div className="w-[400px]">
              <Typography variant="h4" className="mb-6">Восстановление пароля</Typography>
              <Formik
                initialValues={initialValues}
                validate={(values) => {
                  const errors: {
                    password?: string;
                  } = {};
                  if (!values.password) {
                    errors.password = "Обязательно к заполнению";
                  } else if (values.password.length < 4) {
                    errors.password = "Пароль должен содержать минимум 4 символа";
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setError("");
                  setSubmitting(false);
                  updatePassword({ id: user.id, password: values.password! });
                }}
              >
                {({ values, handleChange, handleBlur, handleSubmit }) => (
                  <Form
                    onSubmit={handleSubmit}
                    className="mt-5 flex h-full flex-col justify-between"
                  >
                    <FormControl variant="outlined" className="mt-3">
                      <InputLabel
                        htmlFor="outlined-adornment-password"
                        className="text-subtitle1"
                      >
                        Новый пароль
                      </InputLabel>
                      <OutlinedInput
                        autoComplete="new-password"
                        inputRef={passwordRef}
                        id="password"
                        name="password"
                        placeholder="Новый пароль"
                        type={showPassword ? "text" : "password"}
                        value={values.password}
                        onChange={(e) => {
                          handleChange(e);
                          setError("");
                        }}
                        onBlur={handleBlur}
                        endAdornment={
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="toggle password visibility"
                              onClick={handleClickShowPassword}
                              onMouseDown={handleMouseDownPassword}
                              edge="end"
                            >
                              {showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                          </InputAdornment>
                        }
                        label="Новый пароль"
                        className="text-subtitle1"
                      />
                    </FormControl>
                    <ErrorMessage
                      className="mt-2 text-subtitle2 text-red"
                      name="password"
                      component="div"
                    />
                    {
                      error && <Typography variant="subtitle2" className="mt-2 text-red">{error}</Typography>
                    }
                    <BaseLoadingButton
                      className="w-full mt-6"
                      type="submit"
                      loading={isPasswordUpdating}
                    >
                      Восстановить пароль
                    </BaseLoadingButton>
                  </Form>
                )}
              </Formik>
            </div>
          )
      }
    </div>
  );
};


ResetPasswordPage.getLayout = function getLayout(page) {
  return <DocumentLayout>{page}</DocumentLayout>;
};

ResetPasswordPage.auth = true;

export default ResetPasswordPage;
