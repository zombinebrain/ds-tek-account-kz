import { z } from "zod";
import {
  createTRPCRouter,
  protectedProcedure,
  publicProcedure,
  roleProtectedProcedure,
} from "../trpc";
import path from "path";
import {compare, hash} from "bcrypt";
import { mailOptions, sendMail, transporter } from "~/lib/nodemailer";
import { UserRoles, UserStatuses } from "~/models/user";
import { Errors } from "~/models/errors";
import { UserRole } from "@prisma/client";

const iconPath = path.join(process.cwd(), "public", "logo.png");

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    .replace(/[xy]/g, function (c) {
      const r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

function exclude<User, Key extends keyof User>(
  user: User,
  keys: Key[]
): Omit<User, Key> {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const filteredEntries = Object.entries(user).filter(
    ([key]) => !keys.includes(key as Key)
  );
  return Object.fromEntries(filteredEntries) as Omit<User, Key>;
}

export const usersRouter = createTRPCRouter({
  create: publicProcedure
    .input(
      z.object({
        role: z.enum([
          UserRoles.ADMIN,
          UserRoles.MANAGER,
          UserRoles.RETAIL,
          UserRoles.SALES,
        ]),
        email: z.string().email({ message: "Invalid email address" }),
        password: z
          .string()
          .min(4, { message: "Must be 4 or more characters long" }),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const hashedPassword = await hash(input.password, 12);
      const userInDatabase = await ctx.prisma.user.findUnique({
        where: {
          email: input.email,
        },
      });

      let createdUser = null;

      if (!userInDatabase) {
        createdUser = await ctx.prisma.user.create({
          data: {
            email: input.email,
            password: hashedPassword,
            role: input.role,
            status: UserStatuses.INACTIVE,
          }
        });
      } else {
        throw Error(Errors.ALREADY_EXIST);
      }

      createdUser.status === UserStatuses.INACTIVE &&
        (await sendMail(
          "signupNotification",
          {
            ...mailOptions,
            to: input.email,
            subject: "Верификация почты",
            attachments: [
              {
                filename: "logo.png",
                path: iconPath,
                cid: "logo1", //same cid value as in the html img src
              },
            ],
          },
          {
            rulesUrl: `${process.env.BASE_URL}/points-rules`,
            url: `${process.env.BASE_URL}/verify/${createdUser.verificationCode}`,
          }
        ));

      await ctx.prisma.transaction.create({
        data: {
          type: "AUTH",
          user_id: createdUser.id,
          points: 100,
        },
      });
      return exclude(createdUser, ["password", "verificationCode", "inviteCode", "resetCode"]);
    }),
  update: protectedProcedure
    .input(
      z.object({
        name: z.string(),
        phone: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.update({
        where: {
          id: ctx.session.user.id,
        },
        include: {
          companies: true,
          favorites: true
        },
        data: input,
      });
      return exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"]);
    }),
  updateField: protectedProcedure
    .input(
      z.object({
        key: z.enum(["name", "phone"]),
        value: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.update({
        where: {
          id: ctx.session.user.id,
        },
        data: {
          [input.key]: input.value,
        },
      });
      return exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"]);
    }),
  invite: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        role: z.enum([
          UserRoles.ADMIN,
          UserRoles.MANAGER,
          UserRoles.RETAIL,
          UserRoles.SALES,
        ]),
        email: z.string().email({ message: "Invalid email address" }),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.findUnique({
        where: {
          email: input.email,
        },
      });
      let createdUser = null;
      if (!user) {
        createdUser = await ctx.prisma.user.create({
          data: {
            email: input.email,
            role: input.role,
            status: UserStatuses.PENDING,
          },
        });
      } else {
        throw Error(Errors.ALREADY_EXIST);
      }
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      await transporter.sendMail({
        ...mailOptions,
        to: input.email,
        subject: "Приглашение пользователя",
        text: `Хай! Вас пригласили стать пользователем Личного кабинета ДС-ТЕК в роли ${input.role}. Чтобы завершить регистрацию пользователя, укажите пароль, перейдя по ссылке: ${process.env.BASE_URL}/invite/${createdUser.inviteCode}`,
      });
      return;
    }),
  getUserByEmail: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        email: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const user = ctx.prisma.user.findUnique({
        where: {
          email: input.email,
        },
        include: {
          companies: {
            include: {
              companyDistributors: {
                include: {
                  distributor: true,
                },
              },
            },
          },
        },
      });
      return user;
    }),
  getALL: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        role: z.enum([UserRole.RETAIL, UserRole.SALES, UserRole.MANAGER]),
      })
    )
    .query(async ({ input, ctx }) => {
      const where =
        input.role === UserRole.MANAGER
          ? {
              OR: [
                {
                  role: UserRole.MANAGER,
                },
                {
                  role: UserRole.ADMIN,
                },
              ],
            }
          : {
              role: input.role,
            };
      const users = ctx.prisma.user.findMany({
        where,
        include: {
          companies: {
            select: {
              id: true,
            },
          },
        },
        orderBy: {
          createdAt: "desc",
        },
      });
      return users;
    }),
  verify: publicProcedure.input(z.string()).mutation(async ({ input, ctx }) => {
    const user = await ctx.prisma.user.update({
      where: {
        verificationCode: input,
      },
      data: {
        status: UserStatuses.ACTIVE,
      },
    });
    return exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"]);
  }),
  activate: publicProcedure
    .input(
      z.object({
        inviteCode: z.string(),
        password: z
          .string()
          .min(4, { message: "Must be 4 or more characters long" }),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const hashedPassword = await hash(input.password, 12);
      const user = await ctx.prisma.user.update({
        where: {
          inviteCode: input.inviteCode,
        },
        data: {
          status: UserStatuses.ACTIVE,
          password: hashedPassword,
        },
      });
      return exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"]);
    }),
  getByCode: publicProcedure.input(z.string()).query(async ({ input, ctx }) => {
    const user = await ctx.prisma.user.findUnique({
      where: {
        inviteCode: input,
      },
    });
    return (
      user && exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"])
    );
  }),
  getMe: protectedProcedure.query(async ({ ctx }) => {
    const user = await ctx.prisma.user.findUnique({
      where: {
        id: ctx.session.user.id,
      },
      include: {
        companies: true,
        favorites: true
      },
    });
    return (
      user && exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"])
    );
  }),
  getUserRole: publicProcedure
    .input(
      z.object({
        email: z.string(),
        password: z.string()
      })
    )
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.findUnique({
        where: {
          email: input.email,
        },
      });
      const isValidPassword = user && await compare(input.password, user.password!);
      if (!user || !isValidPassword) {
        throw Error(Errors.NOT_FOUND);
      } else {
        if (user.status === "ACTIVE") {
          return user.role;
        }
        if (user.status === "INACTIVE") {
          throw Error(Errors.NOT_VERIFIED);
        }
        if (user.status === "DELETED") {
          throw Error(Errors.DELETED);
        }
      }
    }),
  getCount: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER]).query(
    async ({ ctx }) => {
      const users = await ctx.prisma.user.findMany();
      return {
        all: users.length,
        retail: users.filter((user) => user.role === UserRoles.RETAIL).length,
        sales: users.filter((user) => user.role === UserRoles.SALES).length,
      };
    }
  ),
  deleteUser: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER])
    .input(
      z.object({
        id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.update({
        where: {
          id: input.id,
        },
        data: {
          status: "DELETED",
        },
      });
      return exclude(user, ["password", "verificationCode", "inviteCode", "resetCode"]);
    }),
  forgotPassword: publicProcedure
    .input(
      z.object({
        email: z.string()
      })
    )
    .mutation(async ({ input, ctx }) => {
      const code = uuidv4();
      try {
        const user = await ctx.prisma.user.update({
          where: {
            email: input.email
          },
          data: {
            resetCode: code
          }
        });
        if (user?.resetCode) {
          (await sendMail(
            "forgotPasswordVerification",
            {
              ...mailOptions,
              to: input.email,
              subject: "Восстановление пароля",
              attachments: [
                {
                  filename: "logo.png",
                  path: iconPath,
                  cid: "logo1", //same cid value as in the html img src
                },
              ],
            },
            {
              url: `${process.env.BASE_URL}/reset-password/${code}`,
            }
          ));
        }
      } catch {
        throw Error(Errors.NOT_FOUND);
      }
    }),
  verifyResetCode: publicProcedure
    .input(
      z.object({ code: z.string() })
    )
    .query(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.findUnique({
        where: {
          resetCode: input.code
        }
      });
      if (user) {
        return { id: user.id };
      } else {
        throw Error(Errors.NOT_FOUND);
      }
    }),
  resetUserPassword: publicProcedure
    .input(
      z.object({ id: z.string(), password: z.string() })
    )
    .mutation(async ({ input, ctx }) => {
      const hashedPassword = await hash(input.password, 12);
      await ctx.prisma.user.update({
        where: {
          id: input.id
        },
        data: {
          resetCode: null,
          password: hashedPassword
        }
      });
    })
});
