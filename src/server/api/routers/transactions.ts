import { z } from "zod";
import { v4 } from "uuid";
import {
  createTRPCRouter,
  protectedProcedure,
  roleProtectedProcedure,
} from "../trpc";
import { UserRoles } from "~/models/user";
import { TransactionType } from "@prisma/client";
import {Errors} from "~/models/errors";
import {calculateTotalPoints} from "~/utils/transactions";
import path from "path";
import {mailOptions, sendMail} from "~/lib/nodemailer";

const iconPath = path.join(process.cwd(), "public", "logo.png");

export const transactionsRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        type: z.enum(["CREDIT", "DEBIT"]),
        bonus_campaign_id: z.string(),
        distributor_id: z.string(),
        distributor_email: z.string(),
        data: z.array(
          z.object({
            inn: z.string().or(z.number()),
            article: z.string(),
            count: z.number(),
          })
        ),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const promises = [];
      const uniqueCompanyIdentifiers: string[] = [];

      const transactionItemsToDelete = [];
      const transactionsToDelete = [];

      const uploadedTransactions = await ctx.prisma.transaction.findMany({
        where: {
          AND: [
            { bonus_campaign_id: input.bonus_campaign_id },
            { distributor_id: input.distributor_id },
          ],
        },
      });

      for (const transaction of uploadedTransactions) {
        transactionItemsToDelete.push(
          ctx.prisma.transactionItem.deleteMany({
            where: {
              transaction_id: transaction.id,
            },
          })
        );
      }

      await Promise.all(transactionItemsToDelete);

      for (const transaction of uploadedTransactions) {
        transactionsToDelete.push(
          ctx.prisma.transaction.delete({
            where: {
              id: transaction.id,
            },
          })
        );
      }

      await Promise.all(transactionsToDelete);

      await ctx.prisma.transaction.deleteMany({
        where: {
          AND: [
            { bonus_campaign_id: input.bonus_campaign_id },
            { distributor_id: input.distributor_id },
          ],
        },
      });

      input.data.forEach((item) => {
        if (!uniqueCompanyIdentifiers.includes(item.inn.toString())) {
          uniqueCompanyIdentifiers.push(item.inn.toString());
        }
      });

      for (const identifier of uniqueCompanyIdentifiers) {
        const company = await ctx.prisma.company.findUnique({
          where: {
            inn: identifier,
          },
        });

        const transactionID = v4();

        const filteredArticles = input.data.filter(
          (item) => item.inn.toString() === identifier
        );

        const transactionItems = filteredArticles.map((item) => ({
          id: v4(),
          count: item.count,
          article: item.article,
        }));

        company &&
          promises.push(
            ctx.prisma.transaction.create({
              data: {
                id: transactionID,
                type: input.type,
                bonus_campaign_id: input.bonus_campaign_id,
                distributor_id: input.distributor_id,
                company_id: company?.id,
                points: 0,
                user_id: company.userId,
                transaction_items: {
                  create: transactionItems,
                },
              },
              include: {
                transaction_items: true,
              },
            })
          );
      }

      const bonusCampaign = await ctx.prisma.bonusCampaign.findUnique({
        where: {
          id: input.bonus_campaign_id,
        },
      });

      await sendMail(
        "distributorReportUploadedNotification",
        {
          ...mailOptions,
          to: input.distributor_email,
          subject: "Загрузка отчёта дистрибьютора",
          attachments: [
            {
              filename: "logo.png",
              path: iconPath,
              cid: "logo1", //same cid value as in the html img src
            },
          ],
        },
        {
          datePeriod: `${bonusCampaign!.startDate} - ${bonusCampaign?.endDate}`
        }
      );

      let response = null;
      await Promise.all(promises).then((res) => {
        response = res;
      });

      return response;
    }),

  getFullTransaction: protectedProcedure
    .input(z.object({ id: z.string() }))
    .query(async ({ input, ctx }) => {
      const transaction = await ctx.prisma.transaction.findUnique({
        where: {
          id: input.id,
        },
        include: {
          bonus_campaign: true,
          company: true,
          distributor: true,
          order: {
            include: {
              orderGifts: {
                include: {
                  gift: true,
                },
              },
            },
          },
          transaction_items: true,
        },
      });
      return transaction;
    }),

  getTransactionItems: roleProtectedProcedure([UserRoles.ADMIN])
    .input(z.object({ bonusCampaignID: z.string() }))
    .query(async ({ input, ctx }) => {
      const transaction_items = await ctx.prisma.transactionItem.findMany({
        where: {
          transaction: {
            bonus_campaign_id: input.bonusCampaignID,
          },
        },
        include: {
          transaction: {
            include: {
              distributor: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });
      return transaction_items;
    }),
  getDistributorReportStatuses: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        bonusCampaignID: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const distributors = await ctx.prisma.distributor.findMany({
        include: {
          transactions: {
            where: {
              bonus_campaign_id: input.bonusCampaignID,
            },
          },
        },
      });
      return distributors;
    }),
  getMy: protectedProcedure.query(async ({ ctx }) => {
    const transactions = await ctx.prisma.transaction.findMany({
      where: {
        user_id: ctx.session.user.id,
        OR: [
          {
            type: TransactionType.CREDIT,
            bonus_campaign: {
              status: "CLOSED",
            },
          },
          {
            type: TransactionType.AUTH,
          },
          {
            type: TransactionType.DEBIT,
          },
          {
            type: TransactionType.CARD,
          },
          {
            type: TransactionType.PHONE,
          },
        ],
      },
      include: {
        company: true,
        distributor: true,
        bonus_campaign: true,
        order: true,
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return transactions;
  }),
  getLatest: protectedProcedure.query(async ({ ctx }) => {
    const transactions = await ctx.prisma.transaction.findMany({
      where: {
        user_id: ctx.session.user.id,
        OR: [
          {
            type: TransactionType.CREDIT,
            bonus_campaign: {
              status: "CLOSED",
            },
          },
          {
            type: TransactionType.AUTH,
          },
          {
            type: TransactionType.DEBIT,
          },
          {
            type: TransactionType.CARD,
          },
          {
            type: TransactionType.PHONE,
          },
        ],
      },
      include: {
        distributor: true,
        bonus_campaign: true,
        order: true,
      },
      orderBy: {
        createdAt: "desc",
      },
      take: 3,
    });
    return transactions;
  }),
  getMyBalance: protectedProcedure.query(async ({ ctx }) => {
    const transactions = await ctx.prisma.transaction.findMany({
      where: {
        user_id: ctx.session.user.id,
        OR: [
          {
            type: TransactionType.CREDIT,
            bonus_campaign: {
              status: "CLOSED",
            },
          },
          {
            type: TransactionType.AUTH,
          },
          {
            type: TransactionType.DEBIT,
          },
          {
            type: TransactionType.CARD,
          },
          {
            type: TransactionType.PHONE,
          },
        ],
      },
    });

    return calculateTotalPoints(transactions);
  }),

  getUserBalanceByEmail: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        email: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const user = await ctx.prisma.user.findUnique({
        where: {
          email: input.email,
        },
        select: {
          id: true,
        },
      });
      const transactions = await ctx.prisma.transaction.findMany({
        where: {
          user_id: user?.id,
          OR: [
            {
              type: TransactionType.CREDIT,
              bonus_campaign: {
                status: "CLOSED",
              },
            },
            {
              type: TransactionType.AUTH,
            },
            {
              type: TransactionType.DEBIT,
            },
            {
              type: TransactionType.CARD,
            },
            {
              type: TransactionType.PHONE,
            }
          ],
        },
      });

      return calculateTotalPoints(transactions);
    }),

  createMoneyGiftTransaction: roleProtectedProcedure([UserRoles.RETAIL])
    .input(
      z.object({
        points: z.number(),
        type: z.enum([
          TransactionType.PHONE,
          TransactionType.CARD
        ])
      })
    )
    .mutation(async ({ input, ctx }) => {
      const transactions = await ctx.prisma.transaction.findMany({
        where: {
          user_id: ctx.session.user.id,
          OR: [
            {
              type: TransactionType.CREDIT,
              bonus_campaign: {
                status: "CLOSED",
              },
            },
            {
              type: TransactionType.CARD,
            },
            {
              type: TransactionType.PHONE,
            },
            {
              type: TransactionType.DEBIT,
            },
            {
              type: TransactionType.AUTH,
            },
          ],
        },
      });

      const hasSomeCreditTransaction = !!transactions.filter(
        (transaction) => transaction.type === "CREDIT"
      ).length;

      if (hasSomeCreditTransaction) {
        const userBalance = calculateTotalPoints(transactions);

        if (userBalance > input.points) {
          const createdTransaction = await ctx.prisma.transaction.create({
            data: {
              type: input.type,
              user_id: ctx.session.user.id,
              points: input.points,
            },
          });

          return createdTransaction;
        } else {
          throw Error(Errors.NOT_ENOUGH_POINTS);
        }
      } else {
        throw Error(Errors.CREDIT_TRANSACTION_REQUIRED);
      }
    }),

  getMoneyGiftTransactions: roleProtectedProcedure([UserRoles.ADMIN])
    .query(async ({ ctx }) => {
      const transactions = await ctx.prisma.transaction.findMany({
        where: {
          OR: [
            {
              type: TransactionType.CARD,
            },
            {
              type: TransactionType.PHONE,
            },
          ]
        },
        include: {
          user: {
            select: {
              email: true
            }
          }
        }
      });
      return transactions;
    }),

  getMoneyGiftTransactionsByDate: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        from: z.string().or(z.null()),
        to: z.string().or(z.null())
      })
    )
    .query(async ({ input, ctx }) => {
      if (input.from && input.to) {
        const transactions = await ctx.prisma.transaction.findMany({
          where: {
            createdAt: {
              lte: new Date(input.to),
              gte: new Date(input.from)
            },
            OR: [
              {
                type: TransactionType.CARD,
              },
              {
                type: TransactionType.PHONE,
              },
            ]
          },
          include: {
            user: {
              select: {
                name: true,
                email: true
              }
            }
          },
          orderBy: {
            createdAt: "asc",
          },
        });
        const giftText = {
          [TransactionType.CARD]: 'Деньги на карту',
          [TransactionType.PHONE]: 'Деньги на телефон',
        };

        return transactions.map((item) => ({
          name: giftText[item.type as keyof typeof giftText],
          points: item.points,
          contact: item.user.name || item.user.email,
          createdAt: item.createdAt
        }));
      }
    })
});
