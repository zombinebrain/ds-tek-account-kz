import { z } from "zod";
import {
  createTRPCRouter,
  publicProcedure,
  roleProtectedProcedure,
} from "../trpc";
import { UserRoles } from "~/models/user";
import { SupportContactType } from "@prisma/client";

export const supportContactsRouter = createTRPCRouter({
  updateContactList: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.array(
        z.object({
          type: z.enum([SupportContactType.EMAIL, SupportContactType.PHONE]),
          value: z.string(),
        })
      )
    )
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.supportContact.deleteMany({});
      const contacts = await ctx.prisma.supportContact.createMany({
        data: input,
      });
      return contacts;
    }),
  getALL: publicProcedure.query(async ({ ctx }) => {
    const contacts = ctx.prisma.supportContact.findMany();
    return contacts;
  }),
});
