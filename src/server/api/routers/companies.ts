import { z } from "zod";
import { createTRPCRouter, roleProtectedProcedure } from "../trpc";
import { UserRoles } from "~/models/user";
import { CompanyType, Prisma } from "@prisma/client";
import {OrderStatuses} from "~/models/order";

export const companiesRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.RETAIL])
    .input(
      z.object({
        inn: z
          .string()
          .min(4, { message: "Must be 4 or more characters long" }),
        name: z
          .string()
          .min(4, { message: "Must be 4 or more characters long" }),
        address: z
          .string()
          .min(4, { message: "Must be 4 or more characters long" }),
        distributors: z.array(z.string()),
        type: z.enum([CompanyType.STO, CompanyType.SHOP, CompanyType.SUBDEALER]),
      })
    )
    .mutation(async ({ input, ctx }) => {
      try {
        const company = await ctx.prisma.company.create({
          data: {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            userId: ctx.session.user.id,
            inn: input.inn,
            name: input.name,
            address: input.address,
            type: input.type,
            companyDistributors: {
              create: input.distributors.map((id) => ({
                distributor: {
                  connect: { id },
                },
              })),
            }
          },
        });
        return company;
      } catch (e) {
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
          if (e.code === "P2002") {
            throw new Error("UNIQUE_CONSTRAINT_VIOLATION");
          }
        }
        throw e;
      }
    }),
  getMyCompanies: roleProtectedProcedure([UserRoles.RETAIL]).query(async ({ ctx }) => {
    const companies = ctx.prisma.company.findMany({
      where: {
        userId: ctx.session.user.id,
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return companies;
  }),
  getMyCompaniesWithDistributors: roleProtectedProcedure([UserRoles.RETAIL]).query(async ({ ctx }) => {
    const companies = ctx.prisma.company.findMany({
      where: {
        userId: ctx.session.user.id,
      },
      include: {
        companyDistributors: {
          include: {
            distributor: true,
          },
        },
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return companies;
  }),
  delete: roleProtectedProcedure([UserRoles.RETAIL, UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.companyDistributor.deleteMany({
        where: {
          companyId: input.id,
        },
      });
      const company = await ctx.prisma.company.delete({
        where: {
          id: input.id,
        },
      });
      return company;
    }),
  deleteCompanyDistributor: roleProtectedProcedure([UserRoles.RETAIL])
    .input(
      z.object({
        companyId: z.string(),
        distributorId: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const companyDistributor = await ctx.prisma.companyDistributor.delete({
        where: {
          companyId_distributorId: {
            companyId: input.companyId,
            distributorId: input.distributorId,
          },
        },
      });
      return companyDistributor;
    }),
  getMyCount: roleProtectedProcedure([UserRoles.RETAIL]).query(
    async ({ ctx }) => {
      const companies = await ctx.prisma.company.findMany({
        where: {
          userId: ctx.session.user.id,
        },
      });
      return companies.length;
    }
  ),
  getCompaniesByType: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER])
    .input(
      z.enum([
        CompanyType.STO,
        CompanyType.SHOP,
        CompanyType.SUBDEALER
      ]),
    )
    .query(
    async ({ input, ctx }) => {
      const companies = ctx.prisma.company.findMany({
        where: {
          type: input
        },
        orderBy: {
          createdAt: "desc",
        },
      });
      return companies;
    }
  ),
  getCount: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER]).query(
    async ({ ctx }) => {
      const companies = await ctx.prisma.company.findMany();
      return {
        all: companies.length,
        shops: companies.filter((company) => company.type === CompanyType.SHOP)
          .length,
        serviceStations: companies.filter(
          (company) => company.type === CompanyType.STO
        ).length,
        subdealers: companies.filter(
          (company) => company.type === CompanyType.SUBDEALER
        ).length,
      };
    }
  ),
});
