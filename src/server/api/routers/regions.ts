import { z } from "zod";
import {
  createTRPCRouter,
  protectedProcedure,
  roleProtectedProcedure,
} from "../trpc";
import { UserRoles } from "~/models/user";

export const regionsRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        name: z.string(),
        code: z.string(),
        points: z.number(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const region = await ctx.prisma.region.create({
        data: input,
      });
      return region;
    }),
  update: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        name: z.string(),
        code: z.string(),
        points: z.number(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const region = await ctx.prisma.region.update({
        where: {
          id: input.id,
        },
        data: {
          name: input.name,
          code: input.code,
          points: input.points,
        },
      });
      return region;
    }),
  delete: roleProtectedProcedure([UserRoles.ADMIN])
    .input(z.string())
    .mutation(async ({ input, ctx }) => {
      const region = ctx.prisma.region.delete({
        where: {
          id: input,
        },
      });
      return region;
    }),
  getALL: protectedProcedure.query(async ({ ctx }) => {
    const regions = ctx.prisma.region.findMany();
    return regions;
  }),
});
