import { z } from "zod";
import {
  createTRPCRouter,
  protectedProcedure, publicProcedure,
  roleProtectedProcedure,
} from "../trpc";
import path from "path";
import { UserRoles } from "~/models/user";
import * as xlsx from "xlsx";
import { mailOptions, sendMail } from "~/lib/nodemailer";
import { Prisma } from "@prisma/client";
import moment from "moment";

const iconPath = path.join(process.cwd(), "public", "logo.png");

export const distributorsRouter = createTRPCRouter({
  sendInnList: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        distributor_id: z.string(),
        bonus_campaign_id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const bonusCampaign = await ctx.prisma.bonusCampaign.findUnique({
        where: {
          id: input.bonus_campaign_id,
        },
      });
      const distributor = await ctx.prisma.distributor.findUnique({
        where: {
          id: input.distributor_id,
        },
        include: {
          companyDistributors: {
            include: {
              company: true,
            },
          },
        },
      });
      const innList =
        distributor?.companyDistributors.map(
          (companyDistributor) => companyDistributor.company.inn
        ) ?? [];

      const data = innList.map((value) => [value]);

      // Создайте XLSX-лист из данных
      const worksheet = xlsx.utils.aoa_to_sheet(data);

      // Создайте XLSX-книгу и добавьте лист к ней
      const workbook = xlsx.utils.book_new();
      xlsx.utils.book_append_sheet(workbook, worksheet, "Sheet1");

      // Convert the workbook to a buffer
      const buffer = xlsx.write(workbook, { type: "buffer", bookType: "xlsx" });

      await sendMail(
        "innListNotification",
        {
          ...mailOptions,
          to: distributor?.email,
          subject: "Список ИНН для отчёта",
          attachments: [
            {
              filename: "logo.png",
              path: iconPath,
              cid: "logo1", //same cid value as in the html img src
            },
            {
              filename: "output.xlsx", // Название файла, как будет видно в письме
              content: buffer,
            },
          ],
        },
        {
          datePeriod: `${bonusCampaign!.startDate} - ${bonusCampaign?.endDate}`,
          deadline: moment(bonusCampaign!.endDate, "DD.MM.YYYY")
            .add(5, "d")
            .format("DD.MM.YYYY"),
        }
      );

      const existingInnList = await ctx.prisma.innList.findUnique({
        where: {
          bonus_campaign_id_distributor_id: {
            bonus_campaign_id: input.bonus_campaign_id,
            distributor_id: input.distributor_id,
          },
        },
      });

      !!existingInnList &&
        (await ctx.prisma.innList.delete({
          where: {
            bonus_campaign_id_distributor_id: {
              bonus_campaign_id: input.bonus_campaign_id,
              distributor_id: input.distributor_id,
            },
          },
        }));

      const createdInnList = await ctx.prisma.innList.create({
        data: {
          bonus_campaign_id: input.bonus_campaign_id,
          distributor_id: input.distributor_id,
        },
      });
      return createdInnList;
    }),
  create: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        name: z.string(),
        website_link: z.string(),
        legal_name: z.string(),
        contact_name: z.string(),
        image: z.string().optional(),
        city: z.string(),
        email: z.string(),
        contact_phone: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      let distributor = null;
      try {
        distributor = await ctx.prisma.distributor.create({
          data: input,
        });
      } catch (e) {
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
          // The .code property can be accessed in a type-safe manner
          if (e.code === "P2002") {
            throw new Error("UNIQUE_CONSTRAINT_VIOLATION");
          }
        }
        throw e;
      }
      return distributor;
    }),
  update: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        name: z.string(),
        website_link: z.string(),
        legal_name: z.string(),
        contact_name: z.string(),
        image: z.string().optional(),
        city: z.string(),
        email: z.string(),
        contact_phone: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const { id, ...rest } = input;
      const distributor = await ctx.prisma.distributor.update({
        where: {
          id,
        },
        data: rest,
      });
      return distributor;
    }),
  delete: roleProtectedProcedure([UserRoles.ADMIN])
    .input(z.string())
    .mutation(async ({ input, ctx }) => {
      const distributor = ctx.prisma.distributor.delete({
        where: {
          id: input,
        },
      });
      return distributor;
    }),
  getAll: protectedProcedure.query(async ({ ctx }) => {
    const distributors = ctx.prisma.distributor.findMany({
      /*      include: {
        companyDistributors: true,
      },*/
      orderBy: {
        createdAt: "desc",
      },
    });
    return distributors;
  }),
  getAllForLanding: publicProcedure
    .input(z.number().optional())
    .query(async ({ ctx, input }) => {
    const distributors = ctx.prisma.distributor.findMany({
      ...(input ? { take: input } : {}),
      select: {
        id: true,
        website_link: true,
        name: true,
        legal_name: true,
        image: true
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return distributors;
  }),
});
