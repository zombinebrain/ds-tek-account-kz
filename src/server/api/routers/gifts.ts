import { z } from "zod";
import {
  createTRPCRouter,
  protectedProcedure, publicProcedure,
  roleProtectedProcedure,
} from "../trpc";
import { UserRoles } from "~/models/user";

export const giftsRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        name: z.string(),
        description: z.string(),
        images: z.array(z.string()),
        article: z.string().optional(),
        points: z.number(),
        visible: z.boolean(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const gift = await ctx.prisma.gift.create({
        data: {
          name: input.name,
          description: input.description,
          article: input.article,
          points: input.points,
          visible: input.visible,
          images: {
            createMany: {
              data: input.images.map((image) => ({
                url: image,
              })),
            },
          },
        },
      });
      return gift;
    }),
  update: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        name: z.string(),
        description: z.string(),
        images: z.array(z.string()),
        article: z.string().or(z.null()).optional(),
        points: z.number(),
        visible: z.boolean(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.giftImage.deleteMany({
        where: {
          giftId: input.id,
        },
      });
      const gift = await ctx.prisma.gift.update({
        where: {
          id: input.id,
        },
        data: {
          name: input.name,
          description: input.description,
          article: input.article,
          points: input.points,
          visible: input.visible,
          images: {
            createMany: {
              data: input.images.map((image) => ({
                url: image,
              })),
            },
          },
        },
      });
      return gift;
    }),
  delete: roleProtectedProcedure([UserRoles.ADMIN])
    .input(z.string())
    .mutation(async ({ input, ctx }) => {
      await ctx.prisma.giftImage.deleteMany({
        where: {
          giftId: input,
        },
      });
      const gift = ctx.prisma.gift.delete({
        where: {
          id: input,
        },
      });
      return gift;
    }),
  getALL: roleProtectedProcedure([UserRoles.ADMIN]).query(async ({ ctx }) => {
    const gifts = ctx.prisma.gift.findMany({
      orderBy: {
        createdAt: "desc",
      },
      include: {
        images: {
          select: {
            id: true,
            url: true,
          },
        },
      },
    });
    return gifts;
  }),
  getAvailable: protectedProcedure.query(async ({ ctx }) => {
    const gifts = ctx.prisma.gift.findMany({
      where: {
        visible: true,
      },
      include: {
        favorites: {
          where: {
            userId: ctx.session.user.id,
          },
        },
        images: {
          select: {
            id: true,
            url: true,
          },
        },
      },
      orderBy: {
        createdAt: "asc",
      },
    });
    return gifts;
  }),
  addToFavorites: roleProtectedProcedure([UserRoles.RETAIL])
    .input(z.string())
    .mutation(async ({ input, ctx }) => {
      const giftOnUser = await ctx.prisma.giftsOnUsers.create({
        data: {
          giftId: input,
          userId: ctx.session.user.id,
        },
      });
      return giftOnUser;
    }),
  deleteFromFavorites: roleProtectedProcedure([UserRoles.RETAIL])
    .input(z.string())
    .mutation(async ({ input, ctx }) => {
      const giftOnUser = await ctx.prisma.giftsOnUsers.delete({
        where: {
          userId_giftId: {
            giftId: input,
            userId: ctx.session.user.id,
          },
        },
      });
      return giftOnUser;
    }),
  getMyFavorites: roleProtectedProcedure([UserRoles.RETAIL]).query(
    async ({ ctx }) => {
      const gifts = await ctx.prisma.gift.findMany({
        include: {
          favorites: {
            where: {
              userId: ctx.session.user.id,
            },
          },
          images: {
            select: {
              id: true,
              url: true,
            },
          },
        },
        where: {
          favorites: {
            some: {
              userId: ctx.session.user.id,
            },
          },
        },
      });
      return gifts;
    }
  ),
  getGiftGallery: publicProcedure
    .input(z.number().optional())
    .query(
    async ({ ctx, input }) => {
      const gifts = await ctx.prisma.gift.findMany({
        ...(input ? { take: input } : {}),
        select: {
          id: true,
          images: true,
          name: true,
          description: true
        },
        orderBy: {
          createdAt: "asc",
        },
      });
      return gifts;
    }
  )
});
