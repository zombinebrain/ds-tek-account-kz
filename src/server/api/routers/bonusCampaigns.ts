import { z } from "zod";
import {
  createTRPCRouter,
  protectedProcedure,
  roleProtectedProcedure,
} from "../trpc";
import path from "path";
import { UserRoles } from "~/models/user";
import { Prisma, Transaction, TransactionItem, User } from "@prisma/client";
import { mailOptions, sendMail } from "~/lib/nodemailer";

const iconPath = path.join(process.cwd(), "public", "logo.png");

export const bonusCampaignsRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        name: z.string(),
        startDate: z.string(),
        endDate: z.string(),
        status: z.enum(["ACTIVE", "CLOSED"]).optional(), // Укажите все возможные значения статуса
      })
    )
    .mutation(async ({ input, ctx }) => {
      let bonusCampaign = null;
      try {
        bonusCampaign = await ctx.prisma.bonusCampaign.create({
          data: input,
        });
      } catch (e) {
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
          // The .code property can be accessed in a type-safe manner
          if (e.code === "P2002") {
            throw new Error("UNIQUE_CONSTRAINT_VIOLATION");
          }
        }
        throw e;
      }
      return bonusCampaign;
    }),
  closePeriod: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        name: z.string().optional(),
        status: z.enum(["ACTIVE", "CLOSED"]).optional(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const promises = [];
      const transactions = await ctx.prisma.transaction.findMany({
        where: {
          bonus_campaign_id: input.id,
        },
        include: {
          user: true,
          transaction_items: true,
        },
      });
      const mailList = transactions.map(
        (transaction) => transaction.user.email
      );
      const uniqueMailList = mailList.reduce((accumulator: string[], mail) => {
        if (!accumulator.some((item) => item === mail)) {
          accumulator.push(mail);
        }
        return accumulator;
      }, []);

      const foundBonusCampaign = await ctx.prisma.bonusCampaign.findUnique({
        where: {
          id: input.id,
        },
      });

      for (const transaction of transactions) {
        const calculatePoints = async (transactionItems: TransactionItem[]) => {
          let points = 0;

          for (const item of transactionItems) {
            const article = await ctx.prisma.article.findFirst({
              where: {
                AND: [
                  { bonus_campaign_id: transaction.bonus_campaign_id! },
                  { code: item.article },
                ],
              },
            });

            if (article) {
              const articlePoints = article.points * item.count;
              points += articlePoints;
            }
          }

          return points;
        };

        const points = await calculatePoints(transaction.transaction_items);

        promises.push(
          ctx.prisma.transaction.update({
            where: {
              id: transaction.id,
            },
            data: {
              points,
            },
            include: {
              user: true,
            },
          })
        );
      }

      type TransactionWithUser = Transaction & {
        user: User;
      };

      let response: TransactionWithUser[] = [];
      await Promise.all(promises).then((res) => {
        response = res;
      });

      uniqueMailList.forEach(async (mail) => {
        const userTransactions = response.filter(
          (transaction) => transaction.user.email === mail
        );
        const pointsAdded = userTransactions.reduce((acc, transaction) => {
          return acc + transaction.points;
        }, 0);

        await sendMail(
          "bonusPointsAddedNotification",
          {
            ...mailOptions,
            to: mail,
            subject: "Начисление баллов",
            attachments: [
              {
                filename: "logo.png",
                path: iconPath,
                cid: "logo1", //same cid value as in the html img src
              },
            ],
          },
          {
            points: pointsAdded.toString(),
            datePeriod: `${foundBonusCampaign!.startDate} - ${
              foundBonusCampaign?.endDate
            }`,
            url: `${process.env.BASE_URL}`,
          }
        );
      });

      const bonusCampaign = await ctx.prisma.bonusCampaign.update({
        where: {
          id: input.id,
        },
        data: {
          name: input.name,
          status: input.status,
        },
      });
      return bonusCampaign;
    }),
  update: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        name: z.string().optional(),
        status: z.enum(["ACTIVE", "CLOSED"]).optional(), // Укажите все возможные значения статуса
      })
    )
    .mutation(async ({ input, ctx }) => {
      const bonusCampaign = await ctx.prisma.bonusCampaign.update({
        where: {
          id: input.id,
        },
        data: {
          name: input.name,
          status: input.status,
        },
      });
      return bonusCampaign;
    }),
  delete: roleProtectedProcedure([UserRoles.ADMIN])
    .input(z.object({ bonus_campaign_id: z.string() }))
    .mutation(async ({ input, ctx }) => {
      const bonusCampaign = ctx.prisma.bonusCampaign.delete({
        where: {
          id: input.bonus_campaign_id,
        },
      });
      return bonusCampaign;
    }),
  getAll: protectedProcedure.query(async ({ ctx }) => {
    const bonusCampaigns = await ctx.prisma.bonusCampaign.findMany({
      orderBy: {
        createdAt: "desc",
      },
    });

    return bonusCampaigns;
  }),
  getLatest: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER]).query(
    async ({ ctx }) => {
      const bonusCampaigns = await ctx.prisma.bonusCampaign.findMany({
        orderBy: {
          createdAt: "desc", // Сортируем по полю createdAt в порядке убывания (от новых к старым)
        },
        take: 3, // Получаем только три записи
      });
      return bonusCampaigns;
    }
  ),
  getLatestCampaignWithArticles: roleProtectedProcedure([UserRoles.RETAIL]).query(
    async ({ ctx }) => {
      const bonusCampaigns = await ctx.prisma.bonusCampaign.findMany({
        include: {
          articles: {
            select: {
              code: true,
              points: true
            }
          }
        },
        orderBy: {
          createdAt: "desc"
        },
        take: 1
      });
      return bonusCampaigns;
    }
  )
});
