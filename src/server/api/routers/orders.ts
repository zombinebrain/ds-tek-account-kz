import {z} from "zod";
import path from "path";
import {
  createTRPCRouter,
  protectedProcedure,
  roleProtectedProcedure,
} from "../trpc";
import { UserRoles } from "~/models/user";
import {OrderStatuses, OrderStatusText} from "~/models/order";
import { TransactionType } from "@prisma/client";
import { Errors } from "~/models/errors";
import { mailOptions, sendMail } from "~/lib/nodemailer";
import { calculateTotalPoints } from "~/utils/transactions";

const iconPath = path.join(process.cwd(), "public", "logo.png");

export const ordersRouter = createTRPCRouter({
  create: protectedProcedure
    .input(
      z.object({
        address: z.string(),
        postal_code: z.string(),
        apartment: z.string(),
        comment: z.string(),
        contact: z.string(),
        phone: z.string(),
        gifts: z.array(
          z.object({
            id: z.string(),
            count: z.number(),
          })
        ),
      })
    )
    .mutation(async ({ input, ctx }) => {
      let giftsCost = 0;

      for (const item of input.gifts) {
        const foundGift = await ctx.prisma.gift.findUnique({
          where: {
            id: item.id,
          },
        });
        giftsCost += item.count * foundGift!.points;
      }

      const transactions = await ctx.prisma.transaction.findMany({
        where: {
          user_id: ctx.session.user.id,
          OR: [
            {
              type: TransactionType.CREDIT,
              bonus_campaign: {
                status: "CLOSED",
              },
            },
            {
              type: TransactionType.DEBIT,
            },
            {
              type: TransactionType.CARD,
            },
            {
              type: TransactionType.PHONE,
            },
            {
              type: TransactionType.AUTH,
            },
          ],
        },
      });

      const hasSomeCreditTransaction = !!transactions.filter(
        (transaction) => transaction.type === "CREDIT"
      ).length;

      if (hasSomeCreditTransaction) {
        const userBalance = calculateTotalPoints(transactions);

        if (userBalance > giftsCost) {
          const createdOrder = await ctx.prisma.order.create({
            data: {
              address: input.address,
              postal_code: input.postal_code,
              apartment: input.apartment,
              comment: input.comment,
              contact: input.contact,
              phone: input.phone,
              user: {
                connect: { id: ctx.session.user.id }, // ID пользователя, которому принадлежит заказ
              },
              orderGifts: {
                create: input.gifts.map((gift) => ({
                  gift: {
                    connect: { id: gift.id },
                  },
                  count: gift.count,
                })),
              },
            },
          });

          const calculatePoints = async () => {
            let points = 0;

            const order = await ctx.prisma.order.findUnique({
              where: {
                id: createdOrder.id,
              },
              include: {
                orderGifts: {
                  include: {
                    gift: true,
                  },
                },
              },
            });

            if (order) {
              order.orderGifts.forEach((orderGift) => {
                const giftPoints = orderGift.count * orderGift.gift.points;
                points += giftPoints;
              });
            }

            return points;
          };

          const points = await calculatePoints();

          await ctx.prisma.transaction.create({
            data: {
              type: TransactionType.DEBIT,
              user_id: ctx.session.user.id,
              order_id: createdOrder.id,
              points,
            },
          });

          await sendMail(
            "orderPendingNotification",
            {
              ...mailOptions,
              to: ctx.session.user.email!,
              subject: "Смена статуса заказа",
              attachments: [
                {
                  filename: "logo.png",
                  path: iconPath,
                  cid: "logo1", //same cid value as in the html img src
                },

              ],
            },
            {
              orderNumber: createdOrder.number.toString(),
              url: `${process.env.BASE_URL}/my-orders`,
            }
          );

          return createdOrder;
        } else {
          throw Error(Errors.NOT_ENOUGH_POINTS);
        }
      } else {
        throw Error(Errors.CREDIT_TRANSACTION_REQUIRED);
      }
    }),
  updateStatus: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        status: z.enum([
          OrderStatuses.CANCELED,
          OrderStatuses.DELIVERED,
          OrderStatuses.INDELIVERY,
          OrderStatuses.INPROGRESS,
          OrderStatuses.PENDING,
        ]),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const order = await ctx.prisma.order.update({
        where: {
          id: input.id,
        },
        data: {
          status: input.status,
        },
        include: {
          user: true,
        },
      });

      order.status === OrderStatuses.INPROGRESS &&
        (await sendMail(
          "orderInprogressNotification",
          {
            ...mailOptions,
            to: order.user.email,
            subject: "Смена статуса заказа",
            attachments: [
              {
                filename: "logo.png",
                path: iconPath,
                cid: "logo1", //same cid value as in the html img src
              },
            ],
          },
          {
            orderNumber: order.number.toString(),
            url: `${process.env.BASE_URL}/my-orders`,
          }
        ));

      order.status === OrderStatuses.DELIVERED &&
        (await sendMail(
          "orderDeliveredNotification",
          {
            ...mailOptions,
            to: order.user.email,
            subject: "Смена статуса заказа",
            attachments: [
              {
                filename: "logo.png",
                path: iconPath,
                cid: "logo1", //same cid value as in the html img src
              },
            ],
          },
          {
            orderNumber: order.number.toString(),
            url: `${process.env.BASE_URL}/my-orders`,
          }
        ));

      return order;
    }),
  cancelOrder: protectedProcedure
    .input(
      z.object({
        id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const foundOrder = await ctx.prisma.order.findUnique({
        where: {
          id: input.id,
        },
      });

      if (
        foundOrder?.userId === ctx.session.user.id ||
        ctx.session.user.role === "ADMIN" ||
        ctx.session.user.role === "MANAGER"
      ) {
        const order = await ctx.prisma.order.update({
          where: {
            id: input.id,
          },
          data: {
            status: OrderStatuses.CANCELED,
          },
          include: {
            user: true,
          },
        });
        await ctx.prisma.transaction.delete({
          where: {
            order_id: input.id,
          },
        });
        await sendMail(
          "orderCanceledNotification",
          {
            ...mailOptions,
            to: order.user.email,
            subject: "Смена статуса заказа",
            attachments: [
              {
                filename: "logo.png",
                path: iconPath,
                cid: "logo1", //same cid value as in the html img src
              },
            ],
          },
          {
            orderNumber: order.number.toString(),
            url: `${process.env.BASE_URL}/my-orders`,
          }
        );
        return order;
      } else throw Error(Errors.HAVE_NO_RIGHTS);
    }),
  confirmOrderDelivery: protectedProcedure
    .input(
      z.object({
        id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const order = await ctx.prisma.order.update({
        where: {
          id: input.id,
          userId: ctx.session.user.id,
        },
        data: {
          status: OrderStatuses.DELIVERED,
        },
        include: {
          user: true,
        },
      });
      await sendMail(
        "orderIndeliveryNotification",
        {
          ...mailOptions,
          to: order.user.email,
          subject: "Смена статуса заказа",
          attachments: [
            {
              filename: "logo.png",
              path: iconPath,
              cid: "logo1", //same cid value as in the html img src
            },
          ],
        },
        {
          site: order.site!,
          code: order.code!,
          orderNumber: order.number.toString(),
          url: `${process.env.BASE_URL}/my-orders`,
        }
      );
      return order;
    }),
  sendToDelivery: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        id: z.string(),
        code: z.string().optional(),
        site: z.string().optional(),
        deliveryDate: z.date().or(z.null()),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const order = await ctx.prisma.order.update({
        where: {
          id: input.id,
        },
        data: {
          code: input.code,
          site: input.site,
          status: OrderStatuses.INDELIVERY,
          deliveryDate: input.deliveryDate,
        },
        include: {
          user: true,
        },
      });
      await sendMail(
        "orderIndeliveryNotification",
        {
          ...mailOptions,
          to: order.user.email,
          subject: "Смена статуса заказа",
          attachments: [
            {
              filename: "logo.png",
              path: iconPath,
              cid: "logo1", //same cid value as in the html img src
            },
          ],
        },
        {
          site: order.site!,
          code: order.code!,
          orderNumber: order.number.toString(),
          url: `${process.env.BASE_URL}/my-orders`,
        }
      );
      return order;
    }),
  getALL: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        status: z.enum([
          OrderStatuses.CANCELED,
          OrderStatuses.DELIVERED,
          OrderStatuses.INDELIVERY,
          OrderStatuses.INPROGRESS,
          OrderStatuses.PENDING,
        ]),
      })
    )
    .query(async ({ input, ctx }) => {
      const orders = ctx.prisma.order.findMany({
        where: {
          status: input.status,
        },
        include: {
          document: {
            select: {
              url: true
            }
          },
          orderGifts: {
            select: {
              gift: {
                include: {
                  images: {
                    select: {
                      id: true,
                      url: true,
                    },
                  },
                },
              },
              count: true,
            },
          },
          user: {
            select: {
              email: true,
            },
          }
        },
        orderBy: {
          createdAt: "desc",
        },
      });
      return orders;
    }),
  getAllOrdersInTimePeriod: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        from: z.string().or(z.null()),
        to: z.string().or(z.null())
      })
    )
    .query(async ({ input, ctx }) => {
      if (input.from && input.to) {
        const orders = await ctx.prisma.orderGift.findMany({
          where: {
            createdAt: {
              lte: new Date(input.to),
              gte: new Date(input.from)
            }
          },
          include: {
            gift: {
              select: {
                article: true,
                name: true
              }
            },
            order: {
              select: {
                contact: true,
                status: true
              }
            }
          },
          orderBy: {
            createdAt: "asc",
          },
        });
        return orders.map((item) => ({
          createdAt: item.createdAt,
          count: item.count,
          article: item.gift.article,
          name: item.gift.name,
          contact: item.order.contact,
          status: OrderStatusText[item.order.status]
        }));
      }
    }),
  getMy: protectedProcedure.query(async ({ ctx }) => {
    const orders = ctx.prisma.order.findMany({
      where: {
        userId: ctx.session.user.id,
      },
      include: {
        orderGifts: {
          select: {
            gift: {
              include: {
                images: {
                  select: {
                    id: true,
                    url: true,
                  },
                },
              },
            },
            count: true,
          },
        },
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return orders;
  }),
  getMyOrdersCount: roleProtectedProcedure([
    UserRoles.RETAIL,
    UserRoles.SALES,
  ]).query(async ({ ctx }) => {
    const orders = ctx.prisma.order.count({
      where: {
        userId: ctx.session.user.id,
        status: {
          in: [
            OrderStatuses.PENDING,
            OrderStatuses.INPROGRESS,
            OrderStatuses.INDELIVERY,
          ],
        },
      },
    });
    return orders;
  }),
  getCount: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER]).query(
    async ({ ctx }) => {
      const orders = await ctx.prisma.order.findMany();
      return {
        all: orders.length,
        pending: orders.filter(
          (order) => order.status === OrderStatuses.PENDING
        ).length,
        inprogress: orders.filter(
          (order) => order.status === OrderStatuses.INPROGRESS
        ).length,
        indelivery: orders.filter(
          (order) => order.status === OrderStatuses.INDELIVERY
        ).length,
        delivered: orders.filter(
          (order) => order.status === OrderStatuses.DELIVERED
        ).length,
        canceled: orders.filter(
          (order) => order.status === OrderStatuses.CANCELED
        ).length,
      };
    }
  ),
  createOrderInvoice: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        url: z.string(),
        orderId: z.string()
      })
    )
    .mutation(async ({ input, ctx }) => {
      const invoice = await ctx.prisma.orderDocument.create({
        data: {
          orderId: input.orderId,
          url: input.url
        }
      });
      return invoice;
    }
  ),
  updateOrderInvoice: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        url: z.string(),
        orderId: z.string()
      })
    )
    .mutation(async ({ input, ctx }) => {
        const invoice = await ctx.prisma.orderDocument.update({
          where: {
            orderId: input.orderId
          },
          data: {
            url: input.url
          }
        });
        return invoice;
      }
    ),
});
