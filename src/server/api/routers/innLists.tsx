import { z } from "zod";
import { createTRPCRouter, roleProtectedProcedure } from "../trpc";
import { UserRoles } from "~/models/user";

export const innListsRouter = createTRPCRouter({
  create: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER])
    .input(
      z.object({
        bonus_campaign_id: z.string(),
        distributor_id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const innList = await ctx.prisma.innList.create({
        data: {
          distributor_id: input.distributor_id,
          bonus_campaign_id: input.bonus_campaign_id,
        },
      });
      return innList;
    }),
  getALL: roleProtectedProcedure([UserRoles.ADMIN, UserRoles.MANAGER])
    .input(
      z.object({
        bonus_campaign_id: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const innLists = ctx.prisma.innList.findMany({
        where: {
          bonus_campaign_id: input.bonus_campaign_id,
        },
      });
      return innLists;
    }),
});
