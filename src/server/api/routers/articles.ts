import { z } from "zod";
import { createTRPCRouter, roleProtectedProcedure } from "../trpc";
import { UserRoles } from "~/models/user";

export const articlesRouter = createTRPCRouter({
  upload: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.array(
        z.object({
          code: z.string(),
          points: z.number(),
          bonus_campaign_id: z.string(),
        })
      )
    )
    .mutation(async ({ input, ctx }) => {
      console.log(input);
      await ctx.prisma.article.deleteMany({
        where: {
          bonus_campaign_id: input[0]?.bonus_campaign_id,
        },
      });
      const articles = await ctx.prisma.article.createMany({
        data: input,
        skipDuplicates: true,
      });
      return articles;
    }),
  getBonusCampaignArticleCodes: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        bonusCampaignID: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const articles = await ctx.prisma.article.findMany({
        where: {
          bonus_campaign_id: input.bonusCampaignID,
        },
      });
      return articles.map((article) => article.code);
    }),
  getBonusCampaignArticles: roleProtectedProcedure([UserRoles.ADMIN])
    .input(
      z.object({
        bonus_campaign_id: z.string(),
      })
    )
    .mutation(async ({ input, ctx }) => {
      const articles = await ctx.prisma.article.findMany({
        where: {
          bonus_campaign_id: input.bonus_campaign_id,
        },
      });
      return articles;
    }),
  getALL: roleProtectedProcedure([UserRoles.ADMIN]).query(async ({ ctx }) => {
    const articles = ctx.prisma.article.findMany({
      orderBy: {
        createdAt: "desc",
      },
    });
    return articles;
  }),
});
