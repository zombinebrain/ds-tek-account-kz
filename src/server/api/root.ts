import { createTRPCRouter } from "~/server/api/trpc";
import { usersRouter } from "./routers/users";
import { companiesRouter } from "./routers/companies";
import { articlesRouter } from "./routers/articles";
import { giftsRouter } from "./routers/gifts";
import { regionsRouter } from "./routers/regions";
import { ordersRouter } from "./routers/orders";
import { distributorsRouter } from "./routers/distributors";
import { bonusCampaignsRouter } from "./routers/bonusCampaigns";
import { transactionsRouter } from "./routers/transactions";
import { innListsRouter } from "./routers/innLists";
import { supportContactsRouter } from "./routers/suportContacts";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  users: usersRouter,
  companies: companiesRouter,
  articles: articlesRouter,
  gifts: giftsRouter,
  regions: regionsRouter,
  orders: ordersRouter,
  distributors: distributorsRouter,
  bonusCampaigns: bonusCampaignsRouter,
  transactions: transactionsRouter,
  innLists: innListsRouter,
  supportContacts: supportContactsRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
