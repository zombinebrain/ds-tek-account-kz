import { PrismaClient } from "@prisma/client";
import { env } from "~/env.mjs";
/*import { tmpdir } from "os";
import fs from "fs";

if (env.NODE_ENV === "production") {
  fs.writeFile(
    `${tmpdir()}/root.crt`,
    process.env.MYSQL_CERTIFICATE!,
    (err) => {
      if (err) return console.log(err);
    }
  );
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global.prisma = new PrismaClient();*/

const globalForPrisma = globalThis as unknown as {
  prisma: PrismaClient | undefined;
};

export const prisma =
  globalForPrisma.prisma ??
  new PrismaClient({
    log:
      env.NODE_ENV === "development" ? ["query", "error", "warn"] : ["error"],
  });

if (env.NODE_ENV !== "production") globalForPrisma.prisma = prisma;
