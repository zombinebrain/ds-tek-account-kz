import {
  BonusCampaign,
  Company,
  Distributor,
  Order,
  Transaction,
  TransactionType,
} from "@prisma/client";

export type TransactionWithBonusCampaignAndOrderAndDistributor = Transaction & {
  order: Order | null;
  distributor: Distributor | null;
  bonus_campaign: BonusCampaign | null;
};

export type TransactionWithBonusCampaignOrderDistributorCompany =
  Transaction & {
    order: Order | null;
    company: Company | null;
    distributor: Distributor | null;
    bonus_campaign: BonusCampaign | null;
  };

export const renderTransactionName = (transactionType: TransactionType) => {
  switch (transactionType) {
    case TransactionType.CREDIT:
      return "Начислены баллы";
    case TransactionType.DEBIT:
      return "Оформлен заказ";
    case TransactionType.AUTH:
      return "Регистрация в программе";
    case TransactionType.PHONE:
      return "Пополнение телефона";
    case TransactionType.CARD:
      return "Перевод на карту";
  }
};
