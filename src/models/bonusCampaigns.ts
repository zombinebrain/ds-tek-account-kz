import { ErrorType, Errors } from "./errors";

export const BonusCampaignErrors: Partial<Record<ErrorType, string>> = {
  [Errors.UNIQUE_CONSTRAINT_VIOLATION]:
    "Программа с таким названием уже существует",
};
