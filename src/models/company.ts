import {
  Company,
  CompanyDistributor,
  CompanyType,
  Distributor,
} from "@prisma/client";
import { Errors } from "./errors";

export type CompanyDistributorWithDistributor = CompanyDistributor & {
  distributor: Distributor;
};

export type CompanyWithCompanyDistributors = Company & {
  companyDistributors: CompanyDistributorWithDistributor[];
};

export const CompanyTypeText: Record<CompanyType, string> = {
  [CompanyType.STO]: "СТО",
  [CompanyType.SHOP]: "Магазин",
  [CompanyType.SUBDEALER]: "Субдилер"
};

export const CompanyErrors = {
  [Errors.UNIQUE_CONSTRAINT_VIOLATION]: "РТО с таким ИНН уже существует",
};
