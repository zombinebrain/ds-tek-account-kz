import {Gift, Order} from "@prisma/client";
import CheckIcon from "@mui/icons-material/Check";
import LocalPostOfficeOutlinedIcon from "@mui/icons-material/LocalPostOfficeOutlined";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import MoreHorizOutlinedIcon from "@mui/icons-material/MoreHorizOutlined";
import DoNotDisturbAltOutlinedIcon from "@mui/icons-material/DoNotDisturbAltOutlined";
import { GiftWithImages } from "./gift";

export const OrderStatuses = {
  PENDING: "PENDING",
  INPROGRESS: "INPROGRESS",
  INDELIVERY: "INDELIVERY",
  DELIVERED: "DELIVERED",
  CANCELED: "CANCELED",
} as const;

export const OrderStatusText = {
  [OrderStatuses.PENDING]: "Оформлен",
  [OrderStatuses.INPROGRESS]: "На сборке",
  [OrderStatuses.INDELIVERY]: "В пути",
  [OrderStatuses.DELIVERED]: "Доставлен",
  [OrderStatuses.CANCELED]: "Отменен",
};

export const OrderStatusIcons = {
  [OrderStatuses.PENDING]: MoreHorizOutlinedIcon,
  [OrderStatuses.INPROGRESS]: LocalPostOfficeOutlinedIcon,
  [OrderStatuses.INDELIVERY]: LocalShippingOutlinedIcon,
  [OrderStatuses.DELIVERED]: CheckIcon,
  [OrderStatuses.CANCELED]: DoNotDisturbAltOutlinedIcon,
};

export type OrderWithImageGift = Order & {
  orderGifts: {
    count: number;
    gift: GiftWithImages;
  }[];
};

export type OrderWithGift = Order & {
  orderGifts: {
    count: number;
    gift: Gift;
  }[];
};

export type OrderStatus = (typeof OrderStatuses)[keyof typeof OrderStatuses];
