import { UserRole } from "./user";

export interface InviteProps {
  email: string;
  role: UserRole;
  code: string;
}
