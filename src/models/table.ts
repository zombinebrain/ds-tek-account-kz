export type TableColumnAlign =
  | "right"
  | "center"
  | "left"
  | "inherit"
  | "justify"
  | undefined;
