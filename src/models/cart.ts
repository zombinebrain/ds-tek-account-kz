import { Gift } from "@prisma/client";
import { GiftImage } from "./gift";

export type CartItem = Omit<Gift, "createdAt" | "updatedAt"> & {
  count: number;
  images: GiftImage[];
  createdAt: string | Date;
  updatedAt: string | Date;
};
