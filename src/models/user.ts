export type UserRole = "ADMIN" | "MANAGER" | "SALES" | "RETAIL";

import {Company, GiftsOnUsers, User} from "@prisma/client";
import { Errors } from "./errors";

export type ClientUser = Omit<
  User,
  "password" | "verificationCode" | "inviteCode" | "resetCode"
>;

export type ExtendedClientUser = Omit<
  User,
  "password" | "verificationCode" | "inviteCode" | "resetCode"
> & { companies: Company[], favorites: GiftsOnUsers[] };

export const UserErrors = {
  [Errors.NOT_FOUND]: "Указаны неправильные Email или пароль",
  [Errors.ALREADY_EXIST]: "Пользователь уже существует",
  [Errors.NOT_VERIFIED]: "Пользователь не верифицировал email",
  [Errors.DELETED]: "Пользователь был удалён администратором"
};

export const UserRoles = {
  ADMIN: "ADMIN",
  MANAGER: "MANAGER",
  SALES: "SALES",
  RETAIL: "RETAIL",
} as const;

export const UserRoleText: Record<UserRole, string> = {
  [UserRoles.ADMIN]: "Администратор",
  [UserRoles.MANAGER]: "Менеджер",
  [UserRoles.SALES]: "Менеджер дистрибьютора",
  [UserRoles.RETAIL]: "Клиент (СТО, РТО, магазин)",
};

export type UserStatus =
  | "ACTIVE"
  | "DELETED"
  | "BLOCKED"
  | "INACTIVE"
  | "PENDING";

export const UserStatuses = {
  ACTIVE: "ACTIVE",
  DELETED: "DELETED",
  BLOCKED: "BLOCKED",
  INACTIVE: "INACTIVE",
  PENDING: "PENDING",
} as const;

export const UserStatusesText = {
  ACTIVE: "Активен",
  DELETED: "Удален",
  BLOCKED: "Заблокирован",
  INACTIVE: "Неактивен",
  PENDING: "Приглашен",
} as const;
