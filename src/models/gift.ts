import { Gift, GiftsOnUsers } from "@prisma/client";

export type GiftWithFavorites = Gift & { favorites: GiftsOnUsers[] };

export type GiftImage = {
  id: number;
  url: string;
};

export type GiftWithImages = Gift & {
  images: GiftImage[];
};

export type CatalogueGift = GiftWithImages & {
  favorites?: GiftsOnUsers[];
};

export const MoneyGiftType = {
  PHONE: "PHONE",
  CARD: "CARD",
} as const;
