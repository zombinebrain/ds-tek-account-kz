import { Errors } from "./errors";

export const DistributorErrors = {
  [Errors.UNIQUE_CONSTRAINT_VIOLATION]: "Такое название или E-mail уже заняты",
};

export type csa = keyof typeof DistributorErrors;
