import Typography from "@mui/material/Typography";
import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import AddIcon from "@mui/icons-material/Add";
import Image from "next/image";

export function MyDropzone({
  className,
  onChange,
}: {
  className?: string;
  onChange: (files: File[]) => void;
}) {
  const onDrop = useCallback(
    (acceptedFiles: File[]) => {
      onChange(acceptedFiles);
    },
    [onChange]
  );
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div
      {...getRootProps()}
      className={`${className} flex cursor-pointer items-center justify-center border border-dashed border-black p-20`}
    >
      <input {...getInputProps()} />
      <div className="flex flex-col items-center">
        <Image
          src="/file-icon.svg"
          width={40}
          height={50}
          alt="file icon"
          className="mb-5"
        />
        <Typography
          variant="subtitle1"
          sx={{
            textTransform: "uppercase",
            display: "flex",
            alignItems: "center",
            fontWeight: "bold",
          }}
        >
          <AddIcon className="mr-2" />
          Загрузить файл
        </Typography>
      </div>
    </div>
  );
}
