import * as xlsx from "xlsx";
import { api } from "~/utils/api";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import moment from "moment";
import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { Article, BonusCampaign } from "@prisma/client";
import { MyDropzone } from "../MyDropzone";
import { VisuallyHiddenInput } from "../VisuallyHiddenInout";
import BaseButton from "~/components/ui/BaseButton";
import {createAndDownloadXLSX} from "~/utils/createAndDownloadXLSX";

export const BonusConfiguration = ({
  bonus_campaign,
  className,
}: {
  bonus_campaign: BonusCampaign;
  className?: string;
}) => {
  const { mutate } = api.articles.upload.useMutation({
    onSuccess() {
      fetchArticles({
        bonus_campaign_id: bonus_campaign.id,
      });
    },
  });

  const { mutate: fetchArticles, isLoading: isArticlesLoading } =
    api.articles.getBonusCampaignArticles.useMutation({
      onSuccess(articles) {
        setArticles(articles);
      },
    });

  const [articles, setArticles] = useState<Article[]>([]);

  useEffect(() => {
    fetchArticles({
      bonus_campaign_id: bonus_campaign.id,
    });
  }, [bonus_campaign, fetchArticles]);

  const handleChange = async (files: File[] | null) => {
    const newFile = files ? files[0] : null;

    if (newFile) {
      try {
        const fileData = await newFile.arrayBuffer();
        const workbook = xlsx.read(fileData, { type: "buffer" });

        // Предполагаем, что таблица находится в первом листе
        const firstSheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[firstSheetName!];

        // Парсинг данных из таблицы
        const parsedData: { code: string; points: number }[] =
          xlsx.utils.sheet_to_json(worksheet!);
        const filledData = parsedData.filter((row) => row.code && row.points);

        mutate(
          filledData.map((item) => ({
            ...item,
            code: item.code.toString(),
            bonus_campaign_id: bonus_campaign.id,
          }))
        );
      } catch (error) {
        console.error("Error parsing the file:", error);
      }
    }
  };

  return (
    <div className={className}>
      <div>
        {!isArticlesLoading ? (
          !!articles?.length ? (
            <>
              <div className="mb-5 flex items-center justify-between">
                <div className="flex items-center text-subtitle2">
                  <DescriptionOutlinedIcon />
                  <span className="mx-2">Файл загружен </span>
                  {moment(articles[0]!.createdAt).format("DD.MM.YYYY")}
                </div>
                <div className="space-x-2">
                  <BaseButton
                    small
                    variant="LIGHT"
                    onClick={() => createAndDownloadXLSX<Article>(articles, [ { key: 'code' }, { key: 'points' }])}
                  >
                    скачать
                  </BaseButton>
                  {
                    <BaseButton small component="label">
                      заменить
                      <VisuallyHiddenInput
                        onChange={async (
                          event: React.ChangeEvent<HTMLInputElement>
                        ) => {
                          const files = event.target.files as File[] | null;
                          await handleChange(files);
                        }}
                        type="file"
                      />
                    </BaseButton>
                  }
                </div>
              </div>
            </>
          ) : (
            <MyDropzone onChange={handleChange} />
          )
        ) : (
          <Box
            sx={{
              display: "flex",
              width: "100%",
              height: "100%",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CircularProgress
              sx={{
                color: "#E51C2F",
              }}
            />
          </Box>
        )}
      </div>
    </div>
  );
};
