/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { api } from "~/utils/api";
import React, { useEffect, useMemo } from "react";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { BonusCampaign } from "@prisma/client";
import Typography from "@mui/material/Typography";

export const ReportLogs = ({
  bonus_campaign,
  className,
}: {
  bonus_campaign: BonusCampaign;
  className?: string;
}) => {
  const {
    data: transactionItems,
    isLoading: transactionItemsLoading,
    refetch: refetchTransactionItems,
  } = api.transactions.getTransactionItems.useQuery({
    bonusCampaignID: bonus_campaign.id,
  });

  const { data: articles, refetch: refetchArticles } =
    api.articles.getBonusCampaignArticleCodes.useQuery({
      bonusCampaignID: bonus_campaign.id,
    });

  useEffect(() => {
    refetchTransactionItems()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    refetchArticles()
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [bonus_campaign, refetchTransactionItems, refetchArticles]);

  const distributorsWithErrors = useMemo(() => {
    // Step 1: Group transactionItems by distributor names
    const distributorsMap = new Map();
    transactionItems?.forEach((transactionItem) => {
      const distributorName = transactionItem.transaction.distributor!.name;
      const article = transactionItem.article;

      if (!distributorsMap.has(distributorName)) {
        distributorsMap.set(distributorName, []);
      }

      distributorsMap.get(distributorName).push(article);
    });

    // Step 2: Create the result array with distributor names and error articles
    const result: { distributorName: string; data: string[] }[] = [];
    distributorsMap.forEach((values: string[], distributorName) => {
      const errorArticles = values.filter(
        (value) =>
          !articles?.includes(value) ||
          !transactionItems?.find(
            (item) =>
              item.article === value &&
              item.transaction.distributor?.name === distributorName
          )?.count
      );
      if (errorArticles.length > 0) {
        result.push({
          distributorName: distributorName,
          data: errorArticles,
        });
      }
    });
    return result;
  }, [transactionItems, articles]);

  const emptyArticles = useMemo(() => {
    const uniqueArticles = transactionItems
      ?.map((transactionItem) => transactionItem.article)
      .reduce((accumulator: string[], article) => {
        if (!accumulator.some((item) => item === article)) {
          accumulator.push(article);
        }
        return accumulator;
      }, []);
    return uniqueArticles?.filter((article) => !articles?.includes(article));
  }, [transactionItems, articles]);

  return (
    <div className={className}>
      <div>
        {!transactionItemsLoading ? (
          !!emptyArticles?.length ? (
            <>
              <div>
                {distributorsWithErrors.map((item, index) => (
                  <div key={index}>
                    <Typography key={index} className="text-center font-bold">
                      {item.distributorName}
                    </Typography>
                    <div>
                      {item.data.map((article) => (
                        <Typography
                          key={`${item.distributorName}-${article}`}
                          className="text-center"
                        >
                          {article}
                        </Typography>
                      ))}
                    </div>
                  </div>
                ))}
              </div>
            </>
          ) : (
            <Typography variant="h6" className="text-center">
              Несовпадений не найдено
            </Typography>
          )
        ) : (
          <Box
            sx={{
              display: "flex",
              width: "100%",
              height: "100%",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CircularProgress
              sx={{
                color: "#E51C2F",
              }}
            />
          </Box>
        )}
      </div>
    </div>
  );
};
