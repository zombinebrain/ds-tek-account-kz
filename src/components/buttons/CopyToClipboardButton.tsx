import IconButton from "@mui/material/IconButton";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";

export default function CopyToClipboardButton({
  textToCopy,
  width,
  height,
}: {
  width?: string | number;
  height?: string | number;
  textToCopy: string;
}) {
  const handleClick = () => navigator.clipboard.writeText(textToCopy);
  return (
    <IconButton onClick={() => handleClick()} aria-label="click to copy">
      <ContentCopyIcon sx={{ width, height }} className="text-red" />
    </IconButton>
  );
}
