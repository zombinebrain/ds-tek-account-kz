import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import Autocomplete from "@mui/material/Autocomplete";
import { Grid } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { debounce } from "@mui/material/utils";
import React from "react";
import { OrderStatuses, OrderWithImageGift } from "~/models/order";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import Link from "next/link";
import CopyToClipboardButton from "../buttons/CopyToClipboardButton";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";
import { CartItem } from "~/models/cart";
import PriceBadge from "../badges/PriceBadge";
import { useRouter } from "next/router";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

interface Suggestion {
  value: string;
  unrestricted_value: string;
  data: {
    house: string;
    postal_code: string;
    inn: string;
    management: {
      name: string;
      post: string;
    };
    address: {
      value: string;
    };
  };
}

export default function CartForm({
  order,
  isCreatingOrder,
  createOrder,
  onCancelOrder,
  isOrderCanceling,
  onConfirmOrderDelivered,
}: {
  order?: OrderWithImageGift;
  isCreatingOrder?: boolean;
  isOrderCanceling?: boolean;
  createOrder?: ({
    address,
    postal_code,
    apartment,
    comment,
    contact,
    phone,
    gifts,
  }: {
    address: string;
    postal_code: string;
    apartment: string;
    comment: string;
    contact: string;
    phone: string;
    gifts: CartItem[];
  }) => void;
  onCancelOrder?: ({ id }: { id: string }) => void;
  onConfirmOrderDelivered?: ({ id }: { id: string }) => void;
}) {
  const router = useRouter();
  const path = router.pathname;
  const user = useSelector((state: RootState) => state.users.user);
  const cart = useSelector((state: RootState) => state.cart.cart);
  const [address, setValue] = useState<Suggestion | null>(null);
  const [inputValue, setInputValue] = React.useState("");
  const [options, setOptions] = useState<readonly Suggestion[]>([]);
  const initialFormValues = {
    address: null,
    apartment: "",
    comment: "",
    name: user?.name ?? "",
    phone: user?.phone ?? "",
  };

  const fetchOptions = useMemo(
    () =>
      debounce((value: string) => {
        const apiKey = process.env.NEXT_PUBLIC_DADATA_API_KEY;
        const url =
          "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address";
        const options = {
          method: "POST",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Token ${apiKey}`,
          },
          body: JSON.stringify({
            count: 5,
            from_bound: {
              value: "street",
            },
            to_bound: {
              value: "house",
            },
            query: value,
          }),
        };
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        fetch(url, options)
          .then((response) => response.json())
          .then((result: { suggestions: Suggestion[] }) => {
            console.log(result.suggestions);
            setOptions(result.suggestions);
          })
          .catch((error) => console.log("error", error));
      }, 400),
    []
  );

  const points =
    path === "/my-orders"
      ? order?.orderGifts.reduce((acc: number, orderGift) => {
          return acc + orderGift.count * orderGift.gift.points;
        }, 0) ?? 0
      : cart.reduce((acc: number, cartItem) => {
          return acc + cartItem.count * cartItem.points;
        }, 0);

  useEffect(() => {
    if (inputValue === "") {
      setOptions([]);
      return undefined;
    }
    fetchOptions(inputValue);
  }, [inputValue, fetchOptions]);

  return (
    <>
      <div className="mb-2 mt-5 flex justify-between border-x-0 border-b-0 border-t border-solid border-grey2 pt-5">
        <Typography variant="h4">Итого</Typography>
        <PriceBadge points={points} />
      </div>
      <Formik
        initialValues={initialFormValues}
        validate={(values) => {
          const errors: {
            address?: string;
            name?: string;
            phone?: string;
          } = {};
          if (!address) {
            errors.address = "Обязательно к заполнению";
          }
          if (!values.name) {
            errors.name = "Обязательно к заполнению";
          }
          if (!values.phone) {
            errors.phone = "Обязательно к заполнению";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          cart &&
            createOrder &&
            createOrder({
              address: address?.value ?? "",
              apartment: values.apartment,
              postal_code: address?.data.postal_code ?? "",
              comment: values.comment,
              contact: values.name,
              phone: values.phone,
              gifts: cart,
            });
        }}
      >
        {({ values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
          <Form
            onSubmit={handleSubmit}
            className="flex h-full flex-col justify-between"
          >
            <div>
              <Typography variant="h3" className="my-5">
                адрес доставки
              </Typography>
              {!order ? (
                <>
                <div className="my-2.5 flex gap-2.5">
                  <div>
                    <Typography variant="subtitle2" className="mb-2">
                      Адрес<span className="text-red">*</span>
                    </Typography>
                    <Autocomplete
                      getOptionLabel={(option) =>
                        typeof option === "string" ? option : option.value
                      }
                      filterOptions={(x) => x}
                      options={options}
                      autoComplete
                      includeInputInList
                      filterSelectedOptions
                      value={address}
                      noOptionsText="Результатов не найдено"
                      onChange={(event: any, newValue: Suggestion | null) => {
                        setOptions(newValue ? [newValue, ...options] : options);
                        setValue(newValue);
                      }}
                      onInputChange={(event, newInputValue) => {
                        setInputValue(newInputValue);
                      }}
                      renderInput={(params) => (
                        <TextField
                          className="bg-grey1 text-subtitle1"
                          required
                          placeholder="Укажите улицу и дом"
                          {...params}
                          fullWidth
                          InputProps={{
                            ...params.InputProps,
                            className: "text-subtitle1",
                          }}
                        />
                      )}
                      renderOption={(props, option) => {
                        return (
                          <li {...props}>
                            <Grid container alignItems="center">
                              <Grid
                                item
                                sx={{
                                  width: "calc(100% - 44px)",
                                  wordWrap: "break-word",
                                }}
                              >
                                <Typography variant="subtitle2">
                                  {option.value}
                                </Typography>
                              </Grid>
                            </Grid>
                          </li>
                        );
                      }}
                    />
                    <ErrorMessage
                      className="mt-2 text-subtitle2 text-red"
                      name="address"
                      component="div"
                    />
                  </div>
                  <div className="w-1/4">
                    <Typography variant="subtitle2" className="mb-2">
                      Номер дома
                    </Typography>
                    <TextField
                      fullWidth
                      disabled
                      className="bg-grey1"
                      value={address ? address.data.house : ""}
                      placeholder="Укажите адрес"
                      InputProps={{ className: "text-subtitle1" }}
                    />
                  </div>
                </div>
                  <div className="my-2.5 flex gap-2.5">
                    <div>
                      <Typography variant="subtitle2" className="mb-2">
                        Индекс
                      </Typography>
                      <TextField
                        fullWidth
                        disabled
                        className="bg-grey1"
                        value={address ? address.data.postal_code : ""}
                        placeholder="Укажите адрес"
                        InputProps={{ className: "text-subtitle1" }}
                      />
                    </div>
                    <div>
                      <Typography variant="subtitle2" className="mb-2">
                        Квартира, офис
                      </Typography>
                      <TextField
                        fullWidth
                        id="apartment"
                        name="apartment"
                        className="bg-grey1"
                        value={values.apartment}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="Указать"
                        InputProps={{ className: "text-subtitle1" }}
                      />
                      <ErrorMessage
                        className="mt-2 text-subtitle2 text-red"
                        name="apartment"
                        component="div"
                      />
                    </div>
                  </div>
                  <Typography variant="subtitle2" className="mb-2">
                    Комментарий к доставке
                  </Typography>
                  <TextField
                    fullWidth
                    id="comment"
                    name="comment"
                    className="bg-grey1"
                    value={values.comment}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Написать комментарий"
                    InputProps={{ className: "text-subtitle1" }}
                  />
                </>
              ) : (
                <>
                  <Typography variant="subtitle1">
                    {`${order.address}${
                      order.apartment && `, ${order.apartment}`
                    }`}
                  </Typography>
                  {order.comment && (
                    <div className="mt-4 flex">
                      <ChatBubbleOutlineIcon className="mr-2.5" />
                      <Typography variant="subtitle1">
                        Комментарий к доставке: {order.comment}
                      </Typography>
                    </div>
                  )}
                </>
              )}

              <Typography variant="h3" className="my-5">
                контактные данные
              </Typography>
              {!order ? (
                <>
                  <Typography variant="subtitle2" className="mb-2">
                    ФИО<span className="text-red">*</span>
                  </Typography>
                  <TextField
                    fullWidth
                    id="name"
                    name="name"
                    className="bg-grey1"
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Указать"
                    InputProps={{ className: "text-subtitle1" }}
                  />
                  <ErrorMessage
                    className="mt-2 text-subtitle2 text-red"
                    name="name"
                    component="div"
                  />
                  <Typography variant="subtitle2" className="mb-2 mt-2.5">
                    Телефон<span className="text-red">*</span>
                  </Typography>
                  <TextField
                    fullWidth
                    id="phone"
                    name="phone"
                    className="bg-grey1"
                    value={values.phone}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Указать"
                    InputProps={{ className: "text-subtitle1" }}
                  />
                  <ErrorMessage
                    className="mt-2 text-subtitle2 text-red"
                    name="phone"
                    component="div"
                  />
                </>
              ) : (
                <>
                  <Typography variant="subtitle1" className="mb-1">
                    {order.contact}
                  </Typography>
                  <Typography variant="subtitle1">{order.phone}</Typography>
                </>
              )}
              {order &&
                order.status === OrderStatuses.INDELIVERY &&
                order.site &&
                order.code &&
                onConfirmOrderDelivered && (
                  <>
                    <Typography variant="h3" className="my-5">
                      отследить заказ
                    </Typography>
                    <div className="flex flex-col bg-grey1 p-5">
                      <div className="mb-5 text-center text-subtitle2">
                        Заходите на сайт{" "}
                        <Link href={order.site} className="text-red">
                          {order.site}
                        </Link>{" "}
                        и вставьте этот код, по нему вы сможете отследить
                        подарки.
                      </div>
                      <Typography
                        variant="h2"
                        className="mb-2.5 text-center text-red"
                      >
                        {order.code}
                      </Typography>
                      <div className="flex items-center justify-center text-red">
                        скопировать код{" "}
                        <CopyToClipboardButton textToCopy={order.code} />
                      </div>
                    </div>
                  </>
                )}
            </div>
            <div>
              <div className="mb-2">
                {order?.status === OrderStatuses.DELIVERED && (
                  <Typography className="text-grey3">
                    Если у вас остались вопросы, пожалуйста, обратитесь к
                    персональному менеджеру или свяжитесь с администратором
                    службы поддержки партнеров Программы Лояльности &quot;Fenox
                    Шоколад&quot; :{" "}
                    <a href={`mailto:chocobonus@dstek.ru`} className="text-red">
                      chocobonus@dstek.ru
                    </a>
                    ,{" "}
                    <a href={`tel:+79165170911`} className="text-red">
                      +79165170911
                    </a>
                  </Typography>
                )}
              </div>
              {!order && (
                <BaseLoadingButton
                  className="w-full"
                  type="submit"
                  loading={isCreatingOrder}
                >
                  Оформить заказ {points} баллов
                </BaseLoadingButton>
              )}
              {order?.status === OrderStatuses.PENDING && onCancelOrder && (
                <BaseLoadingButton
                  className="w-full"
                  loading={isOrderCanceling}
                  onClick={() => onCancelOrder({ id: order.id })}
                  disabled={isSubmitting}
                >
                  отменить заказ
                </BaseLoadingButton>
              )}
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
}
