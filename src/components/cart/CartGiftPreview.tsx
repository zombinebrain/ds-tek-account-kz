import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { CartItem } from "~/models/cart";
import PriceBadge from "../badges/PriceBadge";

export default function CartGiftPreview({
  gift,
  onCountUpdate,
}: {
  gift: CartItem;
  onCountUpdate?: (giftId: string, count: number) => void;
}) {
  const image = gift.images.length
    ? process.env.NEXT_PUBLIC_IMAGES_URL! + gift.images[0]!.url
    : "/gift.jpg";

  return (
    <div className="flex">
      <Image
        src={image}
        width={100}
        height={80}
        alt="Gift image"
        className="mr-3 rounded-[8px]"
      />
      <div className="w-full">
        <div className="flex justify-between">
          <Typography variant="subtitle1" className="font-bold">
            {gift.name}
          </Typography>
          <PriceBadge points={gift.points} />
        </div>
        <Typography
          variant="subtitle2"
          className="mt-1 line-clamp-2 text-grey3"
        >
          {gift.description}
        </Typography>
        <div className="mt-1.5 flex items-center justify-end">
          {onCountUpdate && (
            <IconButton
              onClick={() => onCountUpdate(gift.id, -1)}
              aria-label="add to cart"
              className="h-5 w-5 bg-red text-white hover:bg-grey3"
            >
              <RemoveIcon />
            </IconButton>
          )}
          <Typography variant="h4" className="mx-1 inline-block">
            {gift.count}
          </Typography>
          {onCountUpdate && (
            <IconButton
              onClick={() => onCountUpdate(gift.id, 1)}
              aria-label="add to cart"
              className="h-5 w-5 bg-red text-white hover:bg-grey3"
            >
              <AddIcon />
            </IconButton>
          )}
        </div>
      </div>
    </div>
  );
}
