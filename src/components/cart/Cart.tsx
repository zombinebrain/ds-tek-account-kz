import Typography from "@mui/material/Typography";
import CartGiftPreview from "./CartGiftPreview";
import CartForm from "./CartForm";
import { OrderWithImageGift } from "~/models/order";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";
import { CartItem } from "~/models/cart";

export default function Cart({
  className,
  order,
  isCreatingOrder,
  createOrder,
  onCountUpdate,
  onCancelOrder,
  isOrderCanceling,
  onConfirmOrderDelivered,
}: {
  className: string;
  order?: OrderWithImageGift;
  isCreatingOrder?: boolean;
  isOrderCanceling?: boolean;
  createOrder?: ({
    address,
    postal_code,
    apartment,
    comment,
    contact,
    phone,
    gifts,
  }: {
    address: string;
    postal_code: string;
    apartment: string;
    comment: string;
    contact: string;
    phone: string;
    gifts: CartItem[];
  }) => void;
  onCountUpdate?: (giftId: string, count: number) => void;
  onCancelOrder?: ({ id }: { id: string }) => void;
  onConfirmOrderDelivered?: ({ id }: { id: string }) => void;
}) {
  const cart = useSelector((state: RootState) => state.cart.cart);
  return (
    <div className={`${className} flex flex-col`}>
      {!order && (
        <Typography variant="h3" className="my-5">
          Корзина
        </Typography>
      )}
      <div className="flex flex-col gap-y-5">
        {(
          order?.orderGifts.map((orderGift) => ({
            ...orderGift.gift,
            count: orderGift.count,
          })) ??
          cart ??
          []
        ).map((gift) => (
          <CartGiftPreview
            key={gift.id}
            gift={gift}
            onCountUpdate={onCountUpdate}
          />
        ))}
      </div>
      <CartForm
        order={order}
        isCreatingOrder={isCreatingOrder}
        createOrder={createOrder}
        onCancelOrder={onCancelOrder}
        isOrderCanceling={isOrderCanceling}
        onConfirmOrderDelivered={onConfirmOrderDelivered}
      />
    </div>
  );
}
