import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";
import { Typography } from "@mui/material";
import Link from "next/link";
import IconArrow from "~/components/icons/IconArrow";

export default function VidgetCard({
  className,
  title,
  to,
  count,
  active,
  children,
}: {
  className?: string;
  title: string;
  to?: string;
  active?: boolean;
  count?: number;
  children?: React.ReactNode;
}) {
  return (
    <Card
      className={`${className}`}
      sx={{
        borderRadius: "20px",
        border: active ? "1px solid black" : "1px solid transparent",
        display: "inline-table",
      }}
    >
      {to ? (
        <Link
          href={to}
          className={`flex items-center justify-between px-5 pt-5 text-black no-underline ${
            children ? "pb-2.5" : "pb-5"
          }`}
        >
          <Typography
            variant="h4"
            className="flex items-center"
          >
            {title}
            {count !== undefined && (
              <div className="ml-2 rounded-full bg-black px-2 py-1 text-white">
                {count}
              </div>
            )}
          </Typography>
          <IconButton
            aria-label="view order"
            sx={{
              height: "100%",
              padding: "0",
              borderRadius: "unset",
            }}
          >
            <IconArrow />
          </IconButton>
        </Link>
      ) : (
        <div className="p-5">
          <Typography variant="h4">
            {title}
          </Typography>
        </div>
      )}
      {children}
    </Card>
  );
}
