import CardActions from "@mui/material/CardActions";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import IconButton from "@mui/material/IconButton";
import PriceBadge from "../badges/PriceBadge";
import { MouseEvent, ReactNode, useState } from "react";
import EditIcon from "@mui/icons-material/Edit";
import { CatalogueGift } from "~/models/gift";
import IconArrow from "~/components/icons/IconArrow";

export default function GiftCard({
  gift,
  className,
  descriptionClassName = "line-clamp-2",
  deletable,
  showArticle,
  onEdit,
  onAddToCart,
  onCardClick,
  additionalButton,
  onDelete,
}: {
  gift: CatalogueGift;
  className?: string;
  descriptionClassName?: string;
  deletable?: boolean;
  showArticle?: boolean;
  onEdit?: () => void;
  onDelete?: () => void;
  onAddToCart?: () => void;
  additionalButton?: ReactNode;
  onCardClick?: (gift: CatalogueGift) => void;
}) {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const handleAdd = (e: MouseEvent) => {
    e.stopPropagation();
    onAddToCart && onAddToCart();
  };

  const cardImage = gift.images.length
    ? process.env.NEXT_PUBLIC_IMAGES_URL! + gift.images[selectedIndex]!.url
    : "/gift.jpg";

  const showPrevImage = (e: MouseEvent) => {
    e.stopPropagation();
    selectedIndex >= 1 && setSelectedIndex(selectedIndex - 1);
  };
  const showNextImage = (e: MouseEvent) => {
    e.stopPropagation();
    selectedIndex < (gift.images.length - 1) && setSelectedIndex(selectedIndex + 1);
  };

  return (
    <div
      onClick={onCardClick ? () => onCardClick(gift) : undefined}
      className={`inline-flex flex-col justify-between ${className}`}
    >
      <div>
        <div className="relative mb-3">
          <div className="relative">
            <CardMedia
              image={cardImage}
              title="green iguana"
              className="relative aspect-[4/5]"
            />
            {gift.images.length > 1 && (
              <>
                {
                  selectedIndex > 0 && (
                    <IconButton
                      onClick={showPrevImage}
                      sx={{
                        backgroundColor: "white",
                        border: "1px solid black",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        height: "40px",
                        width: "40px",
                        position: "absolute",
                        top: "50%",
                        left: "5px",
                        zIndex: 2,
                      }}
                      className="rotate-180"
                    >
                      <IconArrow />
                    </IconButton>
                  )
                }
                {
                  selectedIndex < gift.images.length - 1 && (
                    <IconButton
                      onClick={showNextImage}
                      sx={{
                        backgroundColor: "white",
                        border: "1px solid black",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        height: "40px",
                        width: "40px",
                        position: "absolute",
                        top: "50%",
                        right: "5px",
                        zIndex: 2,
                      }}
                    >
                      <IconArrow />
                    </IconButton>
                  )
                }
              </>
            )}
          </div>
          {(deletable ?? onEdit) && (
            <CardActions className="absolute right-0 top-0">
              {deletable && (
                <IconButton
                  size="small"
                  onClick={onDelete}
                  className="bg-grey4 text-white hover:bg-red"
                >
                  <DeleteIcon className="h-5 w-5" />
                </IconButton>
              )}
              {onEdit && (
                <IconButton
                  size="small"
                  onClick={onEdit}
                  className="bg-grey4 text-white hover:bg-red"
                >
                  <EditIcon className="h-5 w-5" />
                </IconButton>
              )}
            </CardActions>
          )}
          {onAddToCart && (
            <IconButton
              onClick={handleAdd}
              aria-label="add to cart"
              sx={{
                backgroundColor: "#E51C2F",
                color: "white",
                border: "1px solid white",
                position: "absolute",
                right: 10,
                bottom: 10,
              }}
              className="hover:bg-grey3"
            >
              <AddIcon />
            </IconButton>
          )}
        </div>
        <div className="flex items-center justify-between">
          <PriceBadge points={gift.points} size="BIG" />
          {additionalButton}
        </div>
        <div>
          <Typography
            gutterBottom
            variant="subtitle2"
            className="line-clamp-2 font-bold"
          >
            {gift.name}
          </Typography>
          {gift.article && showArticle && (
            <Typography gutterBottom className="text-grey3">
              Артикул: {gift.article}
            </Typography>
          )}
          <Typography
            gutterBottom
            className={`text-grey3 ${descriptionClassName}`}
          >
            {gift.description}
          </Typography>
        </div>
      </div>
    </div>
  );
}
