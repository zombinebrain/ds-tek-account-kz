import { BonusCampaign } from "@prisma/client";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import IconArrow from "~/components/icons/IconArrow";

export default function BonusCampaignCard({
  bonusCampaign,
  className,
  onBonusCampaignSelect,
}: {
  bonusCampaign: BonusCampaign;
  className: string;
  onBonusCampaignSelect: () => void;
}) {
  return (
    <Card
      className={`flex ${className}`}
      sx={{ width: "100%", borderRadius: "8px" }}
    >
      <CardContent
        className="flex w-full items-center"
        sx={{ padding: "20px" }}
      >
        <CalendarMonthIcon className="mr-2" />
        <div>
          {bonusCampaign.name && (
            <Typography variant="subtitle2" className="font-bold">
              {bonusCampaign.name}
            </Typography>
          )}
          <Typography variant="subtitle2">
            {bonusCampaign.startDate} - {bonusCampaign.endDate}
          </Typography>
          {bonusCampaign.status === "CLOSED" && (
            <Typography>
              период закрыт
            </Typography>
          )}
        </div>
      </CardContent>
      <CardActions sx={{ padding: 0 }}>
        <IconButton
          onClick={onBonusCampaignSelect}
          aria-label="view order"
          sx={{
            height: "100%",
            padding: "0 10px",
            borderRadius: "unset"
          }}
        >
          <IconArrow />
        </IconButton>
      </CardActions>
    </Card>
  );
}
