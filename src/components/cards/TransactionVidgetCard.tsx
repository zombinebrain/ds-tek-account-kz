import Typography from "@mui/material/Typography";
import moment from "moment";
import Image from "next/image";
import {
  TransactionWithBonusCampaignAndOrderAndDistributor,
  renderTransactionName,
} from "~/models/transactions";

export default function TransactionVidgetCard({
  transaction,
}: {
  transaction: TransactionWithBonusCampaignAndOrderAndDistributor;
}) {
  return (
    <div className="flex justify-between">
      <div className="flex">
        <div>
          <Typography variant="subtitle2">
            {renderTransactionName(transaction.type)}
          </Typography>
          <Typography className="text-grey3">
            {transaction.type === "CREDIT"
              ? `за период ${transaction.bonus_campaign?.startDate}-${transaction.bonus_campaign?.endDate}`
              : transaction.order
              ? moment(transaction.order.createdAt).format("DD.MM.YYYY")
              : ""}
            {
              transaction.type === "PHONE" || transaction.type === "CARD"
                ? moment(transaction.createdAt).format("DD.MM.YYYY")
                : ""
            }
          </Typography>
        </div>
      </div>
      <Typography variant="subtitle2" className="flex">
        {transaction.type === "CREDIT" || transaction.type === "AUTH"
          ? "+"
          : "-"}
        {transaction.points}
        <Image
          className="ml-1"
          src="/fenox_bonus_icon.svg"
          width={20}
          height={20}
          alt="fenox bonus icon"
        />
      </Typography>
    </div>
  );
}
