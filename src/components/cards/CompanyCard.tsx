import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import { CompanyWithCompanyDistributors } from "~/models/company";
import CompanyBriefInfo from "~/components/company/CompanyBriefInfo";

export default function CompanyCard({
  company,
  onDeleteCompany,
  onDeleteDistributor,
  addDistributor,
}: {
  company: CompanyWithCompanyDistributors;
  onDeleteCompany: () => void;
  onDeleteDistributor?: (distributorId: string) => void;
  addDistributor?: () => void;
}) {

  return (
    <Card className="rounded-2xl p-5">
      <CompanyBriefInfo company={company} onDeleteCompany={onDeleteCompany} />
      {!!company.companyDistributors.length && (
        <div className="mt-3">
          <Typography variant="subtitle2" className="font-bold">Дистрибьюторы:</Typography>
          <>
            {company.companyDistributors.map(
              (companyDistributor, companyDistributorIndex) => (
                <div
                  key={companyDistributorIndex}
                  className="flex h-[26px] items-center justify-between text-subtitle2"
                >
                  {companyDistributor.distributor.name}
                  {onDeleteDistributor && (
                    <IconButton
                      onClick={() =>
                        onDeleteDistributor(companyDistributor.distributorId)
                      }
                      sx={{ padding: 0 }}
                    >
                      <DeleteOutlineIcon />
                    </IconButton>
                  )}
                </div>
              )
            )}
          </>
        </div>
      )}
      {addDistributor && (
        <Button variant="text" className="mt-2 w-full font-bold text-black">
          <AddIcon className="mr-2" />
          добавить дистрибьютора
        </Button>
      )}
    </Card>
  );
}
