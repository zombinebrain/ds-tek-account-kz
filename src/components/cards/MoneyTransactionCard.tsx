import {Transaction, TransactionType} from "@prisma/client";
import Typography from "@mui/material/Typography";
import moment from "moment/moment";
import Image from "next/image";

type MoneyTransactionCardProps = {
  transaction: Transaction & { user: { email: string }};
  selectUser: (email: string) => void;
};

const giftText = {
  [TransactionType.CARD]: 'Деньги на карту',
  [TransactionType.PHONE]: 'Деньги на телефон',
};
const MoneyTransactionCard = ({ transaction, selectUser }: MoneyTransactionCardProps) => {
  return (
    <div className="w-[600px] shadow-card rounded-lg bg-white p-4">
      <div className="flex items-center gap-x-2.5 mb-4">
        <Typography variant="h4" className="text-grey3">
          Заказ:
        </Typography>
        <Typography>
          <span className="mr-0.5">{ moment(transaction.createdAt).format("DD.MM.YYYY") }</span>
          <span className="mr-0.5">{ "в " + moment(transaction.createdAt).format("HH:MM") }</span>
        </Typography>
        <button className="text-body1 border-none bg-white text-red hover:bg-grey1 cursor-pointer" onClick={() => selectUser(transaction.user.email)}>
          { transaction.user.email }
        </button>
      </div>
      <div className="flex justify-between items-center">
        <div className="flex items-center">
          <Image
            className="mr-2"
            src="/red-gift.png"
            width={20}
            height={20}
            alt="red-gift"
          />
          <Typography variant="subtitle2" className="ml-2">
            { giftText[transaction.type as keyof typeof giftText] }
          </Typography>
        </div>
        <Typography variant="subtitle2">
          { transaction.points }
        </Typography>
      </div>
    </div>
  );
};

export default MoneyTransactionCard;
