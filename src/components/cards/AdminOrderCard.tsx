import Typography from "@mui/material/Typography";
import moment from "moment";
import { OrderStatus, OrderStatuses, OrderWithImageGift } from "~/models/order";
import PriceBadge from "../badges/PriceBadge";
import Image from "next/image";
import CopyToClipboardButton from "../buttons/CopyToClipboardButton";
import BaseButton from "~/components/ui/BaseButton";
import AddIcon from "@mui/icons-material/Add";
import { VisuallyHiddenInput } from "~/components/VisuallyHiddenInout";
import Button from "@mui/material/Button";
import IconFile from "~/components/icons/IconFile";
import { base64ToArrayBuffer, ContentType, saveByteArray } from "~/utils/downloadFile";

type AdminOrderCardProps = {
  cancelingOrder: boolean;
  updatingStatus: boolean;
  order: OrderWithImageGift & {
    document: { url: string } | null,
    user: {
      email: string;
    };
  };
  setOrderId: (newVal: string) => void;
  selectUser: (email: string) => void;
  cancelOrder: ({ id }: { id: string }) => void;
  addInvoice: ({ url, orderId }: { url: string, orderId: string }) => void;
  updateInvoice: ({ url, orderId }: { url: string, orderId: string }) => void;
  onDeliveryConfirmation: () => void;
  updateOrderStatus: ({
    id,
    status,
  }: {
    id: string;
    status: OrderStatus;
  }) => void;
};

const daysPassed = (date: Date) => {
  const startDate = moment(date);

  // Calculate the number of days that have passed
  const currentDate = moment();
  const days = currentDate.diff(startDate, "days");
  return Math.abs(days);
};

export default function AdminOrderCard(props: AdminOrderCardProps) {
  const {
    order,
    selectUser,
    cancelOrder,
    addInvoice,
    updateInvoice,
    updatingStatus,
    cancelingOrder,
    setOrderId,
    updateOrderStatus,
    onDeliveryConfirmation,
  } = props;

  const onCancelOrder = () => {
    cancelOrder({
      id: order.id,
    });
  };

  const handleStatusUpdate = () => {
    switch (order.status) {
      case OrderStatuses.PENDING: {
        updateOrderStatus({
          id: order.id,
          status: OrderStatuses.INPROGRESS,
        });
        break;
      }
      case OrderStatuses.INPROGRESS: {
        setOrderId(order.id);
        onDeliveryConfirmation();
        break;
      }
      case OrderStatuses.INDELIVERY: {
        updateOrderStatus({
          id: order.id,
          status: OrderStatuses.DELIVERED,
        });
        break;
      }
    }
  };

  const buttonText = (orderStatus: OrderStatus) => {
    switch (orderStatus) {
      case OrderStatuses.PENDING:
        return "одобрить";
      case OrderStatuses.INPROGRESS:
        return "отправлен";
      case OrderStatuses.INDELIVERY:
        return "доставлен";
    }
  };

  const uploadInvoice = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const files = event.target.files as File[] | null;
    const formData = new FormData();
    formData.append("bucket", "invoices");
    if (order.document) {
      const url = order.document.url.substring(order.document.url.indexOf('/') + 1);
      console.log(url)
      formData.append("key", url);
      await fetch("/api/upload", {
        method: "DELETE",
        body: formData,
      }).then(async () => {
        if (!!files) {
          formData.append("file", files[0]!);
          await fetch("/api/upload", {
            method: "POST",
            body: formData,
          })
            .then((res) => res.json())
            .then((data: { urls: string[] }) => {
              const url = data.urls[0]!;
              updateInvoice({
                url,
                orderId: order.id
              });
            })
        }
      });
    } else {
      if (!!files) {
        formData.append("file", files[0]!);
        await fetch("/api/upload", {
          method: "POST",
          body: formData,
        })
          .then((res) => res.json())
          .then((data: { urls: string[] }) => {
            const url = data.urls[0]!;
            addInvoice({
              url,
              orderId: order.id
            });
          })
      }
    }
  };

  const downloadInvoice = async () => {
    const url = order.document!.url;
    await fetch(`/api/upload/?name=${url}`, {
      method: "GET"
    })
      .then((res) => res.json())
      .then(({ data }: { data: string, status: string }) => {
        const contentType = url.substring(url.lastIndexOf('.') + 1) as ContentType;
        const name = url.split('invoices/').pop() ?? 'Накладная';
        const arrayBuffer = base64ToArrayBuffer(data);
        saveByteArray(name, arrayBuffer, contentType);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div className="bg-white p-4">
      <div className="flex justify-between mb-5">
        <div className="flex items-center">
          <Typography variant="h4" className="mr-2.5">
            № Заказа: {order.number}
          </Typography>
          <Typography className="mr-2.5">
            {moment(order.createdAt).format("DD.MM.YYYY")}
          </Typography>
          <Typography
            onClick={() => selectUser(order.user.email)}
            className="cursor-pointer text-red hover:bg-grey2"
          >
            {order.user.email}
          </Typography>
        </div>
        <Typography component="div" className="flex items-center" variant="subtitle2">
          {(order.status === "INDELIVERY" ||
            order.status === "DELIVERED" ||
            order.status === "CANCELED") && (
            <>
              {order.site && (
                <a
                  target="_blank"
                  className="mr-2 text-red"
                  href={order.site.includes('http') ? order.site : 'https://' + order.site}
                >
                  {order.site}
                </a>
              )}
              {order.code && (
                <div className="flex items-center text-red">
                  {order.code}{" "}
                  <CopyToClipboardButton
                    height={20}
                    width={20}
                    textToCopy={order.code}
                  />
                </div>
              )}
            </>
          )}
          {order.status === "INPROGRESS" && (
            <Typography variant="subtitle2">
              На сборке: {daysPassed(order.updatedAt)} дн.
            </Typography>
          )}
          {order.status === "INDELIVERY" && order.deliveryDate && (
            <Typography variant="subtitle2">
              Доставка через: {daysPassed(order.deliveryDate)} дн.
            </Typography>
          )}
        </Typography>
      </div>
      <div className="flex">
        <div className="w-4/12 border-y-0 border-l-0 border-r border-solid border-grey2 pr-6">
          {order.orderGifts.map((orderGifts, index) => (
            <div
              key={`order-gift-${index}`}
              className="flex items-center justify-between mb-1"
            >
              <Typography
                component="div"
                variant="subtitle2"
                className="flex items-center"
              >
                <Image
                  className="mr-2"
                  src="/red-gift.png"
                  width={20}
                  height={20}
                  alt="red-gift"
                />
                {orderGifts.gift.name}
                {orderGifts.gift.article
                  ? `(№${orderGifts.gift.article})`
                  : ""}{" "}
                x {orderGifts.count}
              </Typography>
              <PriceBadge points={orderGifts.gift.points * orderGifts.count} />
            </div>
          ))}
          <div className="flex items-center justify-between font-bold mt-2">
            <div>Итого:</div>
            <PriceBadge
              points={order.orderGifts.reduce(
                (acc, orderGift) =>
                  acc + orderGift.gift.points * orderGift.count,
                0
              )}
            />
          </div>
        </div>
        <div className="flex w-4/12 flex-col justify-center pl-6">
          <Typography variant="subtitle2">{order.contact}</Typography>
          <Typography variant="subtitle2">{order.phone}</Typography>
          <Typography variant="subtitle2">
            {order.postal_code}, {order.address}
            {order.apartment ? `, кв. ${order.apartment}` : ""}
          </Typography>
          <Typography variant="subtitle2">
            Комментарий: {order.comment ? order.comment : "нет"}
          </Typography>
        </div>
        {order.status !== "CANCELED" && order.status !== "DELIVERED" && (
          <div className="flex w-4/12 items-end justify-end gap-2">
            <BaseButton
              small
              variant="LIGHT"
              disabled={cancelingOrder}
              onClick={onCancelOrder}
            >
              отменить
            </BaseButton>
            <BaseButton
              small
              disabled={updatingStatus}
              onClick={handleStatusUpdate}
            >
              {buttonText(order.status)}
            </BaseButton>
          </div>
        )}
      </div>
      {
        order.status === "DELIVERED" && (
          <Button component="label" className="text-red hover:text-grey4 hover:bg-transparent cursor-pointer flex items-center mt-5 w-fit normal-case font-normal text-subtitle1 px-0">
            <AddIcon className="mr-1" />
            Прикрепить транспортную накладную
            <VisuallyHiddenInput
              onChange={uploadInvoice}
              type="file"
              accept="image/png, image/jpg, image/jpeg, application/pdf"
            />
          </Button>
        )
      }
      {
        order.document && (
          <div className="flex items-center space-x-2">
            <IconFile />
            <span className="text-subtitle2">{ order.document.url.split('invoices/').pop() }</span>
            <div className="w-1 h-1 bg-grey4 rounded-full" />
            <Button
              component="label"
              className="text-grey4 hover:text-red hover:bg-transparent cursor-pointer w-fit min-w-fit normal-case font-normal text-subtitle2 px-0"
              onClick={downloadInvoice}
            >
              Скачать
            </Button>
          </div>
        )
      }
    </div>
  );
}
