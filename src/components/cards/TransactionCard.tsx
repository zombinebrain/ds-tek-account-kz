import {
  BonusCampaign,
  Company,
  Distributor,
  Order,
  Transaction,
  TransactionType,
} from "@prisma/client";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import CheckIcon from "@mui/icons-material/Check";
import CardActions from "@mui/material/CardActions";
import IconButton from "@mui/material/IconButton";
import CopyToClipboardButton from "../buttons/CopyToClipboardButton";
import Link from "next/link";
import moment from "moment";
import { renderTransactionName } from "~/models/transactions";
import IconArrow from "~/components/icons/IconArrow";

export default function TransactionCard({
  transaction,
  onTransactionSelect,
}: {
  transaction: Transaction & {
    distributor: Distributor | null;
    company: Company | null;
    bonus_campaign: BonusCampaign | null;
    order: Order | null;
  };
  onTransactionSelect: () => void;
}) {
  return (
    <Card
      className="flex"
      sx={{ width: 400, maxWidth: 400, borderRadius: "20px" }}
    >
      <CardContent className="w-full p-5">
        <div>
          <div className="flex justify-between">
            <div className="flex items-center">
              {transaction.type === TransactionType.CREDIT ||
              transaction.type === TransactionType.AUTH ? (
                <Image
                  className="ml-1"
                  src="/points-icon.png"
                  width={16}
                  height={16}
                  alt="points icon"
                />
              ) : (
                <CheckIcon className="w-5 h-5" />
              )}
              <Typography
                variant="subtitle1"
                component="div"
                sx={{
                  fontWeight: "bold",
                  marginLeft: 1,
                }}
              >
                {renderTransactionName(transaction.type)}
              </Typography>
            </div>
            <Typography
              variant="subtitle1"
              component="div"
              sx={{
                fontWeight: "bold",
                marginLeft: 1,
              }}
            >
              {
                transaction.type === TransactionType.CREDIT ||
                transaction.type === TransactionType.AUTH
                  ? `+${transaction.points}`
                  : `-${transaction.points}`
              }
            </Typography>
          </div>
          <div className="pl-7 mt-1">
            {transaction.type === TransactionType.CREDIT && (
              <div className="text-body1 space-y-0.5">
                <div>{transaction.company?.name ?? 'Компания была удалена'}</div>
                <div>Дистрибьютор: {transaction.distributor!.name}</div>
                <div className="text-grey3">
                  за период:{" "}
                  {transaction.bonus_campaign!.name
                    ? transaction.bonus_campaign!.name + " "
                    : ""}
                  {transaction.bonus_campaign!.startDate}-
                  {transaction.bonus_campaign!.endDate}
                </div>
              </div>
            )}
            {transaction.order && (
              <Typography variant="subtitle2">
                <span className="text-red">№{transaction.order.number}, {moment(transaction.order.createdAt).format("DD.MM.YYYY")}</span>
              </Typography>
            )}
            {
              (transaction.type === "PHONE" || transaction.type === "CARD") && (
                <Typography variant="subtitle2">
                  { moment(transaction.createdAt).format("DD.MM.YYYY") }
                </Typography>
              )
            }
            {/* {transaction.type === TransactionType.DEBIT &&
              transaction.order && (
                <>
                  <OrderStatusChip status={transaction.order.status} />
                </>
              )} */}
          </div>
          {/* <Typography variant="subtitle1">
            {transaction.order.orderGifts
              .map(
                (orderGift) =>
                `${orderGift.gift.name}${
                  orderGift.count > 1 ? ` (${orderGift.count} шт.)` : ""
                }`
                )
                .join(", ")}
              </Typography> */}
          {transaction.order?.code && (
            <Typography variant="subtitle2" className="font-bold">
              Код отслеживания:{" "}
              <span className="text-red">{transaction.order.code} </span>
              <CopyToClipboardButton
                height={20}
                width={20}
                textToCopy={transaction.order?.code}
              />
            </Typography>
          )}
          {transaction.order?.site && (
            <Typography variant="subtitle2" className="font-bold">
              Сайт отслеживания:{" "}
              <Link
                target="_blank"
                className="text-red"
                href={transaction.order?.site}
              >
                {transaction.order?.site}
              </Link>
            </Typography>
          )}
          {/* <Typography
            variant="body2"
            color="text.secondary"
            className="line-clamp-3"
          >
            {gift.description}
          </Typography> */}
        </div>
      </CardContent>
      {transaction.type === "CREDIT" && (
        <CardActions sx={{ padding: 0 }}>
          <IconButton
            onClick={onTransactionSelect}
            aria-label="view order"
            sx={{
              height: "100%",
              padding: "0 10px",
              borderRadius: "unset"
            }}
          >
            <IconArrow />
          </IconButton>
        </CardActions>
      )}
    </Card>
  );
}
