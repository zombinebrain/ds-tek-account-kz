import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import IconButton from "@mui/material/IconButton";
import Link from "next/link";
import CopyToClipboardButton from "../buttons/CopyToClipboardButton";
import OrderStatusChip from "../OrderStatusChip";
import moment from "moment";
import PriceBadge from "../badges/PriceBadge";
import { OrderStatuses, OrderWithImageGift } from "~/models/order";
import IconArrow from "~/components/icons/IconArrow";

export default function OrderCard({
  order,
  onOrderSelect,
}: {
  order: OrderWithImageGift;
  onOrderSelect: (order: OrderWithImageGift) => void;
}) {
  return (
    <Card
      className="flex"
      sx={{ width: 400, maxWidth: 400, borderRadius: "20px" }}
    >
      <CardContent className="w-full" sx={{ padding: "20px" }}>
        <div>
          <div className="flex justify-between">
            <div className="flex">
              <Image
                className="mr-2 mt-1"
                src="/red-box.png"
                width={22}
                height={22}
                alt="order icon"
              />
              <div>
                <Typography variant="subtitle1" className="font-bold">
                  №{order.number}
                </Typography>
                <Typography className="text-grey3">
                  {moment(order.createdAt).format("DD.MM.YYYY")}
                </Typography>
              </div>
            </div>
            <OrderStatusChip status={order.status} />
          </div>
          <div className="pl-7">
            <div className="mt-3 flex flex-col items-center">
              {order.orderGifts.map((orderGift, index) => (
                <div key={index} className="flex w-full justify-between">
                  <div className="flex items-center text-subtitle1 font-bold">
                    {orderGift.count} x {orderGift.gift.name}
                  </div>
                  <PriceBadge
                    points={orderGift.gift.points * orderGift.count}
                  />
                </div>
              ))}
            </div>
            <div className="mt-1 flex justify-between border-x-0 border-b-0 border-t border-solid border-grey2 pb-2 pt-1">
              <div className="text-subtitle1 font-bold uppercase">итого</div>
              <PriceBadge
                points={order.orderGifts.reduce(
                  (acc, orderGift) =>
                    acc + orderGift.gift.points * orderGift.count,
                  0
                )}
              />
            </div>
            {order.status === OrderStatuses.PENDING && (
              <Typography className="mt-1">
                Администратор обрабатывает заказ
              </Typography>
            )}
            {order.status === OrderStatuses.INPROGRESS && (
              <Typography className="mt-1">Заказ формируется на складе</Typography>
            )}
            {order.status === OrderStatuses.INDELIVERY && (
              <div className="mt-1 space-y-0.5">
                {order.site && (
                  <Typography>
                    Отследить:{" "}
                    <Link
                      target="_blank"
                      className="text-red"
                      href={order.site}
                    >
                      {order.site}
                    </Link>
                  </Typography>
                )}
                {order.code && (
                  <Typography>
                    Трек-номер: <span className="text-red">{order.code} </span>
                    <CopyToClipboardButton
                      height={20}
                      width={20}
                      textToCopy={order.code}
                    />
                  </Typography>
                )}
                {order.deliveryDate && (
                  <Typography>
                    Ориентировочная дата доставки:{" "}
                    {moment(order.deliveryDate).format("DD.MM.YY")}
                  </Typography>
                )}
              </div>
            )}
          </div>
        </div>
      </CardContent>
      <CardActions sx={{ padding: 0 }}>
        <IconButton
          onClick={() => onOrderSelect(order)}
          aria-label="view order"
          sx={{
            height: "100%",
            padding: "0 10px",
            borderRadius: "unset",
          }}
        >
          <IconArrow />
        </IconButton>
      </CardActions>
    </Card>
  );
}
