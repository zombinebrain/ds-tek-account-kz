import * as React from "react";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { api } from "~/utils/api";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Region } from "@prisma/client";
import { useEffect } from "react";
import BaseButton from "~/components/ui/BaseButton";

enum RegionAction {
  SET_GIFT = "SET_GIFT",
  UPDATE_NAME = "UPDATE_NAME",
  UPDATE_DESCRIPTION = "UPDATE_DESCRIPTION",
  UPDATE_POINTS = "UPDATE_POINTS",
  UPDATE_VISIBILITY = "UPDATE_VISIBILITY",
}

type RegionState = {
  name: string;
  code: string;
  points: number;
};

const initialRegion: RegionState = {
  name: "",
  code: "",
  points: 0,
};

function reduceGift(
  state: RegionState,
  action: { type: RegionAction; payload: Region | RegionState }
) {
  if (
    action.type === RegionAction.SET_GIFT ||
    action.type === RegionAction.UPDATE_NAME ||
    action.type === RegionAction.UPDATE_DESCRIPTION ||
    action.type === RegionAction.UPDATE_POINTS ||
    action.type === RegionAction.UPDATE_VISIBILITY
  ) {
    return {
      name: action.payload.name,
      code: action.payload.code,
      points: action.payload.points,
    };
  }
  throw Error("Unknown action.");
}

export default function RegionDialog({
  open,
  selectedRegion,
  onClose,
}: {
  open: boolean;
  selectedRegion: Region | null;
  onClose: () => void;
}) {
  const [region, setGift] = React.useReducer(reduceGift, initialRegion);

  const { mutate: createRegion } = api.regions.create.useMutation({
    onSuccess: () => {
      onClose();
    },
  });

  const { mutate: updateRegion } = api.regions.update.useMutation({
    onSuccess: () => {
      onClose();
    },
  });

  useEffect(() => {
    setGift({
      type: RegionAction.SET_GIFT,
      payload: selectedRegion ?? initialRegion,
    });
  }, [selectedRegion]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const name = region.name;
    const code = region.code;
    const points = region.points;
    if (name && code && !isNaN(points)) {
      selectedRegion
        ? updateRegion({
            id: selectedRegion.id,
            name,
            code,
            points: +points,
          })
        : createRegion({
            name,
            code,
            points: +points,
          });
    }
  };

  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle variant="h4">
          {selectedRegion ? "Редактировать" : "Добавить"} регион
        </DialogTitle>
        <DialogContent>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="name"
                  value={region.name}
                  onChange={(e) =>
                    setGift({
                      type: RegionAction.UPDATE_NAME,
                      payload: { ...region, name: e.target.value },
                    })
                  }
                  label="Название региона"
                  name="name"
                  autoComplete="name"
                  InputProps={{ className: 'text-subtitle1' }}
                  InputLabelProps={{ className: 'text-subtitle1' }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="code"
                  label="Коды региона"
                  id="code"
                  value={region.code}
                  onChange={(e) =>
                    setGift({
                      type: RegionAction.UPDATE_DESCRIPTION,
                      payload: { ...region, code: e.target.value },
                    })
                  }
                  autoComplete="new-code"
                  InputProps={{ className: 'text-subtitle1' }}
                  InputLabelProps={{ className: 'text-subtitle1' }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="points"
                  label="Стоимость доставки (баллы)"
                  id="points"
                  value={region.points}
                  onChange={(e) =>
                    setGift({
                      type: RegionAction.UPDATE_POINTS,
                      payload: { ...region, points: +e.target.value },
                    })
                  }
                  autoComplete="new-points"
                  InputProps={{ className: 'text-subtitle1' }}
                  InputLabelProps={{ className: 'text-subtitle1' }}
                />
              </Grid>
            </Grid>
            <DialogActions className="mt-5">
              <BaseButton variant="LIGHT" onClick={onClose}>Отменить</BaseButton>
              <BaseButton type="submit">
                {selectedRegion ? "Сохранить" : "Добавить"}
              </BaseButton>
            </DialogActions>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
}
