import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import { BonusCampaign, BonusCampaignStatus } from "@prisma/client";
import Image from "next/image";
import Typography from "@mui/material/Typography";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";
import BaseButton from "~/components/ui/BaseButton";

export default function SubmitBonusCampaignReportsDialog({
  open,
  bonusCampaign,
  onClose,
  updateBonusCampaign,
  isUpdatingBonusCampaign,
}: {
  open: boolean;
  bonusCampaign: BonusCampaign;
  isUpdatingBonusCampaign: boolean;
  onClose: () => void;
  updateBonusCampaign: ({
    id,
    status,
  }: {
    id: string;
    status: BonusCampaignStatus;
  }) => void;
}) {
  const handleSubmit = () => {
    updateBonusCampaign({
      id: bonusCampaign.id,
      status: BonusCampaignStatus.CLOSED,
    });
    onClose();
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          sx: {
            width: "444px",
          },
        }}
      >
        <DialogContent
          className="flex flex-col items-center"
          sx={{ padding: 5 }}
        >
          <Image
            src="/submit_bonus_campaign.svg"
            width={256}
            height={137}
            alt="submit bonus campaign image"
          />
          <Typography
            variant="h3"
            className="text-center mt-6 mb-4"
          >
            клиенам будут отправлены письма о начислении балов
          </Typography>
          <Typography variant="subtitle2">Период будет закрыт.</Typography>
          <DialogActions className="mt-5">
            <BaseButton
              variant="LIGHT"
              onClick={onClose}
              className="w-[162px]"
            >
              отмена
            </BaseButton>
            <BaseLoadingButton
              loading={isUpdatingBonusCampaign}
              className="w-[162px]"
              onClick={handleSubmit}
            >
              хорошо
            </BaseLoadingButton>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}
