import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";

export default function BaseDialog({
  open,
  onClose,
  actions,
  children,
}: {
  open: boolean;
  actions: {
    text: string;
    action: () => void;
    variant: "outlined" | "contained";
    borderColor?: string;
    backgroundColor: string;
    color: string;
  }[];
  onClose: () => void;
  children?: React.ReactNode;
}) {
  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          sx: {
            width: "444px",
          },
        }}
      >
        <DialogContent
          className="flex flex-col items-center"
          sx={{
            padding: 5,
          }}
        >
          {children}
          <DialogActions className="mt-5">
            {actions.map((item, index) => (
              <Button
                key={index}
                variant={item.variant}
                onClick={item.action}
                sx={{
                  borderColor: item.borderColor,
                  backgroundColor: item.backgroundColor,
                  color: item.color,
                  height: 48,
                  width: 162,
                }}
              >
                {item.text}
              </Button>
            ))}
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}
