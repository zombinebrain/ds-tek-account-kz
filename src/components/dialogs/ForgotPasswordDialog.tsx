import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import { ErrorMessage, Form, Formik } from "formik";
import { useRef, useState } from "react";
import { api } from "~/utils/api";
import { Errors } from "~/models/errors";
import BaseButton from "~/components/ui/BaseButton";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

type ForgotPasswordDialogProps = {
  isOpen: boolean;
  onClose: () => void;
};

export const ForgotPasswordDialogErrors = {
  [Errors.NOT_FOUND]: "Пользователь с таким Email адресом не найден",
};

const ForgotPasswordDialog = ({
  isOpen,
  onClose,
}: ForgotPasswordDialogProps) => {
  const [error, setError] = useState<string | null>(null);
  const [isSuccess, setIsSuccess] = useState(false);
  const emailRef = useRef<{ value: string }>(null);

  const { mutate: verifyEmail, isLoading } = api.users.forgotPassword.useMutation({
    onSuccess() {
      setIsSuccess(true);
    },
    onError(err) {
      console.log(err.message)
      setError(ForgotPasswordDialogErrors[err.message] ?? 'Что-то пошло не так!');
    }
  });

  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      maxWidth={false}
      PaperProps={{
        sx: {
          padding: 5,
          width: "364px"
        }
      }}
    >
      <DialogTitle
        id="alert-dialog-title"
        variant="h4"
        className="mb-6 p-0"
      >
        <IconButton onClick={onClose} className="w-8 h-8 absolute top-6 right-6">
          <CloseIcon />
        </IconButton>
        Восстановление пароля
      </DialogTitle>
      {
        isSuccess ? (
          <>
            <Typography variant="subtitle1">
              В течении 1 минуты вам придёт письмо на почту. Переходите по ссылке, указанной в письме.
            </Typography>
            <BaseButton
              className="w-full mt-6"
              onClick={onClose}
            >
              хорошо
            </BaseButton>
          </>
        ) : (
          <Formik
            initialValues={{ email: "" }}
            validate={(values) => {
              const errors: {
                email?: string;
              } = {};
              if (!values.email) {
                errors.email = "Обязательно к заполнению";
              } else {
                const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                if (!emailRegex.test(values.email)) {
                  errors.email = "Неверный формат email";
                }
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setError("");
              setSubmitting(false);
              verifyEmail({ email: values.email });
            }}
          >
            {({ values, handleChange, handleBlur, handleSubmit }) => (
              <Form
                onSubmit={handleSubmit}
                className="flex h-full flex-col justify-between"
              >
                <FormControl variant="outlined">
                  <InputLabel
                    htmlFor="outlined-adornment-password"
                    className="text-subtitle1"
                  >
                    E-mail
                  </InputLabel>
                  <OutlinedInput
                    inputRef={emailRef}
                    id="email"
                    name="email"
                    value={values.email}
                    onChange={(e) => {
                      handleChange(e);
                      setError("");
                    }}
                    onBlur={handleBlur}
                    label="E-mail"
                    className="text-subtitle1"
                  />
                </FormControl>
                <ErrorMessage
                  className="mt-2 text-subtitle2 text-red"
                  name="email"
                  component="div"
                />
                {
                  error && <Typography variant="subtitle2" className="mt-2 text-red">{error}</Typography>
                }
                <BaseLoadingButton
                  type="submit"
                  loading={isLoading}
                  className="mt-6"
                >
                  Восстановить пароль
                </BaseLoadingButton>
              </Form>
            )}
          </Formik>
        )
      }
    </Dialog>
  );
};

export default ForgotPasswordDialog;
