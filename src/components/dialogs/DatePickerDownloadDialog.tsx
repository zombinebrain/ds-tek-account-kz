import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import {DatePicker} from "@mui/x-date-pickers/DatePicker";
import {Moment} from "moment/moment";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

type DatePickerDownloadDialogProps = {
  isOpen: boolean;
  onClose: () => void;
  title: string;
  timePeriod: { to: null | Moment, from: null | Moment };
  setTimePeriod: (data: { to: null | Moment, from: null | Moment }) => void;
  isDownloading: boolean;
  onDownload: () => void;
};

const DatePickerDownloadDialog = ({
  isOpen,
  onClose,
  title,
  timePeriod,
  setTimePeriod,
  isDownloading,
  onDownload
}: DatePickerDownloadDialogProps) => {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{ className: "px-[52px] pt-[52px] pb-8 relative w-[400px]" }}
    >
      <IconButton
        onClick={onClose}
        className="w-5 h-5 absolute top-6 right-6"
      >
        <CloseIcon />
      </IconButton>
      <Typography variant="h3" className="text-center">
        { title }
      </Typography>
      <div className="flex space-x-3 my-5">
        <div className="flex flex-col">
          <label className="text-subtitle2 mb-2">Дата с</label>
          <DatePicker
            disableFuture
            value={timePeriod.from}
            onChange={(value) => setTimePeriod({ ...timePeriod, from: value })}
          />
        </div>
        <div className="flex flex-col">
          <label className="text-subtitle2 mb-2">Дата по</label>
          <DatePicker
            disableFuture
            value={timePeriod.to}
            onChange={(value: Moment | null) => {
              setTimePeriod({ ...timePeriod, to: value?.endOf('day') ?? value })
            }}
          />
        </div>
      </div>
      <BaseLoadingButton
        loading={isDownloading}
        onClick={onDownload}
      >
        Скачать
      </BaseLoadingButton>
    </Dialog>
  );
};

export default DatePickerDownloadDialog;
