import {useEffect, useState} from "react";
import Image from "next/image";
import Link from "next/link";

const CookiesDialog = () => {
  const [cookies, setIsCookies] = useState(false);

  useEffect(() => {
    const value = localStorage.getItem("cookies");
    if (!value && value !== "approved") {
      setIsCookies(true);
    }
  }, []);

  const confirmCookies = () => {
    if (typeof window !== "undefined") {
      localStorage.setItem("cookies", "approved");
      setIsCookies(false);
    }
  };

  return cookies && (
    <div className="p-8 flex fixed bottom-4 right-1/2 rounded-2xl translate-x-1/2 shadow w-[640px] bg-white z-[9999]">
      <Image
        className="mr-5"
        width={100}
        height={100}
        src="/cookies.png"
        alt="cookies"
      />
      <div>
        <div className="text-[24px] font-bold">Мы используем cookies</div>
        <div className="my-3">
          Это позволяет нам анализировать взаимодействие посетителей с сайтом и делать его лучше. Продолжая пользоваться сайтом, Вы соглашаетесь с использованием файлов cookie и
          {" "} <Link href="/privacy-policy" className="text-red no-underline hover:underline">политикой конфиденциальности</Link>.
        </div>
        <button
          onClick={confirmCookies}
          className="text-subtitle1 font-bold bg-red text-white px-8 py-1.5 border-none rounded-lg cursor-pointer"
        >
          Принять
        </button>
      </div>
    </div>
  );
};

export default CookiesDialog;
