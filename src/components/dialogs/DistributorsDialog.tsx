import * as React from "react";
import Dialog from "@mui/material/Dialog";
import { api } from "~/utils/api";
import { useEffect, useState } from "react";
import { DistributorErrors } from "~/models/distributors";
import { useFormik } from "formik";
import { Typography } from "@mui/material";
import { Distributor } from "@prisma/client";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import BaseButton from "~/components/ui/BaseButton";
import ImageUploader from "../gift/ImageUploader";

type DistributorDialogProps = {
  open: boolean;
  onClose: () => void;
  refetchDistributors: () => void;
  selectedDistributor: Distributor | null;
};

type DistributorFormValuesType = Omit<
  Distributor,
  "id" | "createdAt" | "updatedAt" | "image"
> & {
  image: File | string;
};

const formLabels = {
  name: "Наименование",
  website_link: "Сайт",
  legal_name: "Юридическое наименование",
  contact_name: "Имя контактного лица",
  city: "Город",
  email: "Email",
  contact_phone: "Телефон",
};

const initialValues = {
  name: "",
  website_link: "",
  legal_name: "",
  contact_name: "",
  image: "",
  city: "",
  email: "",
  contact_phone: "",
} as DistributorFormValuesType;

export default function DistributorDialog({
  open,
  onClose,
  refetchDistributors,
  selectedDistributor,
}: DistributorDialogProps) {
  const [error, setError] = useState("");
  const [distributorFormValues, setDistributorFormValues] =
    useState<DistributorFormValuesType>({ ...initialValues });
  const formik = useFormik({
    initialValues: distributorFormValues,
    validate: (values) => {
      const errors: {
        name?: string;
        website_link?: string;
        legal_name?: string;
        contact_name?: string;
        city?: string;
        email?: string;
        contact_phone?: string;
      } = {};
      if (!values.name) {
        errors.name = "Обязательно к заполнению";
      }
      if (!values.website_link) {
        errors.website_link = "Обязательно к заполнению";
      }
      if (!values.legal_name) {
        errors.legal_name = "Обязательно к заполнению";
      }
      if (!values.contact_name) {
        errors.contact_name = "Обязательно к заполнению";
      }
      if (!values.city) {
        errors.city = "Обязательно к заполнению";
      }
      if (!values.email) {
        errors.email = "Обязательно к заполнению";
      } else {
        const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        if (!emailRegex.test(values.email)) {
          errors.email = "Неверный формат email";
        }
      }
      if (!values.contact_phone) {
        errors.contact_phone = "Обязательно к заполнению";
      }
      return errors;
    },
    onSubmit: async (values, { setSubmitting }) => {
      setError("");
      setSubmitting(false);

      const formData = new FormData();
      formData.append("bucket", "distributors");
      if (
        selectedDistributor?.image &&
        (!distributorFormValues.image ||
          selectedDistributor?.image !== distributorFormValues.image)
      ) {
        formData.append("key", selectedDistributor.image);
        formData.append("file", distributorFormValues.image);
        await fetch("/api/upload", {
          method: "DELETE",
          body: formData,
        }).then(async () => {
          if (distributorFormValues.image) {
            await fetch("/api/upload", {
              method: "POST",
              body: formData,
            })
              .then((res) => res.json())
              .then((data: { urls: string[] }) => {
                submitDistributor(values, data.urls[0]!);
              });
          } else {
            submitDistributor(values, "");
          }
        });
      } else if (
        selectedDistributor &&
        selectedDistributor.image !== distributorFormValues.image
      ) {
        formData.append("file", distributorFormValues.image);

        await fetch("/api/upload", {
          method: "POST",
          body: formData,
        })
          .then((res) => res.json())
          .then((data: { urls: string[] }) => {
            submitDistributor(values, data.urls[0]!);
          });
      } else {
        submitDistributor(values, "");
      }
    },
    enableReinitialize: true,
  });

  const submitDistributor = (
    values: DistributorFormValuesType,
    image: string
  ) => {
    const data = {
      name: values.name,
      website_link: values.website_link,
      legal_name: values.legal_name,
      contact_name: values.contact_name,
      image: image,
      city: values.city,
      email: values.email,
      contact_phone: values.contact_phone,
    };
    !selectedDistributor
      ? createDistributor(data)
      : updateDistributor({ ...data, id: selectedDistributor.id });
  };

  useEffect(() => {
    if (selectedDistributor) {
      const copy = { ...selectedDistributor } as Partial<
        Pick<Distributor, "id" | "createdAt" | "updatedAt">
      > &
        DistributorFormValuesType;
      delete copy.id;
      delete copy.createdAt;
      delete copy.updatedAt;
      setDistributorFormValues({ ...copy });
    }
  }, [selectedDistributor]);

  const handleClose = () => {
    onClose();
    setError("");
    formik.setErrors({});
    setDistributorFormValues({ ...initialValues });
  };

  const { mutate: createDistributor } = api.distributors.create.useMutation({
    onSuccess: () => {
      refetchDistributors();
      handleClose();
    },
    onError: (error) => {
      setError(DistributorErrors[error.message]!);
    },
  });

  const { mutate: updateDistributor } = api.distributors.update.useMutation({
    onSuccess: () => {
      refetchDistributors();
      handleClose();
    },
    onError: (error) => {
      setError(DistributorErrors[error.message]!);
    },
  });

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      PaperProps={{
        sx: {
          padding: 5,
          width: "364px",
        },
      }}
    >
      <div className="relative">
        <Typography variant="h4">Добавить дистрибьютора</Typography>
        <IconButton
          onClick={handleClose}
          className="absolute -right-6 -top-6 h-8 w-8"
        >
          <CloseIcon />
        </IconButton>
      </div>

      <ImageUploader
        className="my-6"
        images={
          distributorFormValues.image ? [distributorFormValues.image] : []
        }
        onImageUpdate={(files: any[] | null) => {
          setDistributorFormValues({
            ...distributorFormValues,
            image: files && !!files.length ? files[0]! : "",
          });
        }}
      />

      <form
        onSubmit={formik.handleSubmit}
        className="mt-5 flex h-full flex-col justify-between space-y-5"
      >
        {Object.keys(formik.values)
          .filter((key) => key !== "image")
          .map((key) => (
            <TextField
              key={key}
              fullWidth
              id={key}
              name={key}
              label={formLabels[key as keyof typeof formLabels]}
              value={formik.values[key as keyof typeof formik.values]}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.touched[key as keyof typeof formik.values] &&
                Boolean(formik.errors[key as keyof typeof formik.values])
              }
              helperText={
                formik.touched[key as keyof typeof formik.values] &&
                formik.errors[key as keyof typeof formik.values]
              }
              InputProps={{ className: "text-subtitle1" }}
              InputLabelProps={{ className: "text-subtitle1" }}
            />
          ))}

        {error && <p className="text-red">{error}</p>}

        <div className="mt-5 flex justify-center">
          <BaseButton
            variant="LIGHT"
            className="mr-4 w-full"
            onClick={handleClose}
          >
            Отменить
          </BaseButton>
          <BaseButton
            className="w-full"
            type="submit"
            disabled={formik.isSubmitting}
          >
            {!selectedDistributor ? "Добавить" : "Сохранить"}
          </BaseButton>
        </div>
      </form>
    </Dialog>
  );
}
