import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import { ErrorType, Errors } from "~/models/errors";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

export default function DisabledCartDialog({
  open,
  error,
  onClose,
}: {
  open: boolean;
  error: ErrorType;
  onClose: () => void;
}) {
  return (
    <div>
      <Dialog open={open} onClose={onClose} PaperProps={{ sx: { width: 444 }, className: 'relative' }}>
        <DialogContent
          className="flex flex-col items-center"
          sx={{ padding: "50px" }}
        >
          <IconButton onClick={onClose} className="w-8 h-8 absolute top-6 right-6">
            <CloseIcon />
          </IconButton>
          {error === Errors.CREDIT_TRANSACTION_REQUIRED && (
            <>
              <Image
                src="/order-created.png"
                width={107}
                height={140}
                alt="gift image"
              />
              <Typography variant="h3" className="text-center mt-6 mb-4">
                оформление заказа временно невозможно
              </Typography>
              <Typography variant="subtitle2" className="text-center">
                Потратить приветственные бонусы возможно, после совершения
                первой покупки, а также, после предоставления подтверждающего
                отчета от выбранного Вами дистрибьютора.
              </Typography>
            </>
          )}
          {error === Errors.NOT_ENOUGH_POINTS && (
            <>
              <Image
                src="/order-created.png"
                width={107}
                height={140}
                alt="gift image"
              />
              <Typography variant="h3" className="text-center mt-6">
                у вас недостаточная сумма баллов
              </Typography>
            </>
          )}
          {error === Errors.NOT_AVAILABLE && (
            <>
              <Image
                src="/order-created.png"
                width={107}
                height={140}
                alt="gift image"
              />
              <Typography variant="h3" className="text-center mt-6 mb-4">
                этот подарок пока недоступен к заказу
              </Typography>
              <Typography variant="subtitle2">
                Возможно он закончился и мы ждем поступления. Попробуйте заказать его через некоторое время.
              </Typography>
            </>
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
}
