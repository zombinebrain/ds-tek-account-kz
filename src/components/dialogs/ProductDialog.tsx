/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import * as React from "react";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { api } from "~/utils/api";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Switch from "@mui/material/Switch";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Typography from "@mui/material/Typography";
import { useEffect, useReducer, useState } from "react";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import BaseButton from "~/components/ui/BaseButton";
import ImageUploader from "../gift/ImageUploader";
import { GiftImage, GiftWithImages } from "~/models/gift";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

enum GiftAction {
  SET_GIFT = "SET_GIFT",
  UPDATE_NAME = "UPDATE_NAME",
  UPDATE_IMAGES = "UPDATE_IMAGES",
  UPDATE_DESCRIPTION = "UPDATE_DESCRIPTION",
  UPDATE_ARTICLE = "UPDATE_ARTICLE",
  UPDATE_POINTS = "UPDATE_POINTS",
  UPDATE_VISIBILITY = "UPDATE_VISIBILITY",
}

type GiftState = {
  name: string;
  description: string;
  article: string;
  images: (File | GiftImage)[];
  points: number;
  visible: boolean;
};

const initialGift: GiftState = {
  name: "",
  description: "",
  article: "",
  images: [],
  points: 0,
  visible: false,
};

function reduceGift(
  state: GiftState,
  action: { type: GiftAction; payload: GiftState }
) {
  if (
    action.type === GiftAction.SET_GIFT ||
    action.type === GiftAction.UPDATE_NAME ||
    action.type === GiftAction.UPDATE_IMAGES ||
    action.type === GiftAction.UPDATE_DESCRIPTION ||
    action.type === GiftAction.UPDATE_ARTICLE ||
    action.type === GiftAction.UPDATE_POINTS ||
    action.type === GiftAction.UPDATE_VISIBILITY
  ) {
    return {
      ...state,
      name: action.payload.name,
      article: action.payload.article,
      images: Array.from(action.payload.images),
      description: action.payload.description,
      points: action.payload.points,
      visible: action.payload.visible,
    };
  }
  throw Error("Unknown action.");
}

export default function ProductDialog({
  open,
  selectedGift,
  onClose,
  onSuccess,
}: {
  open: boolean;
  selectedGift: GiftWithImages | null;
  onClose: () => void;
  onSuccess: () => void;
}) {
  const [gift, setGift] = useReducer(reduceGift, initialGift);
  const [isImagesLoading, setIsImagesLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const {mutate: createGift, isLoading: isCreateLoading} = api.gifts.create.useMutation({
    onSuccess: () => {
      onSuccess();
      handleClose();
    },
    onError: () => {
      setError('Упс! Что-то пошло не так!');
    }
  });

  const {mutate: updateGift, isLoading: isUpdateLoading} = api.gifts.update.useMutation({
    onSuccess: () => {
      onSuccess();
      handleClose();
    },
    onError: () => {
      setError('Упс! Что-то пошло не так!');
    }
  });

  useEffect(() => {
    setGift({
      type: GiftAction.SET_GIFT,
      payload: (selectedGift as GiftState) ?? initialGift,
    });
  }, [selectedGift]);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    setError(null);

    function isGiftImage(img: File | GiftImage): img is GiftImage {
      return (img as GiftImage).url !== undefined;
    }

    const noImagesChange = selectedGift && selectedGift.images.length === gift.images.filter(img => isGiftImage(img)).length &&
      !gift.images.filter(img => !isGiftImage(img)).length

    if (
      selectedGift &&
      selectedGift.name === gift.name &&
      selectedGift.description === gift.description &&
      selectedGift.article === gift.article &&
      selectedGift.points === gift.points &&
      selectedGift.visible === gift.visible &&
      noImagesChange
    ) {
      return;
    }

    setIsImagesLoading(true);

    const formData = new FormData();
    formData.append("bucket", "gifts");

    const imagesToDelete = selectedGift ? selectedGift.images.filter(
      (selectedImage: GiftImage) => {
        // Check if the id of selectedImage exists in any element of images
        return !gift.images.some(
          (image: GiftImage | File) =>
            !(image instanceof File) && image.id === selectedImage.id
        );
      }
    ) : [];


    const imagesToUpload = Array.from(gift.images).filter(
      (image) => image instanceof File
    );

    //console.log(imagesToDelete, imagesToUpload)

    if (imagesToDelete.length) {
      imagesToDelete.forEach((image) => {
        formData.append("key", image.url);
      });
      Array.from(gift.images).forEach((image) => {
        if (image instanceof File) {
          formData.append("file", image);
        }
      });

      await fetch("/api/upload", {
        method: "DELETE",
        body: formData,
      })
        .then(async () => {
        if (imagesToUpload.length) {
          await fetch("/api/upload", {
            method: "POST",
            body: formData,
          })
            .then((res) => res.json())
            .then((data: { urls: string[] }) => {
              const images = [
                ...(
                  gift.images.filter(
                    (image) => !(image instanceof File)
                  ) as GiftImage[]
                ).map((image) => image.url),
                ...data.urls,
              ];
              handleGiftUpdate(gift, images);
            })
            .catch(() => {
              setIsImagesLoading(false);
              setError(' Произошла ошибка при загрузке фотографий. Попробуйте ещё раз.')
            });
        } else {
          handleGiftUpdate(
            gift,
            (gift.images as GiftImage[]).map((image) => image.url)
          );
        }
      })
        .catch(() => {
          setIsImagesLoading(false);
          setError(' Произошла ошибка при удалении фотографий. Попробуйте ещё раз.')
        });
    } else {
      if (
        /*!!gift.images.length ||
        (selectedGift && selectedGift.images.length !== gift.images.length)*/
        imagesToUpload.length
      ) {
        Array.from(gift.images).forEach((image) => {
          if (image instanceof File) {
            formData.append("file", image);
          }
        });

        fetch("/api/upload", {
          method: "POST",
          body: formData,
        })
          .then((res) => res.json())
          .then((data: { urls: string[] }) => {
            const images = [
              ...(
                gift.images.filter(
                  (image) => !(image instanceof File)
                ) as GiftImage[]
              ).map((image) => image.url),
              ...data.urls,
            ];
            handleGiftUpdate(gift, images);
          })
          .catch(() => {
            setIsImagesLoading(false);
            setError(' Произошла ошибка при загрузке фотографий. Попробуйте ещё раз.')
          })
      } else {
        handleGiftUpdate(
          gift,
          [
            ...(
              gift.images.filter(
                (image) => !(image instanceof File)
              ) as GiftImage[]
            ).map((image) => image.url)
            ]
        );
      }
    }
  };

  const handleClose = () => {
    setError(null);
    setGift({
      type: GiftAction.SET_GIFT,
      payload: initialGift
    });
    onClose();
  };

  const handleGiftUpdate = (gift: GiftState, images: string[]) => {
    setIsImagesLoading(false);

    const name = gift.name;
    const description = gift.description;
    const article = gift.article;
    const points = gift.points;
    const visible = gift.visible;

    if (name && description && points) {
      selectedGift
        ? updateGift({
            id: selectedGift.id,
            name,
            images,
            description,
            article,
            points: +points,
            visible,
          })
        : createGift({
            name,
            images,
            description,
            article,
            points: +points,
            visible,
          });
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{ className: "px-10 pt-10 pb-8 relative w-[650px]" }}
      >
        <DialogTitle variant="h4">
          <IconButton
            onClick={handleClose}
            className="absolute right-6 top-6 h-8 w-8"
          >
            <CloseIcon />
          </IconButton>
          {selectedGift ? "Редактировать" : "Добавить"} товар
        </DialogTitle>
        <DialogContent>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <ImageUploader
              className="mb-6"
              images={gift.images}
              multiple
              imagesMax={3}
              onImageUpdate={(files: any[] | null) => {
                setGift({
                  type: GiftAction.UPDATE_IMAGES,
                  payload: { ...gift, images: files ?? [] },
                });
              }}
            />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="name"
                  value={gift.name}
                  onChange={(e) =>
                    setGift({
                      type: GiftAction.UPDATE_NAME,
                      payload: { ...gift, name: e.target.value },
                    })
                  }
                  InputProps={{ className: "text-subtitle1" }}
                  InputLabelProps={{ className: "text-subtitle1" }}
                  label="Название товара"
                  name="name"
                  autoComplete="name"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="description"
                  label="Описание"
                  id="description"
                  value={gift.description}
                  onChange={(e) =>
                    setGift({
                      type: GiftAction.UPDATE_DESCRIPTION,
                      payload: { ...gift, description: e.target.value },
                    })
                  }
                  InputProps={{ className: "text-subtitle1" }}
                  InputLabelProps={{ className: "text-subtitle1" }}
                  autoComplete="new-description"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  name="article"
                  label="Артикул"
                  id="article"
                  value={gift.article}
                  onChange={(e) =>
                    setGift({
                      type: GiftAction.UPDATE_ARTICLE,
                      payload: { ...gift, article: e.target.value },
                    })
                  }
                  autoComplete="new-article"
                  InputProps={{ className: "text-subtitle1" }}
                  InputLabelProps={{ className: "text-subtitle1" }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="points"
                  label="Стоимость (баллы/шт)"
                  id="points"
                  value={gift.points}
                  onChange={(e) =>
                    setGift({
                      type: GiftAction.UPDATE_POINTS,
                      payload: { ...gift, points: +e.target.value },
                    })
                  }
                  autoComplete="new-points"
                  InputProps={{ className: "text-subtitle1" }}
                  InputLabelProps={{ className: "text-subtitle1" }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={gift.visible}
                        onChange={(e) =>
                          setGift({
                            type: GiftAction.UPDATE_VISIBILITY,
                            payload: { ...gift, visible: e.target.checked },
                          })
                        }
                        id="visible"
                        name="visible"
                        color="error"
                      />
                    }
                    label={<span className="text-subtitle1">Активный</span>}
                  />
                  <Typography>
                    Активный товар будет отображаться пользователю в каталоге,
                    неактивные товары видны только администратору каталога
                  </Typography>
                </FormGroup>
              </Grid>
            </Grid>
            {
              error && (
                <Typography variant="subtitle2" className="text-red mt-4">
                  { error }
                </Typography>
              )
            }
            <DialogActions className="mt-4">
              <BaseButton variant="LIGHT" onClick={handleClose}>
                Отменить
              </BaseButton>
              <BaseLoadingButton
                loading={isCreateLoading || isUpdateLoading || isImagesLoading}
                type="submit"
              >
                {selectedGift ? "Сохранить" : "Добавить"}
              </BaseLoadingButton>
            </DialogActions>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
}
