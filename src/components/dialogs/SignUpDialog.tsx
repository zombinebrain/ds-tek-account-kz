import * as React from "react";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import { api } from "~/utils/api";
import { UserRole, UserRoles } from "~/models/user";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import { useRef, useState } from "react";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Link from "next/link";
//import Select, { SelectChangeEvent } from "@mui/material/Select";
//import MenuItem from "@mui/material/MenuItem";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";
import {Errors, ErrorType} from "~/models/errors";
import Image from "next/image";

const initialFormValues = {
  role: "",
  email: "",
  password: "",
};

//const availableRoles = [UserRoles.RETAIL, UserRoles.SALES];

export default function SignUpDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  const emailRef = useRef<{ value: string }>(null);
  const passwordRef = useRef<{ value: string }>(null);
  const [showPassword, setShowPassword] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  /** Hardcoded user role */
  const [role, setRole] = useState<UserRole | null>(UserRoles.RETAIL);
  const [error, setError] = useState<ErrorType | null>(null);
  const [isAgreedPolicy, setIsAgreedPolicy] = useState(false);

  const { mutate, isLoading: creatingUser } = api.users.create.useMutation({
    onSuccess: () => {
      setShowSuccess(true);
    },
    onError: (e) => {
      setError(e.message as ErrorType);
    }
  });

  const handleFormSubmit = () => {
    const email = emailRef.current?.value;
    const password = passwordRef.current?.value;
    role &&
      email &&
      password &&
      mutate({
        role,
        email,
        password,
      });
  };

/*  const handleSelectChange = (event: SelectChangeEvent) => {
    setRole(event.target.value as UserRole);
  };*/

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      maxWidth={false}
      PaperProps={{
        sx: {
          padding: 5,
          width: "364px",
        },
      }}
    >
      <Typography variant="h4">
        Регистрация
      </Typography>

      {!showSuccess ? (
        <>
          <Formik
            initialValues={initialFormValues}
            validate={(values) => {
              const errors: {
                role?: string;
                email?: string;
                password?: string;
              } = {};
              if (!role) {
                errors.role = "Выберите роль аккаунта";
              }
              if (!values.email) {
                errors.email = "Обязательно к заполнению";
              } else {
                const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                if (!emailRegex.test(values.email)) {
                  errors.email = "Неверный формат email";
                }
              }
              if (!values.password) {
                errors.password = "Обязательно к заполнению";
              } else if (values.password.length < 4) {
                errors.password = "Пароль должен содержать минимум 4 символа";
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setSubmitting(false);
              handleFormSubmit();
            }}
          >
            {({ values, handleChange, handleBlur, handleSubmit }) => (
              <Form
                onSubmit={handleSubmit}
                className="mt-5 flex h-full flex-col justify-between"
              >
{/*                <FormControl>
                  <InputLabel id="demo-simple-select-label" className="text-subtitle1">
                    Роль аккаунта
                  </InputLabel>
                  <Select
                    className="text-subtitle1"
                    labelId="demo-simple-select-label"
                    id="role"
                    name="role"
                    value={role ?? ""}
                    label="Роль аккаунта"
                    onChange={handleSelectChange}
                  >
                    {availableRoles.map((value) => (
                      <MenuItem key={value} value={value} className="text-subtitle2">
                        {UserRoleText[value]}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <ErrorMessage
                  className="text-red mt-2 text-subtitle2"
                  name="role"
                  component="div"
                />*/}
                <FormControl variant="outlined" className="mt-3">
                  <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                    E-mail
                  </InputLabel>
                  <OutlinedInput
                    className="text-subtitle1"
                    inputRef={emailRef}
                    id="email"
                    name="email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label="E-mail"
                  />
                </FormControl>
                <ErrorMessage
                  className="text-red mt-2 text-subtitle2"
                  name="email"
                  component="div"
                />
                <FormControl variant="outlined" className="mt-3">
                  <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                    Пароль
                  </InputLabel>
                  <OutlinedInput
                    className="text-subtitle1"
                    inputRef={passwordRef}
                    id="password"
                    name="password"
                    type={showPassword ? "text" : "password"}
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                    label="Пароль"
                  />
                </FormControl>
                <ErrorMessage
                  className="text-red mt-2 text-subtitle2"
                  name="password"
                  component="div"
                />
                <div className="flex items-center mt-5">
                  <input
                    required
                    type="checkbox"
                    className="accent-red cursor-pointer mr-3"
                    checked={isAgreedPolicy}
                    onChange={() => setIsAgreedPolicy(!isAgreedPolicy)}
                  />
                  <Typography variant="subtitle2">
                    С{" "}
                    <Link className="text-red no-underline hover:underline" href="/privacy-policy">
                      политикой конфиденциальности
                    </Link>
                    {" "}ознакомлен и согласен.
                  </Typography>
                </div>

                {
                  error && error === Errors.ALREADY_EXIST && (
                    <Typography variant="subtitle2" className="mt-3 text-red">
                      Пользователь с такой почтой уже зарегистрирован
                    </Typography>
                  )
                }
                <BaseLoadingButton
                  type="submit"
                  loading={creatingUser}
                  className="mt-8 w-full"
                >
                  зарегистрироваться
                </BaseLoadingButton>
              </Form>
            )}
          </Formik>
          <div className="mt-4 flex justify-between">
            <Typography>
              Регистрируясь, Вы даёте согласие на обработку Ваших персональных данных в соответствии с условиями{" "}
              <Link className="text-red no-underline hover:underline" href="/terms-of-use">
                Пользовательского соглашения
              </Link>
              .
            </Typography>
          </div>
        </>
      ) : (
        <Typography variant="subtitle1" className="mt-2">
          На указанный адрес электронной почты, в ближайшее время, Вам поступит
          письмо со ссылкой для подтверждения регистрации в Программе Лояльности
          &quot;FENOX Шоколад&quot;. Пожалуйста, перейдите по ссылке, указанной в письме.
        </Typography>
      )}
    </Dialog>
  );
}
