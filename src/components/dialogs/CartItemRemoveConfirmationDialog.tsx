import ConfirmationDialog from "~/components/dialogs/ConfirmationDialog";

export default function CartItemRemoveConfirmationDialog({
  open,
  onClose,
  onRemove
}: {
  open: boolean;
  onClose: () => void;
  onRemove: () => void;
}) {
  return (
    <ConfirmationDialog
      isOpen={open}
      onClose={onClose}
      onConfirm={onRemove}
      title={"Передумали заказывать подарок?"}
    />
  );
}
