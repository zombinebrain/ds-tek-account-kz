import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
// import { useState } from "react";
// import { api } from "~/utils/api";
// import FormControl from "@mui/material/FormControl";
// import InputLabel from "@mui/material/InputLabel";
// import Select, { SelectChangeEvent } from "@mui/material/Select";
// import MenuItem from "@mui/material/MenuItem";
// import { Region } from "@prisma/client";

export default function SelectRegionDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  // const [region, setRegion] = useState<Region | null>(null);

  // const { isLoading, data: regionOptions } = api.regions.getALL.useQuery();

  // const handleChange = (event: SelectChangeEvent) => {
  //   setRegion(event.target.value);
  // };

  const selectRegion = () => {
    console.log(123);
  };
  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>Invite Manager</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create a new Manager account, that will help you to manage this
            system, please enter your manager email and we will send him an
            invitation.
          </DialogContentText>
          {/* <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Регион</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="region"
              name="region"
              value={
                regionOptions![
                  regionOptions?.findIndex(
                    (item: Region) => item.id === selectRegion.id
                  )
                ]
              }
              label="Регион"
              onChange={handleChange}
            >
              {isLoading
                ? "loading..."
                : regionOptions!.map((region) => (
                    <MenuItem key={region.id} value={region}>
                      {region.name}
                    </MenuItem>
                  ))}
            </Select>
          </FormControl> */}
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Отменить</Button>
          <Button onClick={selectRegion}>Выбрать</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
