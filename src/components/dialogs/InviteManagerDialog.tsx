import * as React from "react";
import Dialog from "@mui/material/Dialog";
import { api } from "~/utils/api";
import { UserErrors, UserRoles } from "~/models/user";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import BaseButton from "~/components/ui/BaseButton";

const initialValues = {
  email: "",
};

export default function InviteManagerDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  const [error, setError] = useState("");
  const [showSuccess, setShowSuccess] = useState(false);
  const { mutate } = api.users.invite.useMutation({
    onSuccess: () => {
      setShowSuccess(true);
    },
    onError: (error) => {
      setError(UserErrors[error.message]!);
    },
  });

  const inviteUser = (email: string) => {
    email &&
      mutate({
        role: UserRoles.MANAGER,
        email,
      });
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          sx: {
            padding: 5,
            width: "364px",
          },
          className: "relative",
        }}
      >
        <IconButton
          onClick={onClose}
          className="absolute right-6 top-6 h-8 w-8"
        >
          <CloseIcon />
        </IconButton>
        <Typography variant="h3">Пригласить менеджера</Typography>
        {!showSuccess ? (
          <Formik
            initialValues={initialValues}
            validate={(values) => {
              const errors: {
                email?: string;
              } = {};
              if (!values.email) {
                errors.email = "Обязательно к заполнению";
              } else {
                const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                if (!emailRegex.test(values.email)) {
                  errors.email = "Неверный формат email";
                }
              }
              return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {
              setSubmitting(false);
              inviteUser(values.email);
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
            }) => (
              <Form
                onSubmit={handleSubmit}
                className="mt-5 flex h-full flex-col justify-between"
              >
                <FormControl variant="outlined">
                  <InputLabel
                    htmlFor="outlined-adornment-password"
                    className="text-subtitle1"
                  >
                    E-mail
                  </InputLabel>
                  <OutlinedInput
                    id="email"
                    name="email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label="E-mail"
                    className="text-subtitle1"
                  />
                </FormControl>
                <ErrorMessage
                  className="mt-2 text-subtitle2 text-red"
                  name="email"
                  component="div"
                />

                {error && (
                  <Typography variant="subtitle2" className="mt-2 text-red">
                    {error}
                  </Typography>
                )}

                <div className="mt-5 flex">
                  <BaseButton
                    variant="LIGHT"
                    onClick={onClose}
                    className="mr-2 w-full"
                  >
                    Отменить
                  </BaseButton>
                  <BaseButton
                    type="submit"
                    disabled={isSubmitting}
                    className="w-full"
                  >
                    пригласить
                  </BaseButton>
                </div>
              </Form>
            )}
          </Formik>
        ) : (
          <div className="flex flex-col items-center">
            <Typography className="mt-2">
              На указанный адрес электронной почты, в ближайшее время, поступит
              письмо со ссылкой на страницу создания пароля учётной записи.
            </Typography>
          </div>
        )}
      </Dialog>
    </div>
  );
}
