import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import { signIn } from "next-auth/react";
import { api } from "~/utils/api";
import { UserErrors, UserRoles } from "~/models/user";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import { useRef, useState } from "react";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { ErrorType } from "~/models/errors";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";
import ForgotPasswordDialog from "~/components/dialogs/ForgotPasswordDialog";

const initialFormValues = {
  email: "",
  password: "",
};

export default function SignInDialog({
  open,
  onClose,
  onSignUp,
}: {
  open: boolean;
  onClose: () => void;
  onSignUp?: () => void;
}) {
  const [isSigningIn, setIsSigningIn] = useState(false);
  const [error, setError] = useState("");
  const emailRef = useRef<{ value: string }>(null);
  const passwordRef = useRef<{ value: string }>(null);
  const [showPassword, setShowPassword] = useState(false);
  const [isForgotPasswordDialogOpen, setIsForgotPasswordDialogOpen] = useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const handleSignUp = () => {
    onSignUp && onSignUp();
    onClose();
  };

  const { mutate: login } = api.users.getUserRole.useMutation({
    onSuccess: async (data) => {
      const email = emailRef.current?.value;
      const password = passwordRef.current?.value;
      email &&
        password &&
        (await signIn("credentials", {
          email,
          password,
          callbackUrl:
            data === UserRoles.RETAIL || data === UserRoles.SALES
              ? "/catalogue"
              : "/dashboard",
        }));
      setIsSigningIn(false);
    },
    onError(error) {
      setError(UserErrors[error.message as ErrorType]!);
      setIsSigningIn(false);
    },
  });

  const openForgotPasswordDialog = () => {
    setIsForgotPasswordDialogOpen(true);
    onClose();
  };

  return (
    <>
      <Dialog
        open={open}
        onClose={onClose}
        maxWidth={false}
        PaperProps={{
          sx: {
            padding: 5,
            width: "364px",
          },
        }}
      >
        <Typography variant="h4">Авторизация</Typography>

        <Formik
          initialValues={initialFormValues}
          validate={(values) => {
            const errors: {
              email?: string;
              password?: string;
            } = {};
            if (!values.email) {
              errors.email = "Обязательно к заполнению";
            } else {
              const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
              if (!emailRegex.test(values.email)) {
                errors.email = "Неверный формат email";
              }
            }
            if (!values.password) {
              errors.password = "Обязательно к заполнению";
            } else if (values.password.length < 4) {
              errors.password = "Пароль должен содержать минимум 4 символа";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            setIsSigningIn(true);
            setError("");
            setSubmitting(false);
            login({ email: values.email, password: values.password });
          }}
        >
          {({ values, handleChange, handleBlur, handleSubmit }) => (
            <Form
              onSubmit={handleSubmit}
              className="mt-5 flex h-full flex-col justify-between"
            >
              <FormControl variant="outlined">
                <InputLabel
                  htmlFor="outlined-adornment-password"
                  className="text-subtitle1"
                >
                  E-mail
                </InputLabel>
                <OutlinedInput
                  inputRef={emailRef}
                  id="email"
                  name="email"
                  value={values.email}
                  onChange={(e) => {
                    handleChange(e);
                    setError("");
                  }}
                  onBlur={handleBlur}
                  label="E-mail"
                  className="text-subtitle1"
                />
              </FormControl>
              <ErrorMessage
                className="mt-2 text-subtitle2 text-red"
                name="email"
                component="div"
              />
              <FormControl variant="outlined" className="mt-3">
                <InputLabel
                  htmlFor="outlined-adornment-password"
                  className="text-subtitle1"
                >
                  Пароль
                </InputLabel>
                <OutlinedInput
                  inputRef={passwordRef}
                  id="password"
                  name="password"
                  type={showPassword ? "text" : "password"}
                  value={values.password}
                  onChange={(e) => {
                    handleChange(e);
                    setError("");
                  }}
                  onBlur={handleBlur}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Пароль"
                  className="text-subtitle1"
                />
              </FormControl>
              <ErrorMessage
                className="mt-2 text-subtitle2 text-red"
                name="password"
                component="div"
              />

              {error && (
                <Typography variant="subtitle2" className="mt-2 text-red">
                  {error}
                </Typography>
              )}

              <BaseLoadingButton
                type="submit"
                loading={isSigningIn}
                className="mt-8 w-full"
              >
                войти
              </BaseLoadingButton>
            </Form>
          )}
        </Formik>

        <div className="mt-10 flex justify-between">
          <Button
            variant="text"
            className="text-subtitle1"
            onClick={openForgotPasswordDialog}
            sx={{ color: "black", textTransform: "none", fontWeight: "400" }}
          >
            Забыли пароль?
          </Button>
          {!!onSignUp && (
            <Button
              onClick={handleSignUp}
              variant="text"
              className="text-subtitle1"
              sx={{ color: "black", textTransform: "none", fontWeight: "400" }}
            >
              Зарегистрироваться
            </Button>
          )}
        </div>
      </Dialog>
      <ForgotPasswordDialog
        isOpen={isForgotPasswordDialogOpen}
        onClose={() => setIsForgotPasswordDialogOpen(false)}
      />
    </>
  );
}
