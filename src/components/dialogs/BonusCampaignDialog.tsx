import * as React from "react";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import { ErrorMessage, Form, Formik } from "formik";
import InputLabel from "@mui/material/InputLabel";
import OutlinedInput from "@mui/material/OutlinedInput";
import FormControl from "@mui/material/FormControl";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import moment from "moment";
import BaseButton from "~/components/ui/BaseButton";

const initialFormValues = {
  name: "",
  startDate: null,
  endDate: null,
};

const DatePickerSx = {
  textField: {
    InputProps: {
      className: "text-subtitle1"
    },
    InputLabelProps: {
      className: "text-subtitle1"
    }
  }
};

export default function BonusCampaignDialog({
  open,
  error,
  onClose,
  setError,
  createBonusCampaign,
}: {
  open: boolean;
  error: string;
  onClose: () => void;
  setError: (value: string) => void;
  createBonusCampaign: ({
    name,
    startDate,
    endDate,
  }: {
    name: string;
    startDate: string;
    endDate: string;
  }) => void;
}) {
  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          sx: {
            padding: 5,
            width: "364px",
          },
        }}
      >
        <Typography variant="h4">
          Добавить бонусную кампанию
        </Typography>
        <Formik
          initialValues={initialFormValues}
          validate={(values) => {
            const errors: {
              startDate?: string;
              endDate?: string;
            } = {};
            if (!values.startDate) {
              errors.startDate = "Обязательно к заполнению";
            }
            if (!values.endDate) {
              errors.endDate = "Обязательно к заполнению";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            const startDate = moment(values.startDate).format("DD.MM.YYYY");
            const endDate = moment(values.endDate).format("DD.MM.YYYY");
            setError("");
            setSubmitting(false);
            createBonusCampaign({
              name: values.name,
              startDate,
              endDate,
            });
          }}
        >
          {({
            values,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            isSubmitting,
          }) => (
            <Form
              onSubmit={handleSubmit}
              className="mt-5 flex h-full flex-col justify-between"
            >
              <FormControl variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                  Название
                </InputLabel>
                <OutlinedInput
                  id="name"
                  name="name"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label="Название"
                  className="text-subtitle1"
                />
              </FormControl>
              <ErrorMessage
                className="mt-2 text-subtitle2 text-red"
                name="name"
                component="div"
              />

              <DatePicker
                label="Дата начала"
                value={values.startDate}
                maxDate={values.endDate}
                onChange={async (value) => {
                  value && (await setFieldValue("startDate", moment(value)));
                }}
                className="mt-4"
                slotProps={DatePickerSx}
              />
              <ErrorMessage
                className="mt-2 text-subtitle2 text-red"
                name="startDate"
                component="div"
              />
              <DatePicker
                label="Дата окончания"
                value={values.endDate}
                minDate={values.startDate}
                onChange={async (value) => {
                  value && (await setFieldValue("endDate", moment(value)));
                }}
                className="mt-4"
                slotProps={DatePickerSx}
              />
              <ErrorMessage
                className="mt-2 text-subtitle2 text-red"
                name="endDate"
                component="div"
              />

              {error && <p className="text-red">{error}</p>}

              <BaseButton
                type="submit"
                disabled={isSubmitting}
                className="w-full mt-5"
              >
                добавить
              </BaseButton>
            </Form>
          )}
        </Formik>
      </Dialog>
    </div>
  );
}
