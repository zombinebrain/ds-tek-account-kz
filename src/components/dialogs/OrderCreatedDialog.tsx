import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import Image from "next/image";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

export default function OrderCreatedDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  const user = useSelector((state: RootState) => state.users.user);

  return (
    <div>
      <Dialog open={open} onClose={onClose} PaperProps={{ sx: { width: 444 }, className: 'relative' }}>
        <DialogContent
          className="flex flex-col items-center"
          sx={{ padding: "50px" }}
        >
          <IconButton onClick={onClose} className="w-8 h-8 absolute top-6 right-6">
            <CloseIcon />
          </IconButton>
          <Image
            className="ml-1"
            src="/order-created.png"
            width={107}
            height={140}
            alt="order created"
          />
          <Typography variant="h3" className="mt-8 mb-4">
            заказ оформлен
          </Typography>
          <Typography variant="subtitle1" className="mt-2 text-center">
            Ожидайте подтверждения заказа на{" "}
            <span className="font-bold">{user?.email}</span> или отслеживайте
            изменения в личном кабинете.
          </Typography>
        </DialogContent>
      </Dialog>
    </div>
  );
}
