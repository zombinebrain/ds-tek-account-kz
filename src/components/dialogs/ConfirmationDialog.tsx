import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import CloseIcon from '@mui/icons-material/Close';
import BaseButton from "~/components/ui/BaseButton";

type ConfirmationDialogProps = {
  isOpen: boolean;
  onClose: () => void;
  onCancel?: () => void;
  onConfirm: () => void;
  title: string;
  confirmText?: string;
  cancelText?: string;
}

const ConfirmationDialog = ({
  isOpen,
  onClose,
  onCancel,
  onConfirm,
  title,
  confirmText = "Да",
  cancelText = "Нет"
}: ConfirmationDialogProps) => {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{ className: "px-10 pt-10 pb-8 relative w-[400px]" }}
    >
      <DialogTitle
        id="alert-dialog-title"
        variant="h4"
        className="mb-8 text-center"
      >
        <IconButton onClick={onClose} className="w-8 h-8 absolute top-6 right-6">
          <CloseIcon />
        </IconButton>
        {title}
      </DialogTitle>
      <DialogActions
        className="flex justify-center mb-6">
        <BaseButton
          variant="LIGHT"
          onClick={onConfirm}
          className="w-full"
        >
          { confirmText }
        </BaseButton>
        <BaseButton
          onClick={onCancel ?? onClose}
          className="w-full"
        >
          { cancelText }
        </BaseButton>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
