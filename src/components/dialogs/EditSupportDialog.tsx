import * as React from "react";
import Dialog from "@mui/material/Dialog";
import { api } from "~/utils/api";
import { Form, Formik } from "formik";
import {
  FormControl,
  IconButton,
  InputLabel,
  OutlinedInput,
  Switch,
  Typography,
} from "@mui/material";
import { RootState } from "~/app/store";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { SupportContact, SupportContactType } from "@prisma/client";
import DeleteIcon from "@mui/icons-material/Delete";
import { set } from "~/app/store/modules/supportContacts";
import BaseButton from "~/components/ui/BaseButton";

export default function EditSupportDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  const dispatch = useDispatch();
  const [contacts, setContacts] = useState<SupportContact[]>([]);
  const supportContacts = useSelector(
    (state: RootState) => state.supportContacts.contacts
  );

  const { mutate: updateContacts } =
    api.supportContacts.updateContactList.useMutation({
      onSuccess: () => {
        onClose();
      },
    });

  useEffect(() => {
    setContacts(
      JSON.parse(JSON.stringify(supportContacts)) as SupportContact[]
    );
  }, [supportContacts, setContacts]);

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          sx: {
            padding: 5,
            width: "364px",
          },
        }}
      >
        <Typography variant="h3">
          служба поддержки клиентов
        </Typography>

        <Formik
          initialValues={{ contacts: contacts }}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(false);
            updateContacts(values.contacts);
            dispatch(set(values.contacts));
          }}
        >
          {({ values, handleSubmit, setFieldValue, isSubmitting }) => (
            <Form
              onSubmit={handleSubmit}
              className="mt-5 flex h-full flex-col justify-between"
            >
              <div className="flex max-h-[300px] flex-col gap-y-2 overflow-auto py-2">
                {values.contacts.map((contact, index) => (
                  <div key={index} className="flex items-center">
                    <Switch
                      color="error"
                      checked={contact.type === "PHONE"}
                      onChange={async () => {
                        const newContacts = [...values.contacts];
                        const contactToUpdate = newContacts.find(
                          (contact, contactsIndex) => contactsIndex === index
                        );
                        if (contactToUpdate) {
                          contactToUpdate.type =
                            contactToUpdate.type === "EMAIL"
                              ? "PHONE"
                              : "EMAIL";
                        }
                        await setFieldValue("contacts", newContacts);
                      }}
                    />
                    <FormControl variant="outlined" fullWidth>
                      <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                        {contact.type === "PHONE" ? "Телефон" : "Email"}
                      </InputLabel>
                      <OutlinedInput
                        value={contact.value}
                        onChange={async (e) => {
                          const newContacts = [...values.contacts];
                          const contactToUpdate = newContacts.find(
                            (_, contactsIndex) => contactsIndex === index
                          );
                          if (contactToUpdate) {
                            contactToUpdate.value = e.target.value;
                          }
                          await setFieldValue("contacts", newContacts);
                        }}
                        label={contact.type === "PHONE" ? "Телефон" : "Email"}
                        className="text-subtitle1"
                      />
                    </FormControl>
                    <IconButton
                      onClick={async () => {
                        const newContacts = values.contacts.filter(
                          (_, contactsIndex) => contactsIndex !== index
                        );

                        await setFieldValue("contacts", newContacts);
                      }}
                    >
                      <DeleteIcon className="text-red" />
                    </IconButton>
                  </div>
                ))}
              </div>
              <BaseButton
                onClick={async () => {
                  const newContacts = [
                    ...values.contacts,
                    { type: SupportContactType.PHONE, value: "", id: "" },
                  ];
                  await setFieldValue("contacts", newContacts);
                }}
                className="w-full mt-4"
              >
                добавить
              </BaseButton>
              <div className="mt-5 flex">
                <BaseButton
                  variant="LIGHT"
                  onClick={onClose}
                  className="w-full mr-2"
                >
                  Отменить
                </BaseButton>
                <BaseButton
                  type="submit"
                  disabled={isSubmitting}
                  className="w-full"
                >
                  сохранить
                </BaseButton>
              </div>
            </Form>
          )}
        </Formik>
      </Dialog>
    </div>
  );
}
