import * as React from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

export default function InnListSentDialog({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  return (
    <div>
      <Dialog open={open} onClose={onClose} PaperProps={{ sx: { width: 444 } }}>
        <DialogContent
          className="flex flex-col items-center relative"
          sx={{ padding: "50px" }}
        >
          <IconButton onClick={onClose} className="w-8 h-8 absolute top-6 right-6">
            <CloseIcon />
          </IconButton>
          <Typography variant="h4" className="mb-4">
            Список ИНН отправлен
          </Typography>
          <Typography variant="subtitle2">
            Дистрибьютору отправлено письмо с Excel файлом, содержащим список ИНН
          </Typography>
        </DialogContent>
      </Dialog>
    </div>
  );
}
