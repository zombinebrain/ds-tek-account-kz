import * as React from "react";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useRef, useState } from "react";
import Image from "next/image";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import BaseButton from "~/components/ui/BaseButton";

const DatePickerSx = {
  textField: {
    InputProps: {
      className: "text-subtitle1"
    },
    InputLabelProps: {
      className: "text-subtitle1"
    }
  }
};
export default function DeliveryConfirmationDialog({
  open,
  onClose,
  onDeliveryConfirm,
}: {
  open: boolean;
  onClose: () => void;
  onDeliveryConfirm: (
    code: string | undefined,
    site: string | undefined,
    deliveryDate: Date | null
  ) => void;
}) {
  const codeRef = useRef<{ value: string }>(null);
  const siteRef = useRef<{ value: string }>(null);
  const [deliveryDate, setDeliveryDate] = useState<Date | null>(null);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const code = codeRef.current?.value;
    const site = siteRef.current?.value;

    onDeliveryConfirm(code, site, deliveryDate);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
          className: "p-10",
          sx: {
            width: "344px",
          },
        }}
      >
        <div className="flex flex-col items-center">
          <Image
            src="/delivery-confirmation.png"
            width={141}
            height={122}
            alt="delivery confirmation image"
          />
          <DialogTitle variant="h3" className="text-center">
            данные для отслеживания
          </DialogTitle>
        </div>
        <DialogContent sx={{ padding: 0 }}>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid>
              <Grid item xs={12}>
                <Typography variant="subtitle2" className="mb-2">
                  Код отслеживания
                </Typography>
                <TextField
                  inputRef={codeRef}
                  fullWidth
                  id="code"
                  placeholder="Укажите код"
                  name="code"
                  InputProps={{ className: "text-subtitle1" }}
                />
              </Grid>
              <Grid item xs={12} sx={{ marginTop: "10px" }}>
                <Typography variant="subtitle2" className="mt-3 mb-2">
                  Сайт для отслеживания доставки
                </Typography>
                <TextField
                  inputRef={siteRef}
                  required
                  fullWidth
                  name="site"
                  id="site"
                  placeholder="Укажите сайт"
                  InputProps={{ className: "text-subtitle1" }}
                />
              </Grid>
              <Grid item xs={12} sx={{ marginTop: "10px" }}>
                <Typography variant="subtitle2" className="mt-3 mb-2">
                  Ориентировочная дата доставки
                </Typography>
                <DatePicker
                  value={deliveryDate}
                  sx={{ width: "100%" }}
                  onChange={(value) => {
                    value && setDeliveryDate(value);
                  }}
                  slotProps={DatePickerSx}
                />
              </Grid>
            </Grid>
            <DialogActions className="mt-5 p-0">
              <BaseButton type="submit" className="w-full">
                готово
              </BaseButton>
            </DialogActions>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
}
