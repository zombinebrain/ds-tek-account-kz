import {CompanyType} from "@prisma/client";
import {useEffect, useMemo, useState} from "react";
import Select, {SelectChangeEvent} from "@mui/material/Select";
import {debounce} from "@mui/material/utils";
import Typography from "@mui/material/Typography";
import {ErrorMessage, Form, Formik} from "formik";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import {CompanyTypeText} from "~/models/company";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import BaseButton from "~/components/ui/BaseButton";
import * as React from "react";

export interface Suggestion {
  value: string;
  unrestricted_value: string;
  data: {
    inn: string;
    index: number;
    address: {
      value: string;
      unrestricted_value: string;
    };
  };
}

const initialValues = {
  type: null,
  inn: ''
};

type CompanyDialogRTOViewProps = {
  onCancel: () => void;
  onSubmit: () => void;
  value: Suggestion | null;
  setValue: (newValue: Suggestion | null) => void;
  type: CompanyType | null;
  setType: (value: CompanyType) => void;
};

const CompanyDialogRTOView = ({
  onCancel,
  onSubmit,
  setValue,
  value,
  type,
  setType
}: CompanyDialogRTOViewProps) => {
  const companyTypes = [CompanyType.STO, CompanyType.SHOP, CompanyType.SUBDEALER];

  const [inputValue, setInputValue] = useState("");
  const [options, setOptions] = useState<readonly Suggestion[]>([]);

  const handleSelectChange = (event: SelectChangeEvent) => {
    setType(event.target.value as CompanyType);
  };

  const fetchOptions = useMemo(
    () =>
      debounce((value: string) => {
        const apiKey = process.env.NEXT_PUBLIC_DADATA_API_KEY;
        const url =
          "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party";
        const options = {
          method: "POST",
          mode: "cors",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Token ${apiKey}`,
          },
          body: JSON.stringify({ query: value }),
        };
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        fetch(url, options)
          .then((response) => response.json())
          .then((result: { suggestions: Suggestion[] }) => {
            console.log(result.suggestions);
            setOptions(result.suggestions);
          })
          .catch((error) => console.log("error", error));
      }, 400),
    []
  );

  useEffect(() => {
    if (inputValue === "") {
      setOptions(value ? [value] : []);
      return undefined;
    }

    fetchOptions(inputValue);
  }, [value, inputValue, fetchOptions]);

  return (
    <>
      <Typography variant="h4">
        Розничный торговый объект
      </Typography>

      <Formik
        initialValues={initialValues}
        validate={() => {
          const errors: {
            type?: string;
            inn?: string;
          } = {};
          if (!type) {
            errors.type = "Выберите тип РТО";
          }
          if (!value) {
            errors.inn = "Выберите компанию по ИНН";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          onSubmit();
        }}
      >
        {({ handleBlur, handleSubmit }) => (
          <Form
            onSubmit={handleSubmit}
            className="mt-5 flex h-full flex-col justify-between"
          >
            <FormControl>
              <InputLabel id="demo-simple-select-label" className="text-subtitle1">
                Тип компании
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="type"
                name="type"
                value={type ?? ""}
                onBlur={handleBlur}
                label="Тип компании"
                onChange={handleSelectChange}
                className="text-subtitle1"
              >
                {companyTypes.map((value) => (
                  <MenuItem key={value} value={value} className="text-subtitle2">
                    {CompanyTypeText[value]}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <ErrorMessage
              className="mt-2 text-red text-subtitle2"
              name="type"
              component="div"
            />

            <FormControl sx={{ marginTop: 2 }}>
              <Autocomplete
                className="text-subtitle1"
                getOptionLabel={(option) =>
                  typeof option === "string" ? option : option.value
                }
                filterOptions={(x) => x}
                options={options}
                id="inn"
                autoComplete
                includeInputInList
                filterSelectedOptions
                value={value}
                noOptionsText="Нет результатов по указанному ИНН"
                onChange={(event: any, newValue: Suggestion | null) => {
                  setOptions(newValue ? [newValue, ...options] : options);
                  setValue(newValue);
                }}
                onInputChange={(event, newInputValue) => {
                  setInputValue(newInputValue);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="ИНН"
                    fullWidth
                    placeholder="Укажите ваш ИНН"
                    InputProps={{ ...params.InputProps, className: 'text-subtitle1' }}
                    InputLabelProps={{ ...params.InputLabelProps, className: 'text-subtitle1' }}
                  />
                )}
                renderOption={(props, option) => {
                  return (
                    <li {...props}>
                      <Grid container alignItems="center">
                        <Grid
                          item
                          sx={{
                            width: "calc(100% - 44px)",
                            wordWrap: "break-word",
                          }}
                        >
                          <Typography variant="subtitle2">
                            {option.value}
                          </Typography>
                        </Grid>
                      </Grid>
                    </li>
                  );
                }}
              />
              <ErrorMessage
                className="mt-2 text-red text-subtitle2"
                name="inn"
                component="div"
              />
              {value && (
                <div className="mt-2">
                  <Typography variant="subtitle1">
                    {value?.data.address.unrestricted_value}
                  </Typography>
                </div>
              )}
            </FormControl>

            <div className="mt-5 flex justify-between">
              <BaseButton
                variant="LIGHT"
                onClick={onCancel}
                className="mr-2"
              >
                Отмена
              </BaseButton>
              <BaseButton type="submit">
                Далее
              </BaseButton>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CompanyDialogRTOView;
