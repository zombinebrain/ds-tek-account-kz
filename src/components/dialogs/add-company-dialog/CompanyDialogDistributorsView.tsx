import {api} from "~/utils/api";
import {ChangeEvent, useState} from "react";
import Typography from "@mui/material/Typography";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import Image from "next/image";
import BaseButton from "~/components/ui/BaseButton";
import * as React from "react";

type CompanyDialogDistributorsViewProps = {
  onCancel: () => void;
  isSubmitting: boolean;
  distributors: string[];
  onSubmit: () => void;
  error: string;
  selectDistributor: (arr: string[]) => void;
}

const CompanyDialogDistributorsView = ({
  onCancel,
  isSubmitting,
  distributors,
  onSubmit,
  error,
  selectDistributor
}: CompanyDialogDistributorsViewProps) => {
  const { data: distributorOptions = [] } = api.distributors.getAll.useQuery();
  const [isEmptyError, setIsEmptyError] = useState(false);

  const handleSelect = (e: ChangeEvent) => {
    setIsEmptyError(false);
    const selectedId = e.target.id;
    const index = distributors.findIndex(id => id === selectedId);
    selectDistributor(index === -1 ? [...distributors, selectedId] : distributors.filter(id => id !== selectedId));
  };

  const handleSubmit = () => {
    setIsEmptyError(!distributors.length);
    if (distributors.length) {
      onSubmit();
    }
  };

  return (
    <>
      <Typography variant="h4">
        Выберите своих дистрибьютеров
      </Typography>
      <div className={`my-5 flex border border-solid ${isEmptyError ? 'bg-red-bg border-red' : 'bg-grey1 border-transparent'} p-4 text-subtitle2`}>
        <InfoOutlinedIcon className="mr-2.5 text-red" />
        Выберите дистрибьюторов, у которых вы закупаете товары FENOX.
        Эта информация необходима нам для получения у них отчетов о
        ваших закупках и в дальнейшем начисления вам баллов.
      </div>
      <div className="grid grid-cols-2 gap-x-10 gap-y-2.5">
        {
          distributorOptions.map(distributor => (
            <div key={distributor.id} className="flex items-center justify-between">
              <label htmlFor={distributor.id} className="flex items-center w-full h-8 cursor-pointer">
                {distributor.image ? (
                  <Image
                    src={
                      process.env.NEXT_PUBLIC_IMAGES_URL! + distributor.image
                    }
                    width={32}
                    height={32}
                    className="mr-2.5 rounded-full"
                    alt={`distributor-${distributor.id} logo`}
                  />
                ) : (
                  <div className="rounded-full w-8 h-8 bg-grey2 mr-2.5" />
                )}
                { distributor.name }
              </label>
              <input
                id={distributor.id}
                type="checkbox"
                className="accent-red cursor-pointer"
                checked={distributors.includes(distributor.id)}
                onChange={handleSelect}
              />
            </div>
          ))
        }
      </div>

      {error && <p className="text-red mt-5">{error}</p>}

      <div className="mt-5 flex justify-between">
        <BaseButton
          variant="LIGHT"
          onClick={onCancel}
          className="mr-2"
        >
          Назад
        </BaseButton>
        <BaseButton
          disabled={isSubmitting}
          onClick={handleSubmit}
        >
          Готово
        </BaseButton>
      </div>
    </>
  );
};

export default CompanyDialogDistributorsView;
