import Dialog from "@mui/material/Dialog";
import { api } from "~/utils/api";
import {Company, CompanyType} from "@prisma/client";
import {useState} from "react";
import { CompanyErrors } from "~/models/company";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "~/app/store";
import {setUser} from "~/app/store/modules/user";
import * as React from "react";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import CompanyDialogDistributorsView from "~/components/dialogs/add-company-dialog/CompanyDialogDistributorsView";
import CompanyDialogRTOView, {Suggestion} from "~/components/dialogs/add-company-dialog/CompanyDialogRTOView";

export default function AddCompanyDialog({
  open,
  onClose,
  refetchCompanies
}: {
  open: boolean;
  onClose: () => void;
  refetchCompanies: () => void;
}) {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.users.user);
  const [value, setValue] = useState<Suggestion | null>(null);
  const [error, setError] = useState("");
  const [type, setType] = useState<CompanyType | null>(null);
  const [distributors, setDistributors] = useState<string[]>([]);
  const [dialogState, setDialogState] = useState<'RTO' | 'DISTRIBUTORS'>('RTO');

  const { mutate: createCompany, isLoading: isSubmitting } = api.companies.create.useMutation({
    onSuccess(company: Company) {
      refetchCompanies();
      if (user) {
        dispatch(setUser({
          ...user,
          companies: [...user.companies, company]
        }));
      }
      handleClose();
    },
    onError(error) {
      setError(CompanyErrors[error.message]!);
    },
  });

  const handleFormSubmit = () => {
    setError('');
    value &&
      type &&
        createCompany({
          inn: value.data.inn,
          name: value.value,
          address: value.data.address.unrestricted_value,
          distributors,
          type
        });
  };

  const handleClose = () => {
    setType(null);
    setValue(null);
    setDistributors([]);
    setError('');
    onClose();
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      PaperProps={{
        className: 'relative px-10 pt-10 pb-8',
        sx: {
          width: dialogState === 'RTO' ? '444px' : '824px'
        },
      }}
    >
      <IconButton onClick={handleClose} className="w-8 h-8 absolute top-3 right-3">
        <CloseIcon />
      </IconButton>
      {
        dialogState === 'RTO' ? (
          <CompanyDialogRTOView
            value={value}
            type={type}
            setType={setType}
            setValue={setValue}
            onCancel={handleClose}
            onSubmit={() => setDialogState("DISTRIBUTORS")}
          />
        ) : (
          <CompanyDialogDistributorsView
            error={error}
            isSubmitting={isSubmitting}
            distributors={distributors}
            selectDistributor={setDistributors}
            onSubmit={handleFormSubmit}
            onCancel={() => setDialogState("RTO")}
          />
        )
      }
    </Dialog>
  );
};
