import Dialog from "@mui/material/Dialog";
import GiftCard from "../cards/GiftCard";
import IconButton from "@mui/material/IconButton";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";
import { useEffect, useState } from "react";
import BaseButton from "~/components/ui/BaseButton";
import { CatalogueGift } from "~/models/gift";

type AddGiftToCartDialogProps = {
  gift: CatalogueGift;
  open: boolean;
  onClose: () => void;
  addToCart: (gift: CatalogueGift, count?: number) => void;
};

const AddGiftToCartDialog = (props: AddGiftToCartDialogProps) => {
  const { open, gift, onClose, addToCart } = props;
  const [count, setCount] = useState(1);

  const handleAdd = () => {
    addToCart(gift, count);
    onClose();
  };

  useEffect(() => {
    setCount(1);
  }, [gift]);

  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{
        sx: {
          padding: 2,
          width: "400px",
        },
      }}
    >
      <GiftCard gift={gift} descriptionClassName="line-clamp-none break-words" />
      <div className="flex items-center justify-end">
        <IconButton
          disabled={count === 1}
          onClick={() => setCount(count - 1)}
          aria-label="add to cart"
          className="h-5 w-5 bg-red text-white hover:bg-grey3 disabled:bg-grey2"
        >
          <RemoveIcon />
        </IconButton>
        <div className="text-xl mx-1 inline-block font-bold">{count}</div>
        <IconButton
          onClick={() => setCount(count + 1)}
          aria-label="add to cart"
          className="h-5 w-5 bg-red text-white hover:bg-grey3"
        >
          <AddIcon />
        </IconButton>
      </div>
      <BaseButton onClick={handleAdd} className="mt-4">
        добавить в корзину
      </BaseButton>
    </Dialog>
  );
};

export default AddGiftToCartDialog;
