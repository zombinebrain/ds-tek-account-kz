import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import {Company} from "@prisma/client";
import TableRowsLoader from "~/components/ui/BaseTableSceleton";
import moment from "moment";

type AllCompaniesTableProps = {
  companies?: Company[];
  isFetched: boolean;
}

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "createdAt",
    text: "Дата регистрации"
  },
  {
    key: "inn",
    text: "ИНН"
  },
  {
    key: "name",
    text: "Наименование"
  },
  {
    key: "address",
    text: "Юридический адрес"
  }
];

const AllCompaniesTable = ({
  companies = [],
  isFetched
}: AllCompaniesTableProps) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {columnHeaders.map((columnHeader) => (
              <TableCell key={columnHeader.key} align={columnHeader.align} className="font-bold text-subtitle2">
                {columnHeader.text}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {isFetched ?
            companies.map((company) => (
              <TableRow
                key={company.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="text-subtitle2"
              >
                <TableCell component="th" scope="row">
                  {moment(company.createdAt).format('DD.MM.YYYY')}
                </TableCell>
                <TableCell component="th" scope="row">
                  {company.inn}
                </TableCell>
                <TableCell>{company.name}</TableCell>
                <TableCell>{company.address}</TableCell>
              </TableRow>
            )) : <TableRowsLoader cols={columnHeaders.length} />}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AllCompaniesTable;
