import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import { api } from "~/utils/api";
import IconButton from "@mui/material/IconButton";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { Region } from "@prisma/client";

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "name",
    text: "Название",
  },
  {
    key: "code",
    text: "Коды",
  },
  {
    key: "points",
    text: "Стоимость доставки в баллах",
  },
  {
    key: "actions",
  },
];

export default function RegionsTable({
  onRegionSelect,
}: {
  onRegionSelect: (region: Region) => void;
}) {
  const { status, data: regions } = api.regions.getALL.useQuery();

  const { mutate: deleteRegion } = api.regions.delete.useMutation();

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {columnHeaders.map((columnHeader) => (
              <TableCell key={columnHeader.key} align={columnHeader.align} className="font-bold text-subtitle2">
                {columnHeader.text}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {status === "success" &&
            regions.map((region) => (
              <TableRow
                key={region.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="text-subtitle2"
              >
                <TableCell component="th" scope="row">
                  {region.name}
                </TableCell>
                <TableCell component="th" scope="row">
                  {region.code}
                </TableCell>
                <TableCell component="th" scope="row">
                  {region.points}
                </TableCell>
                <TableCell className="w-[80px]" component="th" scope="row">
                  <IconButton
                    onClick={() => onRegionSelect(region)}
                    aria-label="edit"
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => deleteRegion(region.id)}
                    aria-label="delete"
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
