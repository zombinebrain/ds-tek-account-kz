import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import moment from "moment";
import { Order, OrderStatus } from "@prisma/client";
import DoDisturbIcon from "@mui/icons-material/DoDisturb";
import IconButton from "@mui/material/IconButton";
import { OrderStatuses } from "~/models/order";
import BaseButton from "~/components/ui/BaseButton";

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
  statuses?: OrderStatus[];
};

type ExtededOrder = Order & {
  user: {
    email: string;
  };
  orderGifts: {
    count: number;
    gift: {
      id: string;
      createdAt: Date;
      updatedAt: Date;
      name: string;
      description: string;
      points: number;
      visible: boolean;
    };
  }[];
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "number",
    text: "# заказа",
  },
  {
    key: "createdAt",
    text: "оформлен",
  },
  {
    key: "email",
    text: "email",
  },
  {
    key: "gifts",
    text: "подарки",
  },
  {
    key: "points",
    text: "баллы",
  },
  {
    key: "site",
    text: "сайт",
    statuses: [
      OrderStatuses.INDELIVERY,
      OrderStatuses.DELIVERED,
      OrderStatuses.CANCELED,
    ],
  },
  {
    key: "code",
    text: "код",
    statuses: [
      OrderStatuses.INDELIVERY,
      OrderStatuses.DELIVERED,
      OrderStatuses.CANCELED,
    ],
  },
  {
    key: "actions",
    statuses: [
      OrderStatuses.PENDING,
      OrderStatuses.INPROGRESS,
      OrderStatuses.INDELIVERY,
    ],
  },
];

export default function AllOrdersTable({
  status,
  orders,
  ordersFetched,
  updatingStatus,
  cancelingOrder,
  onDeliveryConfirmation,
  updateOrderStatus,
  cancelOrder,
  setOrderId,
  selectUser,
}: {
  status: OrderStatus;
  orders: ExtededOrder[];
  ordersFetched: boolean;
  updatingStatus: boolean;
  cancelingOrder: boolean;
  onDeliveryConfirmation: () => void;
  setOrderId: (newVal: string) => void;
  selectUser: (email: string) => void;
  cancelOrder: ({ id }: { id: string }) => void;
  updateOrderStatus: ({
    id,
    status,
  }: {
    id: string;
    status: OrderStatus;
  }) => void;
}) {
  const onCancelOrder = (orderId: string) => {
    cancelOrder({
      id: orderId,
    });
  };

  const handleStatusUpdate = (orderId: string) => {
    switch (status) {
      case OrderStatuses.PENDING: {
        updateOrderStatus({
          id: orderId,
          status: OrderStatuses.INPROGRESS,
        });
        break;
      }
      case OrderStatuses.INPROGRESS: {
        setOrderId(orderId);
        onDeliveryConfirmation();
        break;
      }
      case OrderStatuses.INDELIVERY: {
        updateOrderStatus({
          id: orderId,
          status: OrderStatuses.DELIVERED,
        });
        break;
      }
    }
  };

  const buttonText = (orderStatus: OrderStatus) => {
    switch (orderStatus) {
      case OrderStatuses.PENDING:
        return "отправить на сборку";
      case OrderStatuses.INPROGRESS:
        return "собран и отправлен";
      case OrderStatuses.INDELIVERY:
        return "доставлен";
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table
        sx={{ minWidth: 650, overflowX: "auto" }}
        aria-label="simple table"
      >
        <TableHead>
          <TableRow>
            {columnHeaders
              .filter(
                (columnHeader) =>
                  !columnHeader.statuses ||
                  columnHeader.statuses.includes(status)
              )
              .map((columnHeader) => (
                <TableCell key={columnHeader.key} align={columnHeader.align} className="text-subtitle2 font-bold">
                  {columnHeader.text}
                </TableCell>
              ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {ordersFetched &&
            orders.map((order) => (
              <TableRow
                key={order.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                className="text-subtitle2"
              >
                <TableCell component="th" scope="row">
                  {order.number}
                </TableCell>
                <TableCell component="th" scope="row">
                  <div>{moment(order.createdAt).format("DD.MM.YY")}</div>
                  <div>{moment(order.createdAt).format("HH:mm")}</div>
                </TableCell>
                <TableCell onClick={() => selectUser(order.user.email)}>
                  {order.user.email}
                </TableCell>
                <TableCell>
                  {order.orderGifts
                    .map(
                      (orderGift) =>
                        `${orderGift.gift.name}${
                          orderGift.count > 1 && ` (${orderGift.count} шт.)`
                        }`
                    )
                    .join(", ")}
                </TableCell>
                <TableCell>
                  {order.orderGifts.reduce(
                    (acc, orderGift) =>
                      acc + orderGift.gift.points * orderGift.count,
                    0
                  )}
                </TableCell>
                {columnHeaders
                  .find((header) => header.key === "site")!
                  .statuses!.includes(status) && (
                  <TableCell component="th" scope="row">
                    {order.site}
                  </TableCell>
                )}
                {columnHeaders
                  .find((header) => header.key === "code")!
                  .statuses!.includes(status) && (
                  <TableCell component="th" scope="row">
                    {order.code}
                  </TableCell>
                )}
                {columnHeaders
                  .find((header) => header.key === "actions")!
                  .statuses!.includes(status) && (
                  <TableCell sx={{ maxWidth: "180px" }}>
                    <div className="flex justify-between">
                      <BaseButton
                        disabled={updatingStatus}
                        onClick={() => handleStatusUpdate(order.id)}
                      >
                        {buttonText(order.status)}
                      </BaseButton>
                      <IconButton
                        disabled={cancelingOrder}
                        onClick={() => onCancelOrder(order.id)}
                        aria-label="cancel order"
                      >
                        <DoDisturbIcon />
                      </IconButton>
                    </div>
                  </TableCell>
                )}
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
