import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import { Distributor } from "@prisma/client";
import EditIcon from "@mui/icons-material/Edit";
import IconButton from "@mui/material/IconButton";
import Image from "next/image";

type DistributorsTableProps = {
  distributors: Distributor[];
  status: "error" | "loading" | "success";
  onEdit: (distributor: Distributor) => void;
};

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "image",
    text: "Логотип",
  },
  {
    key: "name",
    text: "Наименование",
  },
  {
    key: "website_link",
    text: "Сайт",
  },
  {
    key: "legal_name",
    text: "Юридическое наименование",
  },
  {
    key: "contact_name",
    text: "Имя контактного лица",
  },
  {
    key: "city",
    text: "Город",
  },
  {
    key: "email",
    text: "Email",
  },
  {
    key: "contact_phone",
    text: "Телефон",
  },
  {
    key: "actions",
    text: "",
  },
  /*  {
    key: "companies",
    text: "Количество РТО"
  },*/
];

/*type DistributorWithCompanies = Distributor & {
  companyDistributors: {
    companyId: string;
    distributorId: string;
  }[];
};*/

export default function DistributorsTable({
  distributors,
  status,
  onEdit,
}: DistributorsTableProps) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {columnHeaders.map((columnHeader) => (
              <TableCell key={columnHeader.key} align={columnHeader.align}>
                {columnHeader.text}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {status === "success" &&
            distributors.map((distributor) => (
              <TableRow
                key={distributor.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {distributor.image && (
                    <Image
                      src={
                        process.env.NEXT_PUBLIC_IMAGES_URL! + distributor.image
                      }
                      width={70}
                      height={70}
                      alt={`distributor-${distributor.id} logo`}
                    />
                  )}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.name}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.website_link}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.legal_name}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.contact_name}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.city}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.email}
                </TableCell>
                <TableCell component="th" scope="row">
                  {distributor.contact_phone}
                </TableCell>
                {/*                <TableCell component="th" scope="row">
                  {distributor.companyDistributors.length}
                </TableCell>*/}
                <TableCell align="right">
                  <IconButton onClick={() => onEdit(distributor)}>
                    <EditIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
