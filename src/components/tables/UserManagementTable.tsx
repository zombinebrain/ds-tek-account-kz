import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import { User, UserRole } from "@prisma/client";
import {
  UserRoleText,
  UserRoles,
  UserStatuses,
  UserStatusesText,
} from "~/models/user";
import moment from "moment";
import { UserDrawer } from "~/components/drawers/UserDrawer";
import { useState } from "react";
import ConfirmationDialog from "~/components/dialogs/ConfirmationDialog";
import BaseButton from "~/components/ui/BaseButton";

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
  tabs?: UserRole[];
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "createdAt",
    text: "Зарегистрирован",
    tabs: [UserRoles.SALES, UserRoles.RETAIL],
  },
  {
    key: "email",
    text: "Email",
  },
  {
    key: "phone",
    text: "Телефон",
    tabs: [UserRoles.SALES, UserRoles.RETAIL],
  },
  {
    key: "role",
    text: "Роль",
    tabs: [UserRoles.MANAGER],
  },
  {
    key: "companies",
    text: "РТО",
    tabs: [UserRoles.RETAIL],
  },
  {
    key: "status",
    text: "Статус",
  },
  {
    key: "actions",
    align: "center",
  },
];

type UserWithCompanies = User & {
  companies: { id: string }[];
};

type UserManagementTableProps = {
  tab: UserRole;
  users: UserWithCompanies[];
  deleteUser: ({ id }: { id: string }) => void;
};

export default function UserManagementTable(props: UserManagementTableProps) {
  const { tab, users, deleteUser } = props;

  const [drawerOpen, setDrawerOpen] = useState(false);
  const [isOpenDeleteDialog, setIsOpenDeleteDialog] = useState(false);
  const [selectedToDelete, setSelectedToDelete] = useState<string | null>(null);
  const [userEmail, setUserEmail] = useState("");

  const closeDrawer = () => {
    setDrawerOpen(false);
    setUserEmail("");
  };

  const openDeleteUserDialog = (id: string) => {
    setSelectedToDelete(id);
    setIsOpenDeleteDialog(true);
  };

  const closeDeleteUserDialog = () => {
    setIsOpenDeleteDialog(false);
  };

  const confirmDelete = () => {
    if (selectedToDelete) {
      deleteUser({ id: selectedToDelete });
      setSelectedToDelete(null);
      closeDeleteUserDialog();
    }
  }

  const selectUser = (email: string) => {
    setUserEmail(email);
    setDrawerOpen(true);
  };

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {columnHeaders
              .filter((header) => !header.tabs || header.tabs?.includes(tab))
              .map((columnHeader) => (
                <TableCell key={columnHeader.key} align={columnHeader.align} className="font-bold text-subtitle2">
                  {columnHeader.text}
                </TableCell>
              ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => (
            <TableRow
              key={user.id}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              className="font-bold text-subtitle2"
            >
              {(tab === UserRole.RETAIL || tab === UserRole.SALES) && (
                <TableCell>
                  {moment(user.createdAt).format("DD.MM.YY")}
                </TableCell>
              )}
              <TableCell>{user.email}</TableCell>
              {(tab === UserRole.RETAIL || tab === UserRole.SALES) && (
                <TableCell>{user.phone || "---"}</TableCell>
              )}
              {tab === UserRole.RETAIL && (
                <TableCell>{user.companies.length}</TableCell>
              )}
              {tab === UserRole.MANAGER && (
                <TableCell>{UserRoleText[user.role]}</TableCell>
              )}
              <TableCell>{UserStatusesText[user.status]}</TableCell>
              <TableCell align="right">
                {(tab === UserRole.RETAIL || tab === UserRole.SALES) && (
                  <BaseButton
                    small
                    onClick={() => selectUser(user.email)}
                  >
                    подробнее
                  </BaseButton>
                )}
                {tab === UserRole.MANAGER &&
                  user.status !== UserStatuses.DELETED &&
                  user.role === "MANAGER" && (
                    <BaseButton
                      small
                      onClick={() => openDeleteUserDialog(user.id)}
                    >
                      удалить
                    </BaseButton>
                  )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <UserDrawer
        userEmail={userEmail}
        open={drawerOpen}
        onClose={closeDrawer}
      />
      <ConfirmationDialog
        isOpen={isOpenDeleteDialog}
        onClose={closeDeleteUserDialog}
        onConfirm={confirmDelete}
        title="Вы точно хотите удалить пользователя?"
      />
    </TableContainer>
  );
}
