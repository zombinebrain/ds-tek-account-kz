import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import {
  BonusCampaign,
  BonusCampaignStatus,
  TransactionType,
} from "@prisma/client";
import { api } from "~/utils/api";
import CheckIcon from "@mui/icons-material/Check";
import RemoveIcon from "@mui/icons-material/Remove";
import * as xlsx from "xlsx";
import InnListSentDialog from "../dialogs/InnListSentDialog";
import { VisuallyHiddenInput } from "../VisuallyHiddenInout";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "name",
    text: "Дистрибьютор",
  },
  {
    key: "innList",
    text: "Перечень ИНН дистрибьютору",
  },
  {
    key: "distributorReport",
    text: "Отчет от дистрибьютора",
  },
];

export default function BonusCampaignReportsTable({
  bonusCampaign,
  refetchCampaigns,
}: {
  bonusCampaign: BonusCampaign;
  refetchCampaigns: () => void;
}) {
  const {
    isFetched: distributorsFetched,
    data: distributors,
    refetch: refetchDistributors,
  } = api.transactions.getDistributorReportStatuses.useQuery({
    bonusCampaignID: bonusCampaign.id,
  });

  const { data: innLists, refetch: refetchInnList } =
    api.innLists.getALL.useQuery({
      bonus_campaign_id: bonusCampaign.id,
    });

  const { mutateAsync: createTransactions, isLoading: creatingReport } =
    api.transactions.create.useMutation({
      async onSuccess() {
        await refetchDistributors();
        refetchCampaigns();
      },
    });

  const { mutate: sendInnList, isLoading: sendingInnList } =
    api.distributors.sendInnList.useMutation({
      onSuccess: () => {
        refetchInnList()
          .then(() => {
            setOpen(true);
          })
          .catch((err) => console.log(err));
      },
    });

  const [open, setOpen] = React.useState(false);

  const handleChange = async (
    event: React.ChangeEvent<HTMLInputElement>,
    distributor_id: string,
    distributor_email: string
  ) => {
    const files = event.target.files;
    const newFile = files ? files[0] : null;

    if (newFile) {
      try {
        const fileData = await newFile.arrayBuffer();
        const workbook = xlsx.read(fileData, { type: "buffer" });

        // Предполагаем, что таблица находится в первом листе
        const firstSheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[firstSheetName!];

        // Парсинг данных из таблицы
        const parsedData: { inn: string; article: string; count: number }[] =
          xlsx.utils.sheet_to_json(worksheet!);
        const filledData = parsedData.filter((row) => row.article);

        await createTransactions({
          type: TransactionType.CREDIT,
          bonus_campaign_id: bonusCampaign.id,
          distributor_id,
          distributor_email,
          data: filledData.map((item) => ({
            ...item,
            article: item.article.toString(),
            count: item.count ?? 0,
          })),
        });
      } catch (error) {
        console.error("Error parsing the file:", error);
      }
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table
        sx={{ minWidth: 400, overflowX: "auto" }}
        aria-label="simple table"
      >
        <TableHead>
          <TableRow>
            {columnHeaders.map((columnHeader) => (
              <TableCell
                size="small"
                key={columnHeader.key}
                align={columnHeader.align}
                className="font-bold text-subtitle2"
              >
                {columnHeader.text}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {distributorsFetched &&
            distributors?.map((distributor) => (
              <TableRow
                key={distributor.id}
                sx={{ height: "48px" }}
                className="text-subtitle2"
              >
                <TableCell component="th" scope="row" size="small" className="text-subtitle2">
                  {distributor.name}
                </TableCell>
                <TableCell
                  sx={{ maxWidth: "150px", width: "150px" }}
                  size="small"
                >
                  <div className="flex justify-center items-center">
                    {bonusCampaign.status !== BonusCampaignStatus.CLOSED && (
                      <BaseLoadingButton
                        small
                        loading={sendingInnList}
                        onClick={() =>
                          sendInnList({
                            distributor_id: distributor.id,
                            bonus_campaign_id: bonusCampaign.id,
                          })
                        }
                        className="rounded-none mr-2"
                      >
                        отправить
                      </BaseLoadingButton>
                    )}
                    {innLists?.find(
                      (list) => list.distributor_id === distributor.id
                    ) ? (
                      <CheckIcon />
                    ) : (
                      <RemoveIcon />
                    )}
                  </div>
                </TableCell>
                <TableCell
                  sx={{ maxWidth: "150px", width: "150px" }}
                  size="small"
                >
                  <div className="flex justify-center items-center">
                    {bonusCampaign.status !== BonusCampaignStatus.CLOSED && (
                      <BaseLoadingButton
                        small
                        loading={creatingReport}
                        component="label"
                        className="rounded-none mr-2"
                      >
                        Загрузить
                        <VisuallyHiddenInput
                          onChange={(
                            event: React.ChangeEvent<HTMLInputElement>
                          ) => handleChange(event, distributor.id, distributor.email)}
                          type="file" />
                      </BaseLoadingButton>
                    )}
                    {distributor.transactions.length ? (
                      <CheckIcon />
                    ) : (
                      <RemoveIcon />
                    )}
                  </div>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
      <InnListSentDialog open={open} onClose={() => setOpen(false)} />
    </TableContainer>
  );
}
