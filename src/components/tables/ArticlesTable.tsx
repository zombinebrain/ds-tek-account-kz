import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { TableColumnAlign } from "~/models/table";
import { api } from "~/utils/api";

type ColumnHeader = {
  key: string;
  text?: string;
  align?: TableColumnAlign;
};

const columnHeaders: ColumnHeader[] = [
  {
    key: "code",
    text: "Артикул",
  },
  {
    key: "points",
    text: "Баллы",
  },
];

export default function ArticlesTable() {
  const { status, data: articles } = api.articles.getALL.useQuery();
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {columnHeaders.map((columnHeader) => (
              <TableCell key={columnHeader.key} align={columnHeader.align}>
                {columnHeader.text}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {status === "success" &&
            articles.map((article) => (
              <TableRow
                key={article.id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {article.code}
                </TableCell>
                <TableCell component="th" scope="row">
                  {article.points}
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
