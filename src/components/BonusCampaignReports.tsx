import { BonusCampaign, BonusCampaignStatus } from "@prisma/client";
import BonusCampaignReportsTable from "./tables/BonusCampaignReportsTable";
import SubmitBonusCampaignReportsDialog from "./dialogs/SubmitBonusCampaignReportsDialog";
import { useState } from "react";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

export default function BonusCampaignReports({
  bonusCampaign,
  refetchCampaigns,
  updateBonusCampaign,
  isUpdatingBonusCampaign,
}: {
  bonusCampaign: BonusCampaign;
  refetchCampaigns: () => void;
  updateBonusCampaign: ({
    id,
    status,
  }: {
    id: string;
    status: BonusCampaignStatus;
  }) => void;
  isUpdatingBonusCampaign: boolean;
}) {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <BonusCampaignReportsTable
        bonusCampaign={bonusCampaign}
        refetchCampaigns={refetchCampaigns}
      />
      {bonusCampaign.status === BonusCampaignStatus.ACTIVE && (
        <BaseLoadingButton
          loading={isUpdatingBonusCampaign}
          className="w-full"
          onClick={() => setOpen(true)}
        >
          закрыть период
        </BaseLoadingButton>
      )}
      <SubmitBonusCampaignReportsDialog
        open={open}
        bonusCampaign={bonusCampaign}
        updateBonusCampaign={updateBonusCampaign}
        isUpdatingBonusCampaign={isUpdatingBonusCampaign}
        onClose={() => setOpen(false)}
      />
    </div>
  );
}
