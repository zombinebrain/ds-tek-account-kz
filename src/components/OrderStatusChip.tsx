import Typography from "@mui/material/Typography";
import { OrderStatus, OrderStatusText, OrderStatuses } from "~/models/order";

export default function OrderStatusChip({ status }: { status: OrderStatus }) {
  const statusClasses = () => {
    if (status === OrderStatuses.DELIVERED) {
      return "bg-gray-200";
    } else if (status === OrderStatuses.CANCELED) {
      return "bg-gray-200 text-red";
    } else return "bg-black text-white";
  };

  return (
    <Typography
      className={`h-fit w-fit rounded-md px-2 ${statusClasses()}`}
      variant="subtitle2"
    >
      {OrderStatusText[status]}
    </Typography>
  );
}
