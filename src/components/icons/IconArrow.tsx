const IconArrow = ({ width = 20, height = 20, className = '' }) => {
  return (
    <svg width={width} height={height} className={className} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M8 1L13 9.5L8 18" stroke="#1E2023"/>
    </svg>
  );
};

export default IconArrow;
