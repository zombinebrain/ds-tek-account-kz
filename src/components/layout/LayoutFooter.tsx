import Image from "next/image";
import Link from "next/link";
import { api } from "~/utils/api";
import { createAndDownloadXLSX } from "~/utils/createAndDownloadXLSX";
import { useSession } from "next-auth/react";
import BaseLoader from "~/components/ui/BaseLoader";

const socialLinks = [
  {
    image: {
      src: "/social-icons/vk.svg",
      height: 13,
      width: 20,
    },
    link: "https://vk.com/fenoxautomotive",
  },
  {
    image: {
      src: "/social-icons/youtube.svg",
      height: 15,
      width: 21,
    },
    link: "https://www.youtube.com/user/FENOXGlobalGroup",
  },
  {
    image: {
      src: "/social-icons/viber.svg",
      height: 21,
      width: 21,
    },
    link: "https://invite.viber.com/?g2=AQAWsjLz%2BsAJE0knqgmJHjPnqwJEhyGCfRdBzQwFx%2B1Dcn1fN%2B2OeSSQj%2BE3iPKa&lang=en",
  },
  {
    image: {
      src: "/social-icons/telegram.svg",
      height: 15,
      width: 18,
    },
    link: "https://t.me/fenox_official",
  },
  {
    image: {
      src: "/social-icons/soc_drive.svg",
      height: 40,
      width: 97,
    },
    link: "https://www.drive2.ru/o/FENOX-Automotive"
  }
];

export default function LayoutFooter() {
  const { status } = useSession();

  const {
    isInitialLoading: isPriceListLoading,
    isRefetching,
    refetch: fetchPriceList
  } = api.bonusCampaigns.getLatestCampaignWithArticles.useQuery(undefined, {
    refetchOnWindowFocus: false,
    enabled: false
  });

  const DownloadPriceList = async () => {
    const res = await fetchPriceList();
    if (res.data && res.data[0]) {
      createAndDownloadXLSX<{ code: string, points: number}>(res.data[0].articles, [
        { key: 'code', name: 'Артикул' },
        { key: 'points', name: 'Стоимость в баллах' },
      ], 'Бонусная шкала');
    }
  };

  return (
    <footer className="flex flex-col w-full bg-red px-28 py-6">
      <div className="flex w-full items-center justify-between">
        <div className="flex items-center flex-1">
          {socialLinks.map((social, index) => {
            return (
              <Link
                target="_blank"
                href={social.link}
                key={index}
                className="flex h-[40px] min-w-[40px] items-center justify-center"
              >
                <Image
                  src={social.image.src}
                  width={social.image.width}
                  height={social.image.height}
                  alt={`bonus campaign image`}
                />
              </Link>
            );
          })}
        </div>
        <div className="flex items-center flex-1 justify-end">
          <Link target="_blank" href="https://www.fenox.com/">
            <Image
              src="/logo-white.svg"
              width={95}
              height={18}
              alt={`bonus campaign image`}
            />
          </Link>
        </div>
      </div>
      {/*<nav className="flex space-x-5 text-subtitle2 mt-2.5">
        <Link href="/privacy-policy" className="text-white no-underline">Политика конфиденциальности</Link>
        <Link href="/terms-of-use" className="text-white no-underline">Пользовательское соглашение</Link>
        <Link href="/points-rules" className="text-white no-underline">Правила начисления бонусных баллов</Link>
        {
          status === 'authenticated' && (
            <button
              disabled={isPriceListLoading || isRefetching}
              className="disabled:bg-transparent disabled:text-white text-white bg-transparent p-0 border-none cursor-pointer font-pragmatica flex items-center"
              onClick={DownloadPriceList}
            >
              Бонусная шкала
              { (isPriceListLoading || isRefetching) &&
                <div className="ml-2">
                  <BaseLoader color="#FFF" size={16} />
                </div>
              }
           </button>
          )
        }
      </nav>*/}
    </footer>
  );
}
