import {ReactNode, useState} from "react";
import LayoutFooter from "../LayoutFooter";
import SignInDialog from "~/components/dialogs/SignInDialog";
import SignUpDialog from "~/components/dialogs/SignUpDialog";
import CookiesDialog from "~/components/dialogs/CookiesDialog";
import LandingHeader from "~/components/layout/landing/LandingHeader";

export default function LandingLayout({
  children,
}: {
  children: ReactNode;
}) {
  const [isSignInDialogOpen, setIsSignInDialogOpen] = useState(false);
  const [isSignUpDialogOpen, setIsSignUpDialogOpen] = useState(false);

  return (
    <div className="bg-white text-grey4 flex flex-col items-center overflow-x-hidden">
      <div className="h-full min-h-[calc(100vh-115px)] w-[1440px] max-w-[1440px] relative left-[calc((100vw-100%)/2)]">
        <LandingHeader />
        <main className="w-full">{children}</main>
      </div>
      <LayoutFooter />
      <CookiesDialog />
      <SignInDialog
        open={isSignInDialogOpen}
        onClose={() => setIsSignInDialogOpen(false)}
        onSignUp={() => setIsSignUpDialogOpen(true)}
      />
      <SignUpDialog
        open={isSignUpDialogOpen}
        onClose={() => setIsSignUpDialogOpen(false)}
      />
    </div>
  );
}
