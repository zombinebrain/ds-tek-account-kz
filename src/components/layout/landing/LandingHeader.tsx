import Image from "next/image";
import Link from "next/link";
import LandingButton from "~/components/landing/LandingButton";
import {usePathname} from "next/navigation";

const nav = [
/*  {
    href: '/partners',
    text: 'Дистрибьюторы'
  },*/
  {
    href: '/participant-rules',
    text: 'Правила участия'
  },
  {
    href: '/catalogue-gallery',
    text: 'Каталог подарков'
  }
];

const LandingHeader = () => {
  const currentRoute = usePathname();
  const isActiveLink = (href: string) => currentRoute === href;

  return (
    <header className="h-[168px] bg-transparent flex items-center px-20 z-50 relative">
      <Link href="/" className="h-[26px] mr-10">
        <Image src="/logo.png" width={138} height={26} alt="Fenox logo image" />
      </Link>
{/*      <nav className="flex flex-1 space-x-10 text-subtitle1">
        {
          nav.map(link => (
            <Link
              key={link.text}
              href={link.href}
              className={`no-underline hover:text-red ${isActiveLink(link.href) ? 'font-bold text-red' : 'text-grey4'}`}
            >
              {link.text}
            </Link>
          ))
        }
      </nav>*/}
    </header>
  );
};

export default LandingHeader;
