import {ReactNode} from "react";
import LayoutFooter from "~/components/layout/LayoutFooter";
import Image from "next/image";
import Link from "next/link";
import CookiesDialog from "~/components/dialogs/CookiesDialog";

export default function DocumentLayout({
  children,
}: {
  children: ReactNode;
}) {
  return (
    <div className="flex flex-col items-center">
      <div className="flex h-full min-h-[calc(100vh-115px)] w-[1440px] max-w-[1440px] flex-col pb-10">
        <header className="flex h-[88px] items-center justify-between px-20 py-5">
          <Link href="/" className="h-[26px]">
            <Image src="/logo.png" width={138} height={26} alt="Fenox logo image" />
          </Link>
        </header>
        <div className="flex flex-grow overflow-auto px-20">
          <main className="w-full">
            {children}
          </main>
        </div>
      </div>
      <LayoutFooter />
      <CookiesDialog />
    </div>
  );
};
