import { useEffect } from "react";
import Navbar from "../Navbar";
import { set } from "~/app/store/modules/supportContacts";
import { setUser } from "~/app/store/modules/user";
import { api } from "~/utils/api";
import { useDispatch } from "react-redux";
import CookiesDialog from "~/components/dialogs/CookiesDialog";

export default function MainLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const dispatch = useDispatch();
  const { data: supportContacts = [] } = api.supportContacts.getALL.useQuery();
  const { data: user = null } = api.users.getMe.useQuery();

  useEffect(() => {
    dispatch(set(supportContacts));
  }, [supportContacts, dispatch]);

  useEffect(() => {
    dispatch(setUser(user));
  }, [user, dispatch]);

  return (
    <div className="flex h-screen flex-col">
      <div className="flex h-full">
        <Navbar className="w-[250px]" />
        <main className="w-full overflow-auto bg-[#F2F3F5]">{children}</main>
      </div>
      <CookiesDialog />
    </div>
  );
}
