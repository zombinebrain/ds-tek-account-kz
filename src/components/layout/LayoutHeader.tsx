import Image from "next/image";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

export default function LayoutHeader({ onClick }: { onClick?: () => void }) {
  return (
    <div
      className="flex h-[88px] items-center justify-between px-[3.75rem]"
    >
      <Image src="/logo.png" width={138} height={26} alt="Fenox logo image" />
      {onClick && (
        <Button onClick={onClick}>
          <Typography variant="h4">
            войти
          </Typography>
        </Button>
      )}
    </div>
  );
}
