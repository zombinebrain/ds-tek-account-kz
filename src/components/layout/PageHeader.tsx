import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useSession } from "next-auth/react";
import LogoutIcon from "@mui/icons-material/Logout";
import { signOut } from "next-auth/react";

export default function PageHeader({ title }: { title: string }) {
  const { data: session } = useSession();

  const logout = async () => {
    await signOut({ callbackUrl: process.env.BASE_URL });
  };
  return (
    <div className="flex-center flex justify-between px-10 py-8">
      <Typography variant="h3">
        {title}
      </Typography>
      <Button
        onClick={logout}
        className="flex items-center"
        sx={{
          textTransform: "unset",
          borderRadius: 0,
        }}
      >
        <Typography variant="subtitle1">
          {session?.user.email}
        </Typography>
        <LogoutIcon className="ml-2 text-grey4" />
      </Button>
    </div>
  );
}
