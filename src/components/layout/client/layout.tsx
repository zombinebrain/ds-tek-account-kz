import { useDispatch, useSelector } from "react-redux";
import ClientHeader from "./ClientHeader";
import VidgetBar from "./VidgetBar";
import { setUser, setBalance, setOrders } from "~/app/store/modules/user";
import { set } from "~/app/store/modules/supportContacts";
import { useEffect } from "react";
import { api } from "~/utils/api";
import { RootState } from "~/app/store";
import { SupportDrawer } from "~/components/drawers/SupportDrawer";
import { setLatestTransactions } from "~/app/store/modules/transactions";
import LayoutFooter from "~/components/layout/LayoutFooter";
import CookiesDialog from "~/components/dialogs/CookiesDialog";

export default function ClientLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const dispatch = useDispatch();
  const { data: supportContacts } = api.supportContacts.getALL.useQuery();
  const { data: user = null } = api.users.getMe.useQuery();
  const { data: balance } = api.transactions.getMyBalance.useQuery();
  const { data: latestTransactions } = api.transactions.getLatest.useQuery();
  const { data: ordersCount = 0 } = api.orders.getMyOrdersCount.useQuery();
  const storeUser = useSelector((state: RootState) => state.users.user);

  useEffect(() => {
    dispatch(setUser(user));
  }, [user, dispatch]);

  useEffect(() => {
    if (supportContacts) {
      dispatch(set(supportContacts));
    }
  }, [supportContacts, dispatch]);

  useEffect(() => {
    if (balance !== undefined) {
      dispatch(setBalance(balance));
    }
  }, [balance, dispatch]);

  useEffect(() => {
    if (latestTransactions !== undefined) {
      dispatch(setLatestTransactions(latestTransactions));
    }
  }, [latestTransactions, dispatch]);

  useEffect(() => {
    if (!!ordersCount) {
      dispatch(setOrders(ordersCount));
    }
  }, [ordersCount, dispatch]);

  return (
    <div className="flex flex-col items-center">
      <div className="flex h-full min-h-[calc(100vh-115px)] w-[1440px] max-w-[1440px] flex-col pb-10">
        <ClientHeader />
        <div className="flex flex-grow overflow-auto px-[3.75rem] pb-0.5 -mb-0.5">
          <VidgetBar />
          <main className="ml-10 w-full">
            {storeUser && children}
          </main>
        </div>
      </div>
      <LayoutFooter />
      <CookiesDialog />
      <SupportDrawer />
    </div>
  );
}
