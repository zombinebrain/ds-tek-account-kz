import Image from "next/image";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import Button from "@mui/material/Button";
import { useState } from "react";
import { signOut } from "next-auth/react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "~/app/store";
import { toggleCartOpen } from "~/app/store/modules/cart";
import { toggleContactsDrawer } from "~/app/store/modules/supportContacts";
import IconSupport from "~/components/icons/IconSupport";

export default function LayoutHeader() {
  const router = useRouter();
  const path = router.pathname;
  const dispatch = useDispatch();
  const { data: session } = useSession();
  const cart = useSelector((state: RootState) => state.cart.cart);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  async function handleAuthClick() {
    if (session?.user) {
      await signOut({ callbackUrl: process.env.BASE_URL });
    } else {
      await router.push("/sign-in?callbackUrl=/");
    }
  }

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const isCartBtnVisible = (path === "/catalogue" || path === '/favorites') && !!cart.length;

  return (
    <div className="flex h-[88px] items-center justify-between px-[3.75rem] py-5">
      <Image src="/logo.png" width={138} height={26} alt="Fenox logo image" />
      <div className="flex items-center">
        {isCartBtnVisible && (
          <IconButton
            onClick={() => dispatch(toggleCartOpen(true))}
            sx={{
              position: "relative",
              height: 44,
              width: 44,
              padding: 0,
              backgroundColor: "black",
              marginRight: 2,
              ":hover": {
                backgroundColor: "#E51C2F",
              },
            }}
          >
            <ShoppingCartIcon
              sx={{
                height: 27,
                width: 27,
                color: "white",
              }}
            />
            <div className="absolute right-[-10px] top-[-10px] flex h-[22px] w-[22px] items-center justify-center rounded-full border border-solid border-white bg-[#E51C2F] text-white">
              <Typography variant="h4" className="text-white">
                {cart.length}
              </Typography>
            </div>
          </IconButton>
        )}
        <Button
          id="basic-button"
          aria-controls={open ? "basic-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
          onClick={handleClick}
          className="flex items-center"
          sx={{
            textTransform: "unset",
            padding: "0 0 0 10px",
            borderRadius: 0,
          }}
        >
          <Typography variant="subtitle1">
            {session?.user.email}
          </Typography>
          <KeyboardArrowDownIcon className="text-grey4" />
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={handleAuthClick} className="text-subtitle2">Выйти</MenuItem>
        </Menu>
        <IconButton onClick={() => dispatch(toggleContactsDrawer())}>
          <IconSupport />
        </IconButton>
      </div>
    </div>
  );
}
