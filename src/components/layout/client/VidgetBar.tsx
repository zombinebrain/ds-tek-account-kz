import VidgetCard from "~/components/cards/VidgetCard";
import BonusPointsVidget from "~/components/vidgets/BonusPointsVidget";
import ThumbUpOutlinedIcon from "@mui/icons-material/ThumbUpOutlined";
import { useRouter } from "next/router";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";
import TransactionVidgetCard from "~/components/cards/TransactionVidgetCard";
import React from "react";
import {Company} from "@prisma/client";
import CompanyBriefInfo from "~/components/company/CompanyBriefInfo";
import Image from "next/image";
import Typography from "@mui/material/Typography";
import Link from "next/link";

export default function VidgetBar() {
  const router = useRouter();
  const path = router.pathname;
  const user = useSelector((state: RootState) => state.users.user);
  const userFetched = useSelector(
    (state: RootState) => state.users.userFetched
  );
  const latestTransactionsFetched = useSelector(
    (state: RootState) => state.transactions.latestTransactionsFetched
  );
  const latestTransactions = useSelector(
    (state: RootState) => state.transactions.latest
  );
  const ordersCount = useSelector(
    (state: RootState) => state.users.orders
  );

  return (
    <div className="flex h-full min-w-[327px] flex-col gap-3">
      <BonusPointsVidget />
      <div className={`relative grid grid-cols-2 h-[132px] rounded-3xl bg-white border border-solid ${path === "/calculator" ? 'border-grey4' : 'border-transparent'}`}>
        <div>
          <Image
            className="absolute -scale-x-100 scale-y-100 -top-8 -left-9"
            width={190}
            height={190}
            src="/calculator.png"
            alt="calculator image"
          />
        </div>
        <div className="py-4">
          <Typography variant="subtitle2" className="flex flex-col mb-3">
            <span className="font-bold mb-1">Калькулятор баллов</span>
            <span>Сколько баллов я получу?</span>
          </Typography>
          <Link
            href="/calculator"
            className="bg-red text-subtitle2 font-bold text-white px-4 py-2 rounded-lg no-underline hover:bg-grey3"
          >
            Рассчитать
          </Link>
        </div>
      </div>
      <VidgetCard
        title="каталог подарков"
        to="/catalogue"
        active={path === "/catalogue"}
      />
      <VidgetCard
        title="мой профиль"
        to="/profile"
        active={path === "/profile"}
      >
        <div className="px-5 pb-5">
          {userFetched ? (
            <>
              <div className="mb-1 text-subtitle2 text-grey3">
                {user?.name ? user?.name : "Указать имя"}
              </div>
              <div className="mb-1 text-subtitle2 text-grey3">
                {user?.phone ? user?.phone : "Добавить телефон"}
              </div>
              {!user?.companies.length ? (
                <div className="text-subtitle2 text-grey3">
                  Добавить розничный торговый объект
                </div>
              ) : (
                <div className="mt-2 flex flex-col gap-2">
                  {user?.companies?.map((company: Company) => (
                    <CompanyBriefInfo company={company} key={company.id} />
                  ))}
                </div>
              )}
            </>
          ) : (
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress
                sx={{
                  color: "#E51C2F",
                }}
              />
            </Box>
          )}
        </div>
      </VidgetCard>
      <VidgetCard
        title="история"
        to="/my-balance"
        active={path === "/my-balance"}
      >
        {latestTransactionsFetched ? (
          <div className="px-5 pb-5 child:mb-1">
            {latestTransactions.length ? (
              latestTransactions?.map((transaction) => (
                <TransactionVidgetCard
                  key={transaction.id}
                  transaction={transaction}
                />
              ))
            ) : (
              <div className="text-subtitle2 text-grey3">
                Поступления или списания отсутствуют
              </div>
            )}
          </div>
        ) : (
          <div>
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress
                sx={{
                  color: "#E51C2F",
                }}
              />
            </Box>
          </div>
        )}
      </VidgetCard>
      <VidgetCard
        count={ordersCount}
        title="мои заказы"
        to="/my-orders"
        active={path === "/my-orders"}
      />
      <VidgetCard
        count={user?.favorites.length}
        title="избранное"
        to={user?.favorites.length ? "/favorites" : undefined}
        active={path === "/favorites"}
      >
        {
          !user?.favorites.length && (
            <>
              <div className="flex flex-col items-center px-5 pb-5">
                <div className="mb-3 flex h-[56px] w-[56px] items-center justify-center rounded-full bg-[#D7D7D7]">
                  <ThumbUpOutlinedIcon className="text-white" fontSize="large" />
                </div>
                <div className="text-subtitle2 text-grey3">
                  Добавьте подарок из каталога
                </div>
              </div>
            </>
          )
        }
      </VidgetCard>
    </div>
  );
}
