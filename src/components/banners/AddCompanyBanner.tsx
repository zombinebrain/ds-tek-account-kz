import Typography from "@mui/material/Typography";
import AddIcon from "@mui/icons-material/Add";
import Image from "next/image";
import { useRouter } from "next/router";
import BaseButton from "~/components/ui/BaseButton";

export default function AddCompanyBanner() {
  const router = useRouter();

  const handleCompanyAdd = async () => {
    localStorage.setItem("openAddCompanyDialog", "open");
    await router.push("/profile");
  };
  return (
    <div className="mb-10 flex flex-col items-center space-y-6 bg-white py-12">
      <Image
        src="/car.png"
        width={115}
        height={95}
        alt="file icon"
        className="mb-5"
      />
      <Typography variant="h2">добавить розничную торговую точку</Typography>
      <div className="text-center">
        <Typography variant="subtitle1" className="mt-2 text-grey4">
          СТО или магазин автозапчастей.
        </Typography>
        <Typography variant="subtitle1">
          Мы начисляем баллы с каждой компании.
        </Typography>
      </div>
      <BaseButton onClick={handleCompanyAdd}>
        <AddIcon className="mr-2" />
        добавить
      </BaseButton>
    </div>
  );
}
