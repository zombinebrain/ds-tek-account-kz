import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "~/app/store";
import { toggleContactsDrawer } from "~/app/store/modules/supportContacts";
import ClearIcon from "@mui/icons-material/Clear";
import Image from "next/image";
import Typography from "@mui/material/Typography";

export const SupportDrawer = () => {
  const isDrawerOpen = useSelector(
    (state: RootState) => state.supportContacts.drawerOpen
  );
  // const supportContacts = useSelector(
  //   (state: RootState) => state.supportContacts.contacts
  // );

  const dispatch = useDispatch();

  return (
    <Drawer
      anchor="right"
      open={isDrawerOpen}
      onClose={() => dispatch(toggleContactsDrawer())}
      PaperProps={{
        className: "p-5 w-[400px]",
        sx: {
          backgroundColor: "#F2F3F5",
          height: "calc(100% - 40px)",
        },
      }}
    >
      <div className="flex flex-col justify-between">
        <IconButton
          onClick={() => dispatch(toggleContactsDrawer())}
          aria-label="delete"
          className="w-fit"
          sx={{
            padding: 0,
            marginBottom: "5px",
          }}
        >
          <ClearIcon />
        </IconButton>
        <div className="flex flex-col items-center">
          <Image
            src="/support.png"
            width={360}
            height={197}
            alt="support image"
            className="my-5"
          />
          <Typography
            variant="h4"
            className="text-center"
          >
            Служба поддержки партнеров программы
          </Typography>
          <Typography
            variant="subtitle2"
            className="text-center my-5"
          >
            Если у вас остались вопросы, пожалуйста, обратитесь к персональному
            менеджеру или свяжитесь с администратором службы поддержки партнеров
            Программы Лояльности &quot;FENOX Шоколад&quot;: по адресу электронной почты{" "}
            <a href={`mailto:chocobonus@dstek.ru`}>chocobonus@dstek.ru</a>, или по
            номеру телефона <a href={`tel:+79165170911`}>+79165170911</a>
          </Typography>
          {/* <div className="mt-5 flex flex-col items-center gap-2">
            {supportContacts.map((contact) => {
              if (contact.type === "EMAIL") {
                return (
                  <a key={contact.id} href={`mailto:${contact.value}`}>
                    {contact.value}
                  </a>
                );
              } else
                return (
                  <a key={contact.id} href={`tel:${contact.value}`}>
                    {contact.value}
                  </a>
                );
            })}
          </div> */}
        </div>
      </div>
    </Drawer>
  );
};
