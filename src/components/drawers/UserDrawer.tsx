import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import ClearIcon from "@mui/icons-material/Clear";
import Typography from "@mui/material/Typography";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import { api } from "~/utils/api";
import moment from "moment";
import CompanyCard from "../cards/CompanyCard";
import { CompanyWithCompanyDistributors } from "~/models/company";
import { useState } from "react";
import BaseButton from "~/components/ui/BaseButton";
import ConfirmationDialog from "~/components/dialogs/ConfirmationDialog";

type UserDrawerProps = {
  userEmail: string;
  open: boolean;
  onClose: () => void;
};

export const UserDrawer = (props: UserDrawerProps) => {
  const { userEmail, open, onClose } = props;

  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [selectedCompanyId, setSelectedCompanyId] = useState<null | string>(null);

  const { mutate: deleteCompany } = api.companies.delete.useMutation({
    onSuccess: () => {
      closeDeleteDialog();
      onClose();
    },
  });

  const { data: user } = api.users.getUserByEmail.useQuery({
    email: userEmail,
  });
  const { data: balance } = api.transactions.getUserBalanceByEmail.useQuery({
    email: userEmail,
  });

  const closeDeleteDialog = () => {
    setDeleteDialogOpen(false);
    setSelectedCompanyId(null);
  };

  const handleDeleteCompany = (companyId: string) => {
    setSelectedCompanyId(companyId);
    setDeleteDialogOpen(true);
  };

  const onConfirmDelete = () => {
    if (selectedCompanyId) {
      deleteCompany({ id: selectedCompanyId });
      closeDeleteDialog();
    }
  };

  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={onClose}
      PaperProps={{
        className: "p-5 w-[400px]",
        sx: {
          height: "calc(100% - 40px)",
        },
      }}
    >
      <div className="flex flex-col justify-between">
        <IconButton
          onClick={onClose}
          aria-label="delete"
          className="w-fit p-0"
        >
          <ClearIcon />
        </IconButton>
        <div className="flex flex-col">
          <Typography variant="h4" className="my-5">
            аккаунт
          </Typography>
          <div className="mt-2 flex">
            <div className="mr-2.5 flex h-[36px] w-[36px] items-center justify-center rounded-full bg-grey1">
              <PersonOutlineOutlinedIcon className="text-grey4" />
            </div>
            <div className="space-y-0.5">
              <Typography variant="subtitle2">{user?.email}</Typography>
              <Typography variant="subtitle2">
                Аккаунт зарегистрирован:{" "}
                {moment(user?.createdAt).format("DD.MM.YYYY")}
              </Typography>
              <Typography variant="subtitle2">
                Телефон: {user?.phone ? user?.phone : "не указан"}
              </Typography>
              <Typography variant="subtitle2">
                Имя: {user?.name ? user?.name : "не указано"}
              </Typography>
              <Typography variant="subtitle2">Баллы: {balance}</Typography>
            </div>
          </div>
          <BaseButton
            disabled
            className="my-5"
          >
            заблокировать
          </BaseButton>
          <div className="flex flex-col space-y-2">
            {user?.companies?.map((company: CompanyWithCompanyDistributors) => (
              <CompanyCard
                key={company.id}
                company={company}
                onDeleteCompany={() => handleDeleteCompany(company.id)}
              />
            ))}
          </div>
        </div>
      </div>
      <ConfirmationDialog
        isOpen={deleteDialogOpen}
        onConfirm={onConfirmDelete}
        title="вы уверены, что хотите удалить компанию?"
        onClose={closeDeleteDialog}

      />
    </Drawer>
  );
};
