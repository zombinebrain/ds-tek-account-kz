import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import ClearIcon from "@mui/icons-material/Clear";
import Image from "next/image";
import Typography from "@mui/material/Typography";
import { api } from "~/utils/api";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

export const TransactionDrawer = ({
  transactionID,
  open,
  onClose,
}: {
  transactionID: string;
  open: boolean;
  onClose: () => void;
}) => {
  const { data: transaction, isFetched: transactionFetched } =
    api.transactions.getFullTransaction.useQuery({
      id: transactionID,
    });
  return (
    <Drawer
      anchor="right"
      open={open}
      onClose={onClose}
      PaperProps={{
        className: "p-5 w-[400px]",
        sx: {
          backgroundColor: "#F2F3F5",
          height: "calc(100% - 40px)",
        },
      }}
    >
      <div className="flex justify-between">
        <IconButton
          onClick={onClose}
          aria-label="delete"
          className="w-fit mb-5"
          sx={{
            padding: 0,
            marginBottom: "5px",
          }}
        >
          <ClearIcon />
        </IconButton>
      </div>
      {transactionFetched ? (
        transaction ? (
          <div>
            <div className="flex justify-between border-x-0 border-b border-t-0 border-solid border-[#D7D7D7] pb-5">
              <div className="flex">
                <Image
                  className="mr-2.5 mt-0.5 "
                  src={`/${
                    transaction.type === "CREDIT" ? "points-icon" : "red-box"
                  }.png`}
                  width={16}
                  height={16}
                  alt="points icon"
                />
                <div>
                  <Typography variant="subtitle1" className="font-bold">
                    {transaction.type === "CREDIT"
                      ? "Начислены баллы"
                      : "Оформлен заказ"}
                  </Typography>
                  {transaction.type === "CREDIT" && (
                    <div className="text-body1 space-y-0.5 mt-1">
                      <div>{transaction.company?.name}</div>
                      <div>Дистрибьютор: {transaction.distributor?.name}</div>
                      <div className="text-grey3">{`за период ${transaction.bonus_campaign?.startDate}-${transaction.bonus_campaign?.startDate}`}</div>
                    </div>
                  )}
                </div>
              </div>
              <div className="font-bold text-subtitle1">
                {transaction.type === "CREDIT" ? "+ " : "- "}
                {transaction.points}
              </div>
            </div>
            <div className="flex flex-col gap-2 py-5">
              {transaction.transaction_items?.map((item) => (
                <div key={item.id} className="flex justify-between">
                  <div className="flex">
                    <Image
                      className="mr-2.5"
                      src="/box-icon.svg"
                      width={20}
                      height={20}
                      alt="box icon"
                    />
                    <div className="text-body1 space-y-0.5">
                      <div>Артикул: {item.article}</div>
                      <div className="text-grey3">Количество: {item.count}</div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          "Транзакция не найдена"
        )
      ) : (
        <Box
          sx={{
            display: "flex",
            width: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress
            sx={{
              color: "#E51C2F",
            }}
          />
        </Box>
      )}
    </Drawer>
  );
};
