import GalleryGiftCard, {GalleryGiftType} from "~/components/landing/GalleryGiftCard";
import Dialog from "@mui/material/Dialog";

type GalleryGiftDialogProps = {
  isOpen: boolean;
  onClose: () => void;
  gift: GalleryGiftType
};

const GalleryGiftDialog = ({
  isOpen,
  onClose,
  gift
}: GalleryGiftDialogProps) => {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      PaperProps={{
        className: 'w-[468px] p-0'
      }}
    >
      <GalleryGiftCard
        gift={gift}
        showDescription
        showArrows
        className="rounded-none p-6"
        variant="BIG"
      />
    </Dialog>
  );
};

export default GalleryGiftDialog;
