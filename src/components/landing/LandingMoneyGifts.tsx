import Image from "next/image";

const virtualGifts = [
  {
    img: 'money_phone',
    text: 'Пополнить баланс мобильного телефона',
    id: 'phone'
  },
  {
    img: 'money_card',
    text: 'Перевести бонусные баллы на банковскую карту',
    id: 'card'
  }
];

const LandingMoneyGifts = ({ onClick }: { onClick: () => void }) => {
  return (
    <div className="flex space-x-10">
      {
        virtualGifts.map(gift => (
          <div key={gift.id} className="flex flex-col p-5 pb-6 shadow rounded-[32px] border border-solid border-grey1">
            <div className="relative mb-6">
              <Image
                className="rounded-2xl"
                width={460}
                height={283}
                src={`/${gift.img}.webp`}
                alt={`Изображение функции "${gift.text}"`}
              />
{/*              <button
                className="absolute bottom-5 right-5 text-[24px] bg-red text-white grid place-items-center rounded-full w-11 h-11 border border-solid border-white cursor-pointer hover:bg-grey3"
                onClick={onClick}
              >
                +
              </button>*/}
            </div>
            <span className="text-subtitle1">{gift.text}</span>
          </div>
        ))
      }
    </div>
  );
};

export default LandingMoneyGifts;
