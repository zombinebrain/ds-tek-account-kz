import IconButton from "@mui/material/IconButton";
import IconArrow from "~/components/icons/IconArrow";
import {MouseEvent, useState} from "react";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";

export type GalleryGiftType = {
  name: string;
  id: string;
  description: string;
  images: {
    id: number;
    url: string;
    giftId: string;
  }[];
};

type GalleryGiftCardProps = {
  gift: GalleryGiftType;
  onAddClick?: () => void;
  onCardClick?: () => void;
  showDescription?: boolean;
  showArrows?: boolean;
  className?: string;
  variant?: 'SMALL' | 'BIG';
};

const GalleryGiftCard = ({
  gift,
  onAddClick,
  onCardClick,
  showDescription = false,
  showArrows = false,
  className = '',
  variant = 'SMALL'
}: GalleryGiftCardProps) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const showPrevImage = (e: MouseEvent) => {
    e.stopPropagation();
    selectedIndex >= 1 && setSelectedIndex(selectedIndex - 1);
  };

  const showNextImage = (e: MouseEvent) => {
    e.stopPropagation();
    selectedIndex < (gift.images.length - 1) && setSelectedIndex(selectedIndex + 1);
  };

  const handleAddClick = (e: MouseEvent) => {
    e.stopPropagation();
    onAddClick?.();
  };
  const cardImage = (gift: { id: string, name: string, images: { id: number, url: string, giftId: string }[] }) => gift.images.length
    ? process.env.NEXT_PUBLIC_IMAGES_URL! + gift.images[selectedIndex]!.url
    : "/gift.jpg";

  return (
    <div
      onClick={onCardClick}
      className={`flex flex-col p-5 pb-6 shadow rounded-[32px] border border-solid border-grey1 ${className}`}
    >
      <div className="relative mb-6 mx-auto">
        <CardMedia
          image={cardImage(gift)}
          title={gift.name}
          className={`object-cover ${variant === 'SMALL' ? 'rounded-2xl' : ''} ${variant === 'BIG' ? 'w-[420px] h-[492px]' : 'w-[250px] h-[306px]'}`}
        />
        {
          gift.images.length > 1 && showArrows && (
          <>
            {
              selectedIndex > 0 && (
                <IconButton
                  onClick={showPrevImage}
                  sx={{ zIndex: 2 }}
                  className="rotate-180 w-10 h-10 absolute top-1/2 -translate-y-1/2 left-1 border border-solid border-grey4 bg-white hover:bg-transparent grid place-items-center"
                >
                  <IconArrow />
                </IconButton>
              )
            }
            {
              selectedIndex < gift.images.length - 1 && showArrows && (
                <IconButton
                  onClick={showNextImage}
                  sx={{ zIndex: 2 }}
                  className="w-10 h-10 absolute top-1/2 -translate-y-1/2 right-1 border border-solid border-grey4 bg-white hover:bg-transparent grid place-items-center"
                >
                  <IconArrow />
                </IconButton>
              )
            }
          </>
        )}
        {
          onAddClick && (
            <button
              className="absolute bottom-6 right-4 text-[24px] bg-red text-white grid place-items-center rounded-full w-11 h-11 border border-solid border-white cursor-pointer hover:bg-grey3 p-0"
              onClick={handleAddClick}
            >
              +
            </button>
          )
        }
      </div>
      <Typography variant="subtitle1" className="line-clamp-2 mb-2">
        {gift.name}
      </Typography>
      {
        showDescription && (
          <Typography className="text-grey3 line-clamp-none break-words">
            {gift.description}
          </Typography>
        )
      }
    </div>
  );
};

export default GalleryGiftCard;
