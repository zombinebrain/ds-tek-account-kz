import {ReactNode} from "react";

type LandingButtonProps = {
  children: ReactNode;
  onClick: () => void;
  type?: 'PRIMARY' | 'SECONDARY';
}

const LandingButton = ({
  children,
  onClick,
  type = 'PRIMARY'
}: LandingButtonProps) => {
  const typeStyles = type === 'PRIMARY' ? 'bg-red hover:bg-grey3 text-white border-transparent' : 'bg-white hover:bg-grey3 hover:text-white text-grey4 border-grey4';

  return (
    <button onClick={onClick} className={`border border-solid text-[20px] font-bebasneue uppercase rounded-2xl px-10 py-4 cursor-pointer ${typeStyles}`}>
      { children }
    </button>
  );
};

export default LandingButton;
