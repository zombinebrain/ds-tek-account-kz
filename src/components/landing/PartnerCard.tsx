import Image from "next/image";
import Link from "next/link";

type Partner = {
  id: string;
  image: string | null;
  legal_name: string;
  website_link: string;
  name: string;
};

type PartnerCardProps = {
  partner: Partner
};

const PartnerCard = ({
  partner
}: PartnerCardProps) => {
  return (
    <div className="flex flex-col items-center justify-center text-subtitle1">
      {
        partner.image ? (
          <Image
            className="object-fill"
            width={160}
            height={70}
            src={process.env.NEXT_PUBLIC_IMAGES_URL! + partner.image}
            alt={partner.legal_name}
          />
        ) : (
          <div className="w-[160px] h-[70px] bg-grey2" />
        )
      }
      <span className="font-bold mt-4">{partner.name}</span>
      <span className="my-1">{partner.name}</span>
      <Link
        href={partner.website_link}
        target="_blank"
        className="text-red no-underline hover:underline"
      >
        {partner.website_link}
      </Link>
    </div>
  );
};

export default PartnerCard;
