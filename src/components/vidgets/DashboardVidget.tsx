import Card from "@mui/material/Card";
import IconButton from "@mui/material/IconButton";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { Typography } from "@mui/material";
import Link from "next/link";

export default function DashboardVidget({
  className,
  title,
  to,
  active,
  count,
  children,
  onEdit,
}: {
  className?: string;
  title: string;
  to?: string;
  active?: boolean;
  count?: number;
  children?: React.ReactNode;
  onEdit?: () => void;
}) {
  return (
    <Card
      className={`${className}`}
      sx={{
        borderRadius: "20px",
        border: active ? "1px solid black" : "",
        width: "540px",
      }}
    >
      {to ? (
        <Link
          href={to}
          className="flex items-center justify-between p-5 text-black no-underline"
        >
          <Typography
            variant="h3"
            className="flex items-center"
          >
            {title}
            {count !== undefined && (
              <div className="ml-2 rounded-full bg-black px-2 py-1 text-white text-h4">
                {count}
              </div>
            )}
          </Typography>
          <IconButton
            aria-label="view order"
            sx={{
              height: "100%",
              padding: "0",
              borderRadius: "unset",
              color: "#E51C2F",
            }}
          >
            <Typography variant="subtitle2" className="text-red mr-2 font-bold">Перейти</Typography>
            <ArrowForwardIcon className="h-5 w-5" />
          </IconButton>
        </Link>
      ) : (
        <div className="flex items-center justify-between p-5">
          <Typography variant="subtitle2" className="font-bold">
            {title}
          </Typography>
          <IconButton
            onClick={onEdit}
            aria-label="view order"
            sx={{
              height: "100%",
              padding: "0",
              borderRadius: "unset",
              color: "#E51C2F",
            }}
          >
            <EditOutlinedIcon />
          </IconButton>
        </div>
      )}
      {children}
    </Card>
  );
}
