import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";

const prettifyNumber = (num: number) => {
  const n = num.toString();
  const separator = " ";
  return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + separator);
};

export default function BonusPointsVidget() {
  const myBalance = useSelector((state: RootState) => state.users.balance);
  const myBalanceFetched = useSelector(
    (state: RootState) => state.users.balanceFetched
  );

  return (
    <div className="flex min-h-[200px] flex-col justify-center rounded-3xl bg-chocolate bg-[left_0px_bottom] bg-no-repeat bg-grey4">
      <Typography
        variant="h4"
        color="white"
        className="flex items-center justify-center"
      >
        мой баланс
        <Image
          src="/fenox_bonus_icon.svg"
          width={24}
          height={24}
          alt="fenox bonus icon"
          className="ml-2"
        />
      </Typography>
      {myBalanceFetched ? (
        <Typography
          variant="h1"
          color="white"
          className="flex items-center justify-center"
          sx={{
            fontSize: "120px",
            lineHeight: "100%",
            letterSpacing: "3.6px"
          }}
        >
          {prettifyNumber(myBalance)}
        </Typography>
      ) : (
        <Box
          sx={{
            display: "flex",
            width: "100%",
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress
            sx={{
              color: "#E51C2F",
            }}
          />
        </Box>
      )}
      <Typography
        variant="h4"
        color="white"
        className="flex items-center justify-center"
      >
        баллов
      </Typography>
    </div>
  );
}
