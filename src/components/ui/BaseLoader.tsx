import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";

const BaseLoader = ({
  color = '#E51C2F',
  size = 40
}: {
  color?: string,
  size?: number
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        width: "100%",
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <CircularProgress sx={{ color }} size={size} />
    </Box>
  );
};

export default BaseLoader;
