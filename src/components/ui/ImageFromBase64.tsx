import { useEffect, useState } from "react";

type ImageFromBase64Props = {
  image: File | null;
  className?: string;
};

const ImageFromBase64 = ({ image, className }: ImageFromBase64Props) => {
  const [base64String, setBase64String] = useState("");

  useEffect(() => {
    if (!image) return;
    const reader = new FileReader();

    reader.onload = () => {
      const base64String = (reader.result as string).split(",")[1];
      setBase64String(base64String!);
    };

    reader.onerror = (error) => {
      console.error("File reading error:", error);
    };

    reader.readAsDataURL(image);
  }, [image]);

  return (
    // eslint-disable-next-line @next/next/no-img-element
    <img
      src={`data:image/png;base64,${base64String}`}
      className={className}
      alt="Base64 Image"
    />
  );
};

export default ImageFromBase64;
