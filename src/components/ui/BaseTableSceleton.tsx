import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import {Skeleton} from "@mui/material";

type TableRowsLoaderProps = {
  cols: number,
  rows?: number
};
const TableRowsLoader = ({ rows = 5, cols }: TableRowsLoaderProps) => {
  return [...Array(rows)].map((_, index) => (
    <TableRow key={`row-${index}`}>
      {
        [...Array(cols)].map((_, index) => (
          <TableCell component="th" scope="row" key={`col-${index}`}>
              <Skeleton animation="wave" variant="text" />
          </TableCell>
        ))
      }
    </TableRow>
  ));
};

export default TableRowsLoader;
