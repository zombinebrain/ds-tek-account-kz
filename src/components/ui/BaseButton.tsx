import { ReactNode } from "react";
import Button from "@mui/material/Button";

type BaseButtonProps = {
  children: ReactNode;
  onClick?: () => void;
  variant?: "LIGHT" | "DARK";
  small?: boolean;
  className?: string;
  type?: "button" | "submit" | "reset";
  disabled?: boolean;
  component?: "label" | "button" | "a";
  href?: string;
};
const BaseButton = ({
  children,
  onClick,
  variant = "DARK",
  small,
  className = "",
  type = "button",
  disabled,
  href,
  component = href ? "a" : "button",
}: BaseButtonProps) => {
  const disabledClassName =
    "disabled:bg-grey2 disabled:border-none disabled:text-white";
  const defaultClassName = `enabled:cursor-pointer enabled:hover:bg-red ${
    component !== "button" ? "hover:bg-red" : ""
  } rounded-none ${small ? "h-9 px-3 py-2.5" : "h-12 px-5 py-4"}`;
  const darkClassName = "bg-grey4 text-white border-none";
  const lightClassName = `bg-white text-grey4 enabled:hover:text-white ${
    !small ? "border-solid border border-grey4 hover:border-red" : ""
  }`;

  return (
    <Button
      component={component}
      href={href}
      type={type}
      disabled={disabled}
      onClick={onClick}
      className={`${className} ${disabledClassName} ${defaultClassName} ${
        variant === "DARK" ? darkClassName : lightClassName
      }`}
    >
      {children}
    </Button>
  );
};

export default BaseButton;
