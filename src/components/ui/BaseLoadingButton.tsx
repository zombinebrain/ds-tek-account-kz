import LoadingButton from "@mui/lab/LoadingButton";
import { ReactNode } from "react";

type BaseLoadingButtonProps = {
  loading?: boolean;
  disabled?: boolean;
  onClick?: () => void;
  small?: boolean;
  children: ReactNode;
  className?: string;
  component?: 'label' | 'button';
  type?: 'button' | 'submit' | 'reset';
}
const BaseLoadingButton = ({
  loading,
  disabled,
  onClick,
  small,
  className,
  children,
  component = 'button',
  type = 'button'
}: BaseLoadingButtonProps) => {
  return (
    <LoadingButton
      variant="contained"
      component={component}
      loading={loading}
      disabled={disabled}
      onClick={onClick}
      type={type}
      sx={{
        backgroundColor: "#1E2023",
        color: "white",
        height: small ? "36px" : "48px",
        "&:hover": {
          backgroundColor: "#E51C2F",
        }
      }}
      className={className}
    >
      {children}
    </LoadingButton>
  );
};

export default BaseLoadingButton;
