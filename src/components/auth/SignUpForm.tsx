import * as React from "react";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import { useRef, useState } from "react";
import MenuItem from "@mui/material/MenuItem";
import { api } from "~/utils/api";
import { signIn } from "next-auth/react";
import { ClientUser, UserRole, UserRoles } from "~/models/user";
import { ErrorMessage, Form, Formik } from "formik";
import OutlinedInput from "@mui/material/OutlinedInput";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

export default function SignUpForm({
  initialFormValues,
  code,
}: {
  initialFormValues: {
    role: UserRole;
    email: string;
    password: string;
  };
  code?: string;
}) {
  const [showPassword, setShowPassword] = useState(false);
  const availableRoles = [UserRoles.RETAIL, UserRoles.SALES];
  const roleOptions = [...availableRoles, UserRoles.MANAGER];
  const passwordRef = useRef<{ value: string }>(null);

  const { mutate, isLoading: creatingUser } = api.users.create.useMutation();

  const { mutate: activateUser } = api.users.activate.useMutation({
    onSuccess: async (user: ClientUser) => {
      const password = passwordRef.current?.value;
      password &&
        (await signIn("credentials", {
          email: initialFormValues.email,
          password: password,
          callbackUrl:
            user.role === UserRoles.RETAIL || user.role === UserRoles.SALES
              ? "/catalogue"
              : "/dashboard",
        }));
    },
  });

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const handleFormSubmit = (values: {
    email: string;
    role: UserRole;
    password: string;
  }) => {
    if (code) {
      activateUser({
        inviteCode: code,
        password: values.password,
      });
    } else {
      mutate(values);
    }
  };

  return (
    <>
      <Formik
        initialValues={initialFormValues}
        validate={(values) => {
          const errors: {
            role?: string;
            email?: string;
            password?: string;
          } = {};
          if (!values.role) {
            errors.role = "Выберите роль аккаунта";
          }
          if (!values.email) {
            errors.email = "Обязательно к заполнению";
          } else {
            const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
            if (!emailRegex.test(values.email)) {
              errors.email = "Неверный формат email";
            }
          }
          if (!values.password) {
            errors.password = "Обязательно к заполнению";
          } else if (values.password.length < 4) {
            errors.password = "Пароль должен содержать минимум 4 символа";
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(false);
          handleFormSubmit(values);
        }}
      >
        {({ values, handleChange, handleBlur, handleSubmit }) => (
          <Form
            onSubmit={handleSubmit}
            className="mt-5 flex h-full flex-col justify-between"
          >
            <FormControl>
              <InputLabel id="demo-simple-select-label" className="text-subtitle1">
                Роль аккаунта
              </InputLabel>
              <Select
                disabled={!!code}
                labelId="demo-simple-select-label"
                id="role"
                name="role"
                value={values.role}
                label="Роль аккаунта"
                onBlur={handleBlur}
                onChange={handleChange}
                className="text-subtitle1"
              >
                {(code ? roleOptions : availableRoles).map((value) => (
                  <MenuItem key={value} value={value} className="text-subtitle2">
                    {value}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <ErrorMessage
              className="text-red text-subtitle2 mt-2"
              name="role"
              component="div"
            />
            <FormControl variant="outlined" sx={{ marginTop: 1 }}>
              <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                E-mail
              </InputLabel>
              <OutlinedInput
                disabled={!!code}
                id="email"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                label="E-mail"
                className="text-subtitle1"
              />
            </FormControl>
            <ErrorMessage
              className="text-red text-subtitle2 mt-2"
              name="email"
              component="div"
            />
            <FormControl variant="outlined" sx={{ marginTop: 1 }}>
              <InputLabel htmlFor="outlined-adornment-password" className="text-subtitle1">
                Пароль
              </InputLabel>
              <OutlinedInput
                inputRef={passwordRef}
                id="password"
                name="password"
                type={showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Пароль"
                className="text-subtitle1"
              />
            </FormControl>
            <ErrorMessage
              className="text-red text-subtitle2 mt-2"
              name="password"
              component="div"
            />

            <BaseLoadingButton
              type="submit"
              loading={creatingUser}
              className="w-full mt-5"
            >
              зарегистрироваться
            </BaseLoadingButton>
          </Form>
        )}
      </Formik>
      <div className="mt-10 flex justify-between">
        <Typography>
          Нажимая кнопку “Зарегистрироваться” я подтверждаю, что ознакомилен(а)
          с{" "}
          <Link className="text-red" href="/">
            Пользовательским соглашением
          </Link>
          .
        </Typography>
      </div>
    </>
  );
};
