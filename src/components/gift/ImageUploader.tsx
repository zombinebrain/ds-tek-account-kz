import { VisuallyHiddenInput } from "../VisuallyHiddenInout";
import BaseButton from "../ui/BaseButton";
import Image from "next/image";
import ImageFromBase64 from "../ui/ImageFromBase64";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import { GiftImage } from "~/models/gift";
import { useMemo, useState } from "react";

type ImageUploaderProps = {
  images?: (File | GiftImage | string)[];
  imagesMax?: number;
  multiple?: boolean;
  className?: string;
  onImageUpdate: (files: any[] | null) => void;
};

export default function ImageUploader(props: ImageUploaderProps) {
  const {
    images = [],
    multiple = false,
    imagesMax = 1,
    className,
    onImageUpdate,
  } = props;
  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleChange = (files: File[] | null) => {
    if (files && files.length + images.length <= imagesMax) {
      onImageUpdate([...images, ...files]);
    } else {
      alert(`Максимальное количество картинок для загрузки ${imagesMax}`);
    }
  };

  const handleDelete = (index: number) => {
    setSelectedIndex(0);
    onImageUpdate(
      Array.from(images).filter((_, imageIndex) => imageIndex !== index)
    );
  };

  const selectedImage = useMemo(() => {
    if (images.length) {
      if (images[selectedIndex] instanceof File) {
        return images[selectedIndex];
      } else if (typeof images[selectedIndex] === "string") {
        return images[selectedIndex]!;
      } else {
        return (images[selectedIndex]! as GiftImage).url;
      }
    }
    return "";
  }, [images, selectedIndex]);

  return (
    <div className={className}>
      {!!selectedImage && (
        <div className="relative mb-4 flex items-center justify-center rounded-[10px] border border-solid border-grey2">
          {typeof selectedImage === "string" ? (
            <Image
              src={process.env.NEXT_PUBLIC_IMAGES_URL! + selectedImage}
              width={362}
              height={100}
              alt="image-upload"
            />
          ) : (
            <ImageFromBase64
              image={selectedImage as File}
              className="max-h-[100px] min-h-[100px] min-w-[362px] max-w-[362px] object-cover"
            />
          )}
          <Button
            onClick={(e) => {
              e.stopPropagation();
              handleDelete(selectedIndex);
            }}
            variant="outlined"
            sx={{
              position: "absolute",
              zIndex: 2,
              backgroundColor: "white",
              ":hover": {
                backgroundColor: "white",
              },
              top: "10px",
              right: "10px",
              padding: "0px",
              height: "32px",
              width: "32px",
              minWidth: "32px",
              borderColor: "#C2C2C2",
              borderRadius: "100%",
            }}
          >
            <DeleteIcon className="text-red" />
          </Button>
        </div>
      )}
      {imagesMax > 1 && !!images.length ? (
        <div className="flex gap-4 overflow-hidden">
          {Array.from(images).map((image, index) => (
            <div
              key={index}
              className={`${
                selectedIndex === index && "border-2 border-solid border-grey3"
              } relative box-content max-h-[62px] min-h-[62px] min-w-[127.5px] max-w-[127.5px] cursor-pointer`}
              onClick={() => setSelectedIndex(index)}
            >
              {image instanceof File ? (
                <ImageFromBase64
                  image={image}
                  className="max-h-[62px] min-h-[62px] min-w-[127.5px] max-w-[127.5px]"
                />
              ) : (
                <Image
                  src={
                    process.env.NEXT_PUBLIC_IMAGES_URL! +
                    (typeof image === "string" ? image : image.url)
                  }
                  width={127.5}
                  height={62}
                  alt="image-upload"
                />
              )}
              <Button
                onClick={(e) => {
                  e.stopPropagation();
                  handleDelete(index);
                }}
                variant="outlined"
                sx={{
                  position: "absolute",
                  zIndex: 2,
                  backgroundColor: "white",
                  ":hover": {
                    backgroundColor: "white",
                  },
                  top: "2px",
                  right: "2px",
                  padding: "0px",
                  height: "32px",
                  width: "32px",
                  minWidth: "32px",
                  borderColor: "#C2C2C2",
                  borderRadius: "100%",
                }}
              >
                <DeleteIcon className="text-red" />
              </Button>
            </div>
          ))}
          <BaseButton
            disabled={images.length === 3}
            component="label"
            className="h-[62px] w-full border !border-dashed border-grey3 bg-transparent !text-grey3 hover:!text-white"
          >
            <AddIcon />
            <VisuallyHiddenInput
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                const files = event.target.files as File[] | null;
                handleChange(files);
              }}
              type="file"
              multiple={multiple}
              accept="image/png, image/jpg, image/jpeg"
            />
          </BaseButton>
        </div>
      ) : (
        <BaseButton
          disabled={images.length === 3}
          component="label"
          className="h-[62px] w-full border !border-dashed border-grey3 bg-transparent !text-grey3 hover:!text-white"
        >
          <AddIcon />
          <VisuallyHiddenInput
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              const files = event.target.files as File[] | null;
              handleChange(files);
            }}
            type="file"
            multiple={multiple}
            accept="image/png, image/jpg, image/jpeg"
          />
        </BaseButton>
      )}
    </div>
  );
}
