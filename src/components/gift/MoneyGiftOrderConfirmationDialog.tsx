import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";
import Typography from "@mui/material/Typography";
import BaseButton from "~/components/ui/BaseButton";
import Dialog from "@mui/material/Dialog";
import BaseLoadingButton from "~/components/ui/BaseLoadingButton";

type MoneyGiftOrderConfirmationDialogProps = {
  isOpen: boolean;
  onClose: () => void;
  onCancel: () => void;
  onConfirm: () => void;
  isSuccess: boolean;
  text: string;
  isLoading?: boolean;
};

const MoneyGiftOrderConfirmationDialog = ({
  isOpen,
  onClose,
  onCancel,
  onConfirm,
  isSuccess,
  text,
  isLoading
}: MoneyGiftOrderConfirmationDialogProps) => {
  return (
    <Dialog
      open={isOpen}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      PaperProps={{ className: "px-[52px] pt-[52px] pb-8 relative w-[400px]" }}
    >
      <>
        <IconButton
          onClick={onClose}
          className="w-5 h-5 absolute top-6 right-6"
        >
          <CloseIcon />
        </IconButton>
        {
          isSuccess ? (
            <div className="flex flex-col items-center">
              <Image
                src="/success.png"
                width={132}
                height={136}
                alt="success image"
                className="mb-6"
              />
              <Typography variant="h3">Деньги отправлены!</Typography>
            </div>
          ) : (
            <Typography variant="h4" className="mb-8">
              {text}
            </Typography>
          )
        }
        {
          !isSuccess && (
            <div className="flex">
              <BaseButton
                variant="LIGHT"
                onClick={onCancel}
                className="w-full mr-4"
              >
                Назад
              </BaseButton>
              <BaseLoadingButton
                onClick={onConfirm}
                className="w-full"
                loading={isLoading}
              >
                подтвердить
              </BaseLoadingButton>
            </div>
          )
        }
      </>
    </Dialog>
  );
};

export default MoneyGiftOrderConfirmationDialog;
