import Drawer from "@mui/material/Drawer";
import { useEffect, useState } from "react";
import Cart from "~/components/cart/Cart";
import GiftCard from "~/components/cards/GiftCard";
import { api } from "~/utils/api";
import ClearIcon from "@mui/icons-material/Clear";
import IconButton from "@mui/material/IconButton";
import CartItemRemoveConfirmationDialog from "~/components/dialogs/CartItemRemoveConfirmationDialog";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "~/app/store";
import {
  add,
  remove,
  reset,
  updateCount,
  toggleCartOpen,
} from "~/app/store/modules/cart";
import Typography from "@mui/material/Typography";
import OrderCreatedDialog from "~/components/dialogs/OrderCreatedDialog";
import DisabledCartDialog from "~/components/dialogs/DisabledCartDialog";
import {setBalance, setOrders, setUser} from "~/app/store/modules/user";
import { ErrorType } from "~/models/errors";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { setLatestTransactions } from "~/app/store/modules/transactions";
import AddGiftToCartDialog from "~/components/dialogs/AddGiftToCartDialog";
import ThumbUpOutlinedIcon from "@mui/icons-material/ThumbUpOutlined";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import { MouseEvent } from "react";
import { CatalogueGift } from "~/models/gift";

type CatalogueTemplate = {
  title: string;
  isLoadingCatalogue: boolean;
  catalogue?: CatalogueGift[];
  refetchCatalogue: () => Promise<any>;
};
const CatalogueTemplate = ({
  title,
  isLoadingCatalogue,
  catalogue,
  refetchCatalogue,
}: CatalogueTemplate) => {
  const [selectedGift, setSelectedGift] = useState<CatalogueGift | null>(null);
  const [addToCartDialogOpen, setAddToCartDialogOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const [error, setError] = useState<ErrorType | null>(null);
  const cart = useSelector((state: RootState) => state.cart.cart);
  const isCartOpen = useSelector((state: RootState) => state.cart.open);
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.users.user);
  const myBalance = useSelector((state: RootState) => state.users.balance);
  const myBalanceFetched = useSelector(
    (state: RootState) => state.users.balanceFetched
  );

  const { data: balance, refetch: refetchBalance } =
    api.transactions.getMyBalance.useQuery();
  const { data: latestTransactions, refetch: refetchLatestTransactions } =
    api.transactions.getLatest.useQuery();
  const { data: ordersCount = 0, refetch: refetchOrdersCount } = api.orders.getMyOrdersCount.useQuery();

  const { mutate: createOrder, isLoading: isCreatingOrder } =
    api.orders.create.useMutation({
      onSuccess: async () => {
        await refetchBalance();
        await refetchLatestTransactions();
        await refetchOrdersCount();
        dispatch(toggleCartOpen(false));
        dispatch(reset());
        setDialogOpen(true);
      },
      onError: (e) => {
        setError(e.message as ErrorType);
        setOpen(true);
      },
    });

  const [cartItemToRemoveId, setCartItemToRemoveId] = useState<string | null>(
    null
  );
  const [removeConfirmationOpen, setRemoveConfirmationOpen] = useState(false);
  const [isDialogOpen, setDialogOpen] = useState(false);

  const removeCartItem = () => {
    dispatch(remove({ id: cartItemToRemoveId! }));
    dispatch(toggleCartOpen(false));
    setRemoveConfirmationOpen(false);
  };

  const addToCart = (gift: CatalogueGift, count?: number) => {
    if (!gift.visible) {
      setError("NOT_AVAILABLE");
      setOpen(true);
      return;
    }
    if (myBalanceFetched && !!myBalance) {
      if (cart.findIndex((cartItem) => cartItem.id === gift.id) !== -1) {
        update(gift.id, count ?? 1);
      } else {
        dispatch(
          add({
            ...gift,
            count: count ?? 1,
          })
        );
      }
    } else {
      setOpen(true);
    }
  };

  const update = (giftId: string, count: number) => {
    const gift = cart.find((cartItem) => cartItem.id === giftId);
    if (gift!.count + count === 0) {
      setCartItemToRemoveId(giftId);
      setRemoveConfirmationOpen(true);
    } else {
      dispatch(updateCount({ id: giftId, count }));
    }
  };

  useEffect(() => {
    if (balance !== undefined) {
      dispatch(setBalance(balance));
    }
  }, [balance, dispatch]);

  useEffect(() => {
    if (latestTransactions !== undefined) {
      dispatch(setLatestTransactions(latestTransactions));
    }
  }, [latestTransactions, dispatch]);

  useEffect(() => {
    if (!!ordersCount) {
      dispatch(setOrders(ordersCount))
    }
  }, [ordersCount, dispatch]);

  const isFavorite = (gift: CatalogueGift) => !!gift.favorites?.length;

  const handleGiftClick = (gift: CatalogueGift) => {
    setSelectedGift(gift);
    setAddToCartDialogOpen(true);
  };

  const { mutate: addToFavorite, isLoading: isAddingToFavorite } =
    api.gifts.addToFavorites.useMutation({
      onSuccess: async (newFavorite) => {
        await refetchCatalogue();
        if (user) {
          dispatch(
            setUser({
              ...user,
              favorites: [...user.favorites, newFavorite],
            })
          );
        }
      },
    });

  const { mutate: deleteFromFavorite, isLoading: isDeletingFromFavorite } =
    api.gifts.deleteFromFavorites.useMutation({
      onSuccess: async (deletedFavorite) => {
        await refetchCatalogue();
        if (user) {
          const index = user.favorites.findIndex(
            (item) => item.giftId === deletedFavorite.giftId
          );
          dispatch(
            setUser({
              ...user,
              favorites: [
                ...user.favorites.slice(0, index),
                ...user.favorites.slice(index + 1),
              ],
            })
          );
        }
      },
    });
  const addToFavoriteClick = (e: MouseEvent, gift: CatalogueGift) => {
    e.stopPropagation();
    isFavorite(gift) ? deleteFromFavorite(gift.id) : addToFavorite(gift.id);
  };

  return (
    <>
      <div>
        <Typography variant="h2" className="mb-5">
          {title}
        </Typography>
        <div className="grid grid-cols-4 gap-5 py-6">
          {isLoadingCatalogue ? (
            <Box
              sx={{
                display: "flex",
                width: "100%",
                height: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <CircularProgress
                sx={{
                  color: "#E51C2F",
                }}
              />
            </Box>
          ) : catalogue?.length ? (
            catalogue.map((gift) => (
              <GiftCard
                onCardClick={handleGiftClick}
                key={gift.id}
                gift={gift}
                onAddToCart={() => addToCart(gift)}
                additionalButton={
                  <IconButton
                    disabled={isAddingToFavorite || isDeletingFromFavorite}
                    onClick={(e) => addToFavoriteClick(e, gift)}
                    className="h-8 w-8 bg-white p-1.5 text-red hover:bg-grey2"
                  >
                    {isFavorite(gift) ? (
                      <ThumbUpIcon />
                    ) : (
                      <ThumbUpOutlinedIcon />
                    )}
                  </IconButton>
                }
              />
            ))
          ) : (
            <Typography variant="subtitle1">
              Подарки не были добавлены
            </Typography>
          )}
        </div>
      </div>
      <Drawer
        anchor="right"
        open={isCartOpen}
        onClose={() => dispatch(toggleCartOpen(false))}
        PaperProps={{
          className: "p-5 w-[400px]",
          sx: {
            height: "calc(100% - 40px)",
          },
        }}
      >
        <IconButton
          onClick={() => dispatch(toggleCartOpen(false))}
          aria-label="delete"
          className="w-fit"
          sx={{
            padding: 0,
            marginBottom: "5px",
          }}
        >
          <ClearIcon />
        </IconButton>
        <Cart
          className="h-full"
          onCountUpdate={update}
          createOrder={createOrder}
          isCreatingOrder={isCreatingOrder}
        />
      </Drawer>
      <CartItemRemoveConfirmationDialog
        open={removeConfirmationOpen}
        onClose={() => setRemoveConfirmationOpen(false)}
        onRemove={removeCartItem}
      />
      <OrderCreatedDialog
        open={isDialogOpen}
        onClose={() => setDialogOpen(false)}
      />
      <DisabledCartDialog
        open={open}
        onClose={() => setOpen(false)}
        error={error!}
      />
      {selectedGift && (
        <AddGiftToCartDialog
          gift={selectedGift}
          open={addToCartDialogOpen}
          onClose={() => setAddToCartDialogOpen(false)}
          addToCart={addToCart}
        />
      )}
    </>
  );
};

export default CatalogueTemplate;
