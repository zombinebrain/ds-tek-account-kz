import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import {useFormik} from "formik";
import MoneyGiftOrderConfirmationDialog from "~/components/gift/MoneyGiftOrderConfirmationDialog";
import {TransactionType} from "@prisma/client";
import DisabledCartDialog from "~/components/dialogs/DisabledCartDialog";
import BaseButton from "~/components/ui/BaseButton";
import {useMoneyGiftForm} from "~/hooks/useMoneyGiftForm";

const cardMoneyFormValues = {
  card_number: "",
  points: ""
};

const CardMoneyGiftForm = ({
  closeDrawer
}: {
  closeDrawer: () => void;
}) => {
  const {
    setIsConfirmationModalOpen,
    createTransaction,
    setIsSuccess,
    isConfirmationModalOpen,
    isCreatingTransaction,
    isSuccess,
    isErrorDialogOpen,
    setIsErrorDialogOpen,
    error
  } = useMoneyGiftForm();

  const formik = useFormik({
    initialValues: cardMoneyFormValues,
    validate: (values) => {
      const errors: {
        card_number?: string,
        points?: string
      } = {};
      if (!values.card_number) {
        errors.card_number = "Обязательно к заполнению";
      }
      if (!values.points || values.points === "0") {
        errors.points = "Введите количество баллов";
      }
      return errors;
    },
    onSubmit: () => {
      setIsConfirmationModalOpen(true);
    }
  });

  const handleConfirm = () => {
    const data = {
      //card_number: formik.values.card_number,
      points: Number(formik.values.points),
      type: TransactionType.CARD
    };
    createTransaction({...data});
  };

  const handleClose = () => {
    setIsConfirmationModalOpen(false);
    setIsSuccess(false);
  };

  const formatCardNumber = (value: string) => {
    const v = value
      .replace(/\s+/g, "")
      .replace(/[^0-9]/gi, "")
      .substring(0, 16);
    const parts = [];

    for (let i = 0; i < v.length; i += 4) {
      parts.push(v.substring(i, i + 4));
    }

    return parts.length > 1 ? parts.join(" ") : value;
  };

  return (
    <>
      <form
        onSubmit={formik.handleSubmit}
        className="flex h-full flex-col justify-between pt-5"
      >
        <div className="space-y-5">
          <Typography variant="h3">
            Перевести бонусные баллы на банковскую карту
          </Typography>
          <div>
            <label htmlFor="card_number" className="text-subtitle2 cursor-pointer">
              Номер банковской карты
            </label>
            <TextField
              fullWidth
              id="card_number"
              name="card_number"
              className="mt-2"
              value={formik.values.card_number}
              onChange={(e) => {
                const regex = /^[\d\s]+$/;
                if (e.target.value === '' || regex.test(e.target.value)) {
                  e.target.value = formatCardNumber(e.target.value);
                  formik.handleChange(e);
                }
              }}
              onBlur={formik.handleBlur}
              error={formik.touched.card_number && Boolean(formik.errors.card_number)}
              helperText={formik.touched.card_number && formik.errors.card_number}
              placeholder="XXXX XXXX XXXX XXXX"
              InputProps={{ className: 'bg-grey1 text-subtitle1' }}
            />
          </div>
          <div className="flex justify-between space-x-2.5">
            <div className="w-full">
              <label htmlFor="points" className="text-subtitle2 cursor-pointer">
                Баллы
              </label>
              <TextField
                fullWidth
                id="points"
                name="points"
                className="mt-2"
                value={formik.values.points}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.points && Boolean(formik.errors.points)}
                helperText={formik.touched.points && formik.errors.points}
                placeholder="0"
                InputProps={{ className: 'text-subtitle1 bg-grey1' }}
              />
            </div>
            <ArrowForwardIcon className="text-grey4 pt-10" />
            <div className="w-full">
              <label htmlFor="money" className="text-subtitle2 cursor-pointer">
                Денежная сумма
              </label>
              <TextField
                fullWidth
                disabled
                id="money"
                name="money"
                className="mt-2"
                value={formik.values.points}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                helperText=""
                placeholder="0"
                InputProps={{ className: 'text-subtitle1 bg-grey1' }}
              />
            </div>
          </div>
        </div>
        <BaseButton
          type="submit"
          className="w-full"
          disabled
        >
          перевести { formik.values.points || 'X' } руб.
        </BaseButton>
      </form>
      <MoneyGiftOrderConfirmationDialog
        isOpen={isConfirmationModalOpen}
        isLoading={isCreatingTransaction}
        onCancel={handleClose}
        onClose={() => {
          handleClose();
          closeDrawer();
        }}
        onConfirm={handleConfirm}
        text={`Проверьте правильность ввода номера банковской карты получателя ${formik.values.card_number}`}
        isSuccess={isSuccess}
      />
      <DisabledCartDialog
        open={isErrorDialogOpen}
        onClose={() => setIsErrorDialogOpen(false)}
        error={error!}
      />
    </>
  );
};

export default CardMoneyGiftForm;
