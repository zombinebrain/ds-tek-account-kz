import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import {useFormik} from "formik";
import { ChangeEvent } from "react";
import MoneyGiftOrderConfirmationDialog from "~/components/gift/MoneyGiftOrderConfirmationDialog";
import BaseButton from "~/components/ui/BaseButton";
import DisabledCartDialog from "~/components/dialogs/DisabledCartDialog";
import {TransactionType} from "@prisma/client";
import {useMoneyGiftForm} from "~/hooks/useMoneyGiftForm";

const phoneMoneyFormValues = {
  phone_number: "",
  points: ""
};

const PhoneMoneyGiftForm = ({
  closeDrawer
}: {
  closeDrawer: () => void;
}) => {
  const {
    setIsConfirmationModalOpen,
    createTransaction,
    setIsSuccess,
    isConfirmationModalOpen,
    isCreatingTransaction,
    isSuccess,
    isErrorDialogOpen,
    setIsErrorDialogOpen,
    error
  } = useMoneyGiftForm();

  const formik = useFormik({
    initialValues: phoneMoneyFormValues,
    validate: (values) => {
      const errors: {
        phone_number?: string,
        points?: string
      } = {};
      if (!values.phone_number) {
        errors.phone_number = "Обязательно к заполнению";
      } else if (values.phone_number.length !== 10) {
        errors.phone_number = "Превышено количество символов";
      }
      if (!values.points || values.points === "0") {
        errors.points = "Введите количество баллов";
      }
      return errors;
    },
    onSubmit: () => {
      setIsConfirmationModalOpen(true);
    }
  });

  const handleConfirm = () => {
    const data = {
      //phone_number: `+7${formik.values.phone_number}`,
      points: Number(formik.values.points),
      type: TransactionType.PHONE
    };
    createTransaction({...data});
  };

  const handleClose = () => {
    setIsConfirmationModalOpen(false);
    setIsSuccess(false);
  };

  const handleNumberInput = (e: ChangeEvent<HTMLInputElement>) => {
    const regex = /^[0-9\b]+$/;
    if (e.target.value === "" || regex.test(e.target.value)) {
      formik.handleChange(e);
    }
  };

  return (
    <>
      <form
        onSubmit={formik.handleSubmit}
        className="flex h-full flex-col justify-between pt-5"
      >
        <div className="space-y-5">
          <Typography variant="h3">
            Пополнить баланс мобильного телефона
          </Typography>
          <div>
            <label htmlFor="phone_number" className="text-subtitle2 cursor-pointer">
              Номер телефона
            </label>
            <TextField
              fullWidth
              id="phone_number"
              name="phone_number"
              className="mt-2"
              value={formik.values.phone_number}
              onChange={(e) => {
                const regex = /^[0-9\b]+$/;
                if (e.target.value === "" || regex.test(e.target.value)) {
                  formik.handleChange(e);
                }}}
              onBlur={formik.handleBlur}
              error={formik.touched.phone_number && Boolean(formik.errors.phone_number)}
              helperText={formik.touched.phone_number && formik.errors.phone_number}
              InputProps={{
                startAdornment: (
                  <span className="mr-1">+7</span>
                ),
                className: 'text-subtitle1 bg-grey1'
            }}
            />
          </div>
          <div className="flex justify-between space-x-2.5">
            <div className="w-full">
              <label htmlFor="points" className="text-subtitle2 cursor-pointer">
                Баллы
              </label>
              <TextField
                fullWidth
                id="points"
                name="points"
                className="mt-2"
                value={formik.values.points}
                onChange={handleNumberInput}
                onBlur={formik.handleBlur}
                placeholder="0"
                InputProps={{ className: 'text-subtitle1 bg-grey1' }}
              />
            </div>
            <ArrowForwardIcon className="text-grey4 pt-10" />
            <div className="w-full">
              <label htmlFor="money" className="text-subtitle2 cursor-pointer">
                Денежная сумма
              </label>
              <TextField
                fullWidth
                disabled
                id="money"
                name="points"
                className="mt-2"
                value={formik.values.points}
                onChange={handleNumberInput}
                onBlur={formik.handleBlur}
                placeholder="0"
                InputProps={{ className: 'text-subtitle1 bg-grey1' }}
              />
            </div>
          </div>
        </div>
        <BaseButton
          type="submit"
          className="w-full"
          disabled
        >
          пополнить баланс на { formik.values.points || 'X' } руб.
        </BaseButton>
      </form>
      <MoneyGiftOrderConfirmationDialog
        isOpen={isConfirmationModalOpen}
        isLoading={isCreatingTransaction}
        onCancel={handleClose}
        onClose={() => {
          handleClose();
          closeDrawer();
        }}
        onConfirm={handleConfirm}
        text={`Проверьте правильность ввода номера мобильного телефона: +7 ${formik.values.phone_number}`}
        isSuccess={isSuccess}
      />
      <DisabledCartDialog
        open={isErrorDialogOpen}
        onClose={() => setIsErrorDialogOpen(false)}
        error={error!}
      />
    </>
  );
};

export default PhoneMoneyGiftForm;
