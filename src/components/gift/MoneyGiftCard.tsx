import IconButton from "@mui/material/IconButton";
import AddIcon from "@mui/icons-material/Add";
import Typography from "@mui/material/Typography";
import React from "react";
import Image from "next/image";

const MoneyGiftCard = ({
  title,
  onAddClick,
  imageSrc
}: {
  title: string;
  onAddClick: () => void;
  imageSrc: string;
}) => {
  return (
    <div className="w-[308px]">
      <div className="relative">
        <Image
          src={imageSrc}
          alt={title}
          width={308}
          height={166}
          className="rounded-[20px]"
        />
        <IconButton
          onClick={onAddClick}
          aria-label={title}
          sx={{
            color: "white",
            border: "1px solid white",
          }}
          className="hover:bg-grey3 absolute bg-red right-3 bottom-3"
        >
          <AddIcon />
        </IconButton>
      </div>
      <Typography variant="subtitle1" className="font-bold mt-3">
        {title}
      </Typography>
    </div>
  );
};

export default MoneyGiftCard;
