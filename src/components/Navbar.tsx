"use client";
import { usePathname } from "next/navigation";
import Link from "next/link";
import { UserRoles, UserRole } from "~/models/user";
import { useEffect, useState } from "react";
import Image from "next/image";
import { useSelector } from "react-redux";
import { RootState } from "~/app/store";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

type NavbarLink = {
  href?: string;
  label?: string;
  roles?: UserRole[] | "*";
  subRoutes?: NavbarLink[]
};

export default function Navbar({ className }: { className?: string }) {
  const user = useSelector((state: RootState) => state.users.user);
  const currentRoute = usePathname();
  const [filteredLinks, setFilteredLinks] = useState([] as NavbarLink[]);
  // Функция для определения, является ли ссылка активной
  const isActiveLink = (href: string) => currentRoute === href;

  const isActiveSubLink= (arr: NavbarLink[]) => arr.findIndex(link => link.href === currentRoute) !== -1;

  useEffect(() => {
    const links: NavbarLink[] = [
      {
        href: "/dashboard",
        label: "Мой рабочий стол",
        roles: [UserRoles.ADMIN, UserRoles.MANAGER],
      },
      {
        label: "Заказы",
        roles: [UserRoles.ADMIN, UserRoles.MANAGER],
        subRoutes: [
          {
            href: "/orders",
            label: "Физические подарки",
            roles: [UserRoles.ADMIN, UserRoles.MANAGER],
          },
          {
            href: "/money-orders",
            label: "Денежные подарки",
            roles: [UserRoles.ADMIN, UserRoles.MANAGER],
          },
        ]
      },
      {
        href: "/distributors",
        label: "Дистрибьюторы",
        roles: [UserRoles.ADMIN],
      },
      {
        href: "/reports",
        label: "Бонусные программы",
        roles: [UserRoles.ADMIN],
      },
      {
        href: "/catalogue-management",
        label: "Управление каталогом",
        roles: [UserRoles.ADMIN],
      },
      {
        href: "/users",
        label: "Пользователи",
        roles: [UserRoles.ADMIN],
      },
      {
        href: "/companies",
        label: "РТО",
        roles: [UserRoles.ADMIN, UserRoles.MANAGER],
      },
    ];
    const filtered = links.filter((link) => {
      return link.roles === "*"
        ? !!user
        : !link.roles ||
            link.roles?.findIndex((role) => user?.role === role) !== -1;
    });
    setFilteredLinks(filtered);
  }, [user]);

  return (
    <div className="bg-white py-10 pl-5">
      <Image src="/logo.png" width={138} height={26} alt="Fenox logo image" />
      <div className={`${className} flex flex-col justify-between mt-5 p-0`}>
        {
          filteredLinks.map(({ href, label, subRoutes }) => (
            !subRoutes ? (
              <div
                key={`${label}-link`}
                className={`flex items-center hover:bg-grey1 h-12 border-0 border-l-4 border-solid ${isActiveLink(href!) ? "border-l-red" : "border-l-transparent"}`}
              >
                <Link
                  href={href!}
                  className="block w-full p-2 text-black no-underline text-button uppercase"
                >
                  {label}
                </Link>
              </div>
              ) : (
              <div key={`${label}-link`}>
                <div className={`flex items-center w-full text-button uppercase h-12 border-0 border-l-4 border-solid ${isActiveSubLink(subRoutes) ? "border-l-red" : "border-l-transparent"}`}>
                  <span className="p-2">
                  { label }
                  </span>
                </div>
                {
                  subRoutes.map(route => (
                    <div className="flex items-center" key={`${route.label}-sub-link`}>
                      <ArrowRightIcon
                        className={`h-4 w-4 ml-2 ${isActiveLink(route.href!) ? 'text-red' : 'text-grey2'}`}
                      />
                      <Link
                        href={route.href!}
                        className="block w-full text-black p-2 no-underline text-subtitle2"
                      >
                        {route.label}
                      </Link>
                    </div>
                  ))
                }
              </div>
            )
        ))}
      </div>
    </div>
  );
}
