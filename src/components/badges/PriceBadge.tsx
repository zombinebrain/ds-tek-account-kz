import Typography from "@mui/material/Typography";
import Image from "next/image";

export type PriceBadgeSize = "BIG" | /*"MEDIUM" |*/ "SMALL";

const iconSize = {
  BIG: 32,
  //MEDIUM: 20,
  SMALL: 20
};

const textVariants = {
  BIG: "h3",
  //MEDIUM: "h4",
  SMALL: "h4"
} as const;

type textVariantsType = (typeof textVariants)[PriceBadgeSize]

export default function PriceBadge({
  points,
  size = "SMALL",
}: {
  points: number;
  size?: PriceBadgeSize;
}) {
  return (
    <div className="flex items-center">
      <Typography
        variant={textVariants[size] as textVariantsType}
        className="font-bold"
      >
        {points}
      </Typography>
      <Image
        className="ml-1"
        src="/fenox_bonus_icon.svg"
        width={iconSize[size]}
        height={iconSize[size]}
        alt="fenox bonus icon"
      />
    </div>
  );
}
