import { Company, CompanyType } from "@prisma/client";
import TireRepairOutlinedIcon from "@mui/icons-material/TireRepairOutlined";
import StoreMallDirectoryOutlinedIcon from "@mui/icons-material/StoreMallDirectoryOutlined";
import HandshakeIcon from "@mui/icons-material/Handshake";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

const companyIcon = {
  [CompanyType.STO]: <TireRepairOutlinedIcon />,
  [CompanyType.SHOP]: <StoreMallDirectoryOutlinedIcon />,
  [CompanyType.SUBDEALER]: <HandshakeIcon />
};
const CompanyBriefInfo = ({
  company,
  onDeleteCompany
}: {
  company: Company;
  onDeleteCompany?: () => void;
}) => {
  return (
    <div className="flex items-center justify-between">
      <div className="flex items-center">
        <div className="mr-2.5 flex h-[36px] w-[36px] items-center justify-center rounded-full bg-grey1">
          {companyIcon[company.type]}
        </div>
        <div>
          <Typography
            variant="subtitle1"
            className="font-bold mb-0.5"
          >
            {company.name}
          </Typography>
          <div className="text-subtitle2">ИНН {company.inn}</div>
        </div>
      </div>
      {
        onDeleteCompany && (
          <IconButton onClick={onDeleteCompany} className="p-0">
            <DeleteOutlineIcon />
          </IconButton>
        )
      }
    </div>
  );
};

export default CompanyBriefInfo;
