import {Transaction, TransactionType} from "@prisma/client";

export function calculateTotalPoints(transactions: Transaction[]) {
  let totalPoints = 0;

  for (const transaction of transactions) {
    if (transaction.type ===  TransactionType.CREDIT || transaction.type ===  TransactionType.AUTH) {
      totalPoints += transaction.points;
    } else if (
      transaction.type === TransactionType.DEBIT ||
      transaction.type === TransactionType.CARD ||
      transaction.type === TransactionType.PHONE
    ) {
      totalPoints -= transaction.points;
    }
  }

  return totalPoints;
}
