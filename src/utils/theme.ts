import { createTheme } from "@mui/material/styles";
import { BebasNeue, Pragmatica } from "~/utils/fonts";

const rootElement =
  typeof document !== "undefined"
    ? document.getElementById("__next")
    : undefined;

console.log(rootElement);

export const theme = createTheme({
  typography: {
    fontFamily: Pragmatica.style.fontFamily,
    allVariants: {
      color: "#1E2023",
    },
    h1: {
      fontFamily: BebasNeue.style.fontFamily,
      fontSize: "70px",
      fontWeight: 400,
      lineHeight: "100%",
      letterSpacing: "2.1px",
      textTransform: "uppercase",
    },
    h2: {
      fontFamily: BebasNeue.style.fontFamily,
      fontSize: "50px",
      fontWeight: 400,
      lineHeight: "120%",
      letterSpacing: "1px",
      textTransform: "uppercase",
    },
    h3: {
      fontFamily: BebasNeue.style.fontFamily,
      fontSize: "32px",
      fontWeight: 400,
      lineHeight: "120%",
      letterSpacing: "0.64px",
      textTransform: "uppercase",
    },
    h4: {
      fontFamily: BebasNeue.style.fontFamily,
      fontSize: "20px",
      fontWeight: 400,
      lineHeight: "100%",
      letterSpacing: "0.4px",
      textTransform: "uppercase",
    },
    subtitle1: {
      fontFamily: Pragmatica.style.fontFamily,
      fontSize: "16px",
      fontWeight: 400,
      lineHeight: "130%",
    },
    subtitle2: {
      fontFamily: Pragmatica.style.fontFamily,
      fontSize: "14px",
      fontWeight: 400,
      lineHeight: "120%",
    },
    button: {
      fontFamily: Pragmatica.style.fontFamily,
      textTransform: "uppercase",
      fontSize: "12px",
      fontWeight: 700,
      lineHeight: "130%",
    },
    body1: {
      fontFamily: Pragmatica.style.fontFamily,
      fontSize: "12px",
      fontWeight: 400,
      lineHeight: "120%",
    },
  },
  components: {
    MuiPaper: {
      styleOverrides: {
        root: {
          boxSizing: "content-box"
        }
      }
    },
    MuiCard: {
      styleOverrides: {
        root: {
          boxShadow: "0px 10px 20px 0px rgba(97, 117, 132, 0.10)"
        }
      }
    },
    MuiAutocomplete: {
      styleOverrides: {
        noOptions: {
          fontSize: "14px",
          fontWeight: 400,
          lineHeight: "120%",
          //equals to subtitle2 font
        },
      },
    },
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          fontSize: "14px",
          fontWeight: 400,
          lineHeight: "120%",
          //equals to subtitle2 font
          margin: "8px 0 0",
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          "&.Mui-selected": {
            color: "black",
          },
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          ".MuiTabs-indicator": {
            backgroundColor: "black",
          },
        },
      },
    },
    /*
      TODO Test if it fixes mui styles overriding tailwind on prod to this guide https://mui.com/material-ui/guides/interoperability/#tailwind-css
        This code needed due to Mui Portal components been out of root container on default and tailwind file won't work properly
    */
    ...(rootElement
      ? {
          MuiPopover: {
            defaultProps: {
              container: rootElement,
            },
          },
          MuiPopper: {
            defaultProps: {
              container: rootElement,
            },
          },
          MuiDialog: {
            defaultProps: {
              container: rootElement,
            },
          },
          MuiModal: {
            defaultProps: {
              container: rootElement,
            },
          },
        }
      : {}),
  },
});
