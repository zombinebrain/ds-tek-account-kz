import localFont from "next/font/local";

export const BebasNeue = localFont({
  src: "../styles/fonts/BebasNeue/BebasNeue.ttf",
  variable: '--font-bebasneue'
});
export const Pragmatica = localFont({
  src: "../styles/fonts/Pragmatica/Pragmatica.woff2",
  variable: '--font-pragmatica'
});
