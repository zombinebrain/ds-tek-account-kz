const types = {
  png: 'image/png',
  jpeg: 'image/jpeg',
  jpg: 'image/jpg',
  pdf: 'application/pdf'
} as const;

export type ContentType = keyof typeof types;

export function base64ToArrayBuffer(base64: string) {
  const binaryString = window.atob(base64);
  const binaryLen = binaryString.length;
  const bytes = new Uint8Array(binaryLen);
  for (let i = 0; i < binaryLen; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }
  return bytes;
}
export function saveByteArray(fileName: string, byte: Uint8Array, contentType: ContentType) {
  const blob = new Blob([byte], { type: types[contentType] });
  const link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
}
