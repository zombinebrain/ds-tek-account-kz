import * as xlsx from "xlsx";

type ColType<T> = {
  key: keyof T;
  name?: string
}

export function createAndDownloadXLSX<T>(data: T[], cols: ColType<T>[], title = "output") {
  // Создайте массив данных для XLSX
  const xlsxData = [
    cols.map(col => col.name ?? col.key), // Названия колонок
    // Array.isArray(article[key]) ? xlsx.utils.json_to_sheet(article[key]) :
    ...data.map((article) => [...cols.map(col => article[col.key])]),
  ];

  // Создайте XLSX-лист из данных
  const worksheet = xlsx.utils.aoa_to_sheet(xlsxData);
  worksheet['!cols'] = [...cols.map(_ => ({wch: 30}))];

  // Создайте XLSX-книгу и добавьте лист к ней
  const workbook = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(workbook, worksheet, "Sheet1");

  // Преобразуйте книгу в буфер
  const buffer = xlsx.write(workbook, { type: "buffer", bookType: "xlsx" });

  // Создайте Blob объект из буфера
  const blob = new Blob([buffer], {
    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  });

  // Создайте ссылку для скачивания файла
  const url = window.URL.createObjectURL(blob);

  // Создайте ссылку для скачивания
  const a = document.createElement("a");
  a.href = url;
  a.download = `${title}.xlsx`;
  document.body.appendChild(a);
  a.click();

  // Очистите ссылку и объект Blob
  window.URL.revokeObjectURL(url);
  document.body.removeChild(a);
}
