import type { NextPage } from "next";
import { ComponentType, ReactElement, ReactNode } from "react";
import { UserRole } from "~/models/user";

export type NextPageProps = {
  fallback?: any;
};

export type NextPageWithLayout<P = NextPageProps> = NextPage<P> & {
  getLayout?: (page: ReactElement) => ReactNode;
  auth?:
    | boolean
    | {
        roles: UserRole[];
      };
  layout?: ComponentType;
};
