import {useEffect, useState} from "react";
import {ErrorType} from "~/models/errors";
import {useDispatch} from "react-redux";
import {api} from "~/utils/api";
import {setBalance} from "~/app/store/modules/user";
import {setLatestTransactions} from "~/app/store/modules/transactions";

export const useMoneyGiftForm = () => {
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [error, setError] = useState<ErrorType | null>(null);
  const [isErrorDialogOpen, setIsErrorDialogOpen] = useState(false);
  const dispatch = useDispatch();

  const { data: balance, refetch: refetchBalance } =
    api.transactions.getMyBalance.useQuery();
  const { data: latestTransactions, refetch: refetchLatestTransactions } =
    api.transactions.getLatest.useQuery();

  useEffect(() => {
    if (balance !== undefined) {
      dispatch(setBalance(balance));
    }
  }, [balance, dispatch]);

  useEffect(() => {
    if (latestTransactions !== undefined) {
      dispatch(setLatestTransactions(latestTransactions));
    }
  }, [latestTransactions, dispatch]);

  const { mutate: createTransaction, isLoading: isCreatingTransaction } =
    api.transactions.createMoneyGiftTransaction.useMutation({
      onSuccess: async () => {
        await refetchBalance();
        await refetchLatestTransactions();
        setIsSuccess(true);
      },
      onError: (e) => {
        setIsConfirmationModalOpen(false);
        setError(e.message as ErrorType);
        setIsErrorDialogOpen(true);
      }
    });

  return {
    isConfirmationModalOpen,
    setIsConfirmationModalOpen,
    isSuccess,
    setIsSuccess,
    error,
    setError,
    isErrorDialogOpen,
    setIsErrorDialogOpen,
    createTransaction,
    isCreatingTransaction
  };
};
