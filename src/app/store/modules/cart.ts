import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { CartItem } from "~/models/cart";

export interface CartState {
  cart: CartItem[];
  open: boolean;
}

const initialState: CartState = {
  cart: [],
  open: false,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    reset: (state) => {
      state.cart = [];
    },
    add: (state, action: PayloadAction<CartItem>) => {
      state.cart.push(action.payload);
    },
    remove: (state, action: PayloadAction<{ id: string }>) => {
      state.cart = state.cart.filter(
        (cartItem) => cartItem.id !== action.payload.id
      );
    },
    updateCount: (
      state,
      action: PayloadAction<{
        id: string;
        count: number;
      }>
    ) => {
      state.cart = state.cart.map((cartItem) => {
        if (cartItem.id === action.payload.id) {
          return {
            ...cartItem,
            count: cartItem.count + action.payload.count,
          };
        }
        return cartItem;
      });
    },
    toggleCartOpen: (state, action: PayloadAction<boolean>) => {
      state.open = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { reset, add, remove, updateCount, toggleCartOpen } =
  cartSlice.actions;

export default cartSlice.reducer;
