import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { TransactionWithBonusCampaignAndOrderAndDistributor } from "~/models/transactions";

export interface transactionsState {
  latest: TransactionWithBonusCampaignAndOrderAndDistributor[];
  latestTransactionsFetched: boolean;
}

const initialState: transactionsState = {
  latest: [],
  latestTransactionsFetched: false,
};

export const transationsSlice = createSlice({
  name: "transactions",
  initialState,
  reducers: {
    resetLatestTransactions: (state) => {
      state.latest = [];
      state.latestTransactionsFetched = false;
    },
    setLatestTransactions: (
      state,
      action: PayloadAction<
        TransactionWithBonusCampaignAndOrderAndDistributor[]
      >
    ) => {
      state.latest = action.payload;
      state.latestTransactionsFetched = true;
    },
  },
});

// Action creators are generated for each case reducer function
export const { resetLatestTransactions, setLatestTransactions } =
  transationsSlice.actions;

export default transationsSlice.reducer;
