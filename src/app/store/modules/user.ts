import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import {ExtendedClientUser} from "~/models/user";

export interface UserState {
  user: ExtendedClientUser | null;
  userFetched: boolean;
  balance: number;
  balanceFetched: boolean;
  orders: number;
  ordersFetched: boolean;
}

const initialState: UserState = {
  user: null,
  userFetched: false,
  balance: 0,
  balanceFetched: false,
  orders: 0,
  ordersFetched: false
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    resetUser: (state) => {
      state.user = null;
      state.userFetched = false;
    },
    setUser: (state, action: PayloadAction<ExtendedClientUser | null>) => {
      state.user = action.payload;
      state.userFetched = true;
    },
    setBalance: (state, action: PayloadAction<number>) => {
      state.balance = action.payload;
      state.balanceFetched = true;
    },
    setOrders: (state, action: PayloadAction<number>) => {
      state.orders = action.payload;
      state.ordersFetched = true;
    }
  },
});

// Action creators are generated for each case reducer function
export const { setUser, setBalance, setOrders, resetUser } = userSlice.actions;

export default userSlice.reducer;
