import { SupportContact } from "@prisma/client";
import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

export interface SupportContactsState {
  contacts: SupportContact[];
  drawerOpen: boolean;
}

const initialState: SupportContactsState = {
  contacts: [],
  drawerOpen: false,
};

export const supportContactsSlice = createSlice({
  name: "supportContacts",
  initialState,
  reducers: {
    reset: (state) => {
      state.contacts = [];
    },
    set: (state, action: PayloadAction<SupportContact[]>) => {
      state.contacts = action.payload;
    },
    toggleContactsDrawer: (state) => {
      state.drawerOpen = !state.drawerOpen;
    },
  },
});

// Action creators are generated for each case reducer function
export const { reset, set, toggleContactsDrawer } =
  supportContactsSlice.actions;

export default supportContactsSlice.reducer;
