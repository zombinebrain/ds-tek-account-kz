import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./modules/cart";
import supportContactsReducer from "./modules/supportContacts";
import transactionsReducer from "./modules/transactions";
import usersReducer from "./modules/user";

export const store = configureStore({
  // Добавил middleware c serializableCheck: false,
  // чтобы убрать из консоли ворнинги, когда я диспатчу createdAt и updatedAt в формате Date,
  // а не string, хотя так делать не рекомендуется,
  // возможно возникнут какие-то проблемы с этим в будущем,
  // но на данный момент быстрого фикса для этого не нашёл,
  // а мапить всё в string долго
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    cart: cartReducer,
    supportContacts: supportContactsReducer,
    users: usersReducer,
    transactions: transactionsReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
