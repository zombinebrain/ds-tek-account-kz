import { type Config } from "tailwindcss";

export default {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  /*
  TODO Test if it fixes mui styles overriding tailwind on prod
   according to this guide https://mui.com/material-ui/guides/interoperability/#tailwind-css
  */
  important: "#__next",
  theme: {
    fontSize: {
      h1: ['70px', {
        fontWeight: 400,
        lineHeight: '100%',
        letterSpacing: '2.1px'
      }],
      h2: ['50px', {
        fontWeight: 400,
        lineHeight: '120%',
        letterSpacing: '1px',
      }],
      h3: ['32px', {
        fontWeight: 400,
        lineHeight: '120%',
        letterSpacing: '0.64px',
      }],
      h4: ['20px', {
        fontWeight: 400,
        lineHeight: '100%',
        letterSpacing: '0.4px',
      }],
      subtitle1: ['16px', {
        fontWeight: 400,
        lineHeight: '130%'
      }],
      subtitle2: ['14px', {
        fontWeight: 400,
        lineHeight: '120%'
      }],
      button: ['12px', {
        fontWeight: 700,
        lineHeight: '130%'
      }],
      body1: ['12px', {
        fontWeight: 400,
        lineHeight: '120%'
      }]
    },
    extend: {
      colors: {
        red: "#E51C2F",
        grey1: "#F2F3F5",
        grey2: "#D7D7D7",
        grey3: "#737373",
        grey4: "#1E2023",
        'red-bg': 'rgba(229, 28, 47, 0.10)'
      },
      fontFamily: {
        bebasneue: ['var(--font-bebasneue)'],
        pragmatica: ['var(--font-pragmatica)']
      },
      boxShadow: {
        DEFAULT: '0px 20px 30px 20px #71646426',
        card: '0px 10px 20px 0px rgba(97, 117, 132, 0.10);'
      },
      backgroundImage: {
        'chocolate': "url('/chocolate-bar-small.png')"
      },
      spacing: {
        '15': '60px',
        '25': '100px'
      },
      animation: {
        'infinite-scroll': 'infiniteScroll 25s linear infinite',
        'infinite-scroll-reverse': 'infiniteScrollReverse 25s linear infinite',
      }
    },
  },
  plugins: [
    function ({ addVariant }: { addVariant: any }) {
      addVariant('child', '& > *');
    }
  ],
  corePlugins: {
    preflight: false,
  },
} satisfies Config;
