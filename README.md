# DS-TEK Account

This is a Full Stack project bootstrapped with `create-t3-app`.

## Run application locally

To run this app locally you must:

- Create your own `.env.local` file using `.env.example` file as example
- Use `npm i` to install dependencies
- Use `npm run dev` to run application once everything above is set up

## Learn More

To learn more about the [T3 Stack](https://create.t3.gg/), take a look at the following resources:

- [Documentation](https://create.t3.gg/)
- [Learn the T3 Stack](https://create.t3.gg/en/faq#what-learning-resources-are-currently-available) — Check out these awesome tutorials
